package com.biz.health.servermodel;

import com.google.gson.annotations.Expose;

public class NewProfile {
	@Expose public String firstName;
	@Expose public String lastName;
	@Expose public String password;
	@Expose public String email;
	
}
