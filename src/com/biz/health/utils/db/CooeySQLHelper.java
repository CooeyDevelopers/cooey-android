package com.biz.health.utils.db;

import java.io.IOException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CooeySQLHelper extends SQLiteOpenHelper {

	public CooeySQLHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		
		
	}

	private static final String DATABASE_NAME = "cooeyprofile.db";
	private static final int DATABASE_VERSION = 1;
	  
	  public static final String TABLE_USER = "cooeyuser";
	  public static final String TABLE_DEV = "cooeydevice";
	  public static final String TABLE_BP = "cooeyuserbp";
	  public static final String TABLE_WT = "cooeyuserweight";
	  public static final String TABLE_MED = "cooeyusermed";
	  public static final String TABLE_EMCONTACT = "cooeyemergcontact";
	  
	  public static final String COL_ID = "_id";
	  public static final String COLUMN_FNAME = "firstname";
	  public static final String COLUMN_LNAME = "lastname";
	  public static final String COLUMN_EMAIL = "email";
	  public static final String COLUMN_PH = "phoneNumber";
	  public static final String COLUMN_GENDER = "gender";
	  public static final String COLUMN_AREA = "area";
	  public static final String COLUMN_COUNTRY = "country";
	  public static final String COLUMN_CITY = "city";
	  public static final String COLUMN_DOB = "dob";
	  public static final String COLUMN_BGRP = "bloodGroup";
	  public static final String COLUMN_NICKN = "nickname";
	  public static final String COL_ISLOGGED = "isloggedin";
	  public static final String COL_PATIENTID = "patientid";
	  public static final String COL_ISATHLETE = "isAthlete";
	  public static final String COL_ATHLETELEVEL = "athleteLevel";
	  public static final String COL_GOALWT = "goalWeight";
	  public static final String COL_WAISTLINE = "waistline";
	  public static final String COL_ONBOARDWT = "onboardweight";
	  public static final String COL_PAR_IwD = "parentid";
	  public static final String COL_EXT_ID = "externalId";
	  public static final String COLUMN_PARENT = "parentId";
	  public static final String COLUMN_HT = "height";

  	//public static final String KEY_ID="_id";
  	public static final String USR_ID = "uid";
  	public static final String KEY_DEVICE_NAME="device_name";
	public static final String KEY_DEVICE_ADDRESS="device_address";
	public static final String KEY_DEVICE_TYPE="device_type";
	public static final String KEY_DEVICE_ID="device_id";
	public static final String KEY_DEVICE_SN="device_sn";
	public static final String KEY_DEVICE_PAIRFLAGS="device_pairflags";
	public static final String KEY_DEVICE_MODELNUMBER="device_modelnumber";
	public static final String KEY_DEVICE_PASSWORD="device_password";
	public static final String KEY_DEVICE_BROADCASTID="device_brocastID";
	public static final String KEY_DEVICE_SOFTWARE_VERSION="software_version";
	public static final String KEY_DEVICE_HARDWARE_VERSION="hardware_version";
	public static final String KEY_DEVICE_FIRMWARE_VERSION="firmware_version";
	public static final String KEY_DEVICE_MANUFACTURENAME="manufacture_name";
	public static final String KEY_DEVICE_SYSTEMID="device_systemID";
	public static final String KEY_DEVICE_MODEL="device_model";
	public static final String KEY_DEVICE_USER_NUMBER="user_number";
	public static final String KEY_DEVICE_USER_NAME="user_name";
	public static final String KEY_DEVICE_SERVICE_UUID="service_uuid";
	public static final String KEY_DEVICE_STATUS="device_status";
	public static final String KEY_MAX_USER_QUANTITY="max_user_number";
	public static final String KEY_PROTOCOL_TYPE="protocol_type";
	public static final String KEY_PAIRSTATUS = "pair_status";
		
	public static final String COL_SYS = "systolic";
	public static final String COL_DIA = "diastolic";
	public static final String COL_DATE = "date";
	public static final String COL_PUL = "pulse";
	
	//Weight data
	public static final String COL_WT = "weight";
	public static final String COL_MUS = "muscleratio";
	public static final String COL_BMI = "bmi";
	public static final String COL_FAT = "fatratio";
	public static final String COL_WTUNITS = "weightunits";
	public static final String COL_WTR = "waterratio";
	public static final String COL_IMP = "impedance";
	public static final String COL_BONE = "bonedensity";
	//Medicine data
	
	public static final String COL_MEDNAME = "medname";
	public static final String COL_MEDID = "medicineId";
	public static final String COL_BCOMP = "basecomponent";
	public static final String COL_DOSAGE = "dosage";
	public static final String COL_DOSAGE_UNIT = "dosageunit";
	public static final String COL_ISREMIND = "isreminder";
	public static final String COL_REMINDER = "reminders";
    public static final String COL_FREQ = "usagefreq";
    public static final String COL_MEDTYPE = "type";
	
	private static final String TABLE_WT_CREATE = "create table "
		      + TABLE_WT
		      + "(" 
		      + COL_ID + " integer primary key autoincrement, " 
		      + USR_ID + " integer, "
		      + COL_WT + " float(3,1), "
		      + COL_MUS + " float(3,2), "
		      + COL_BMI + " float(3,2), "
		      + COL_FAT + " float(3,2), "
		      + COL_WTUNITS + " text, "
		      + COL_WTR + " float(3,2), "
		      + COL_IMP + " float(3,2), "
		      + COL_BONE + " float(3,2), "
		      + COL_GOALWT + " float default 0.0, "
		      + COL_WAISTLINE + " float default 0.0, "
		      + COL_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP); ";
	
	  // Database creation sql statement
	  private static final String TABLE_MED_CREATE = "create table "
	      + TABLE_MED 
	      + "(" 
	      + COL_ID + " integer primary key autoincrement, " 
	      + USR_ID + " integer, "
	      + COL_MEDNAME + " text not null, "
	      + COL_BCOMP + " text, "
	      + COL_DOSAGE + " text, "
	      + COL_DOSAGE_UNIT + " text, "
	      + COL_ISREMIND + " integer default 0, "
	      + COL_REMINDER + " text, "
	      + COL_FREQ + " text, "
	      + COL_MEDTYPE + " text, "
	      + COL_MEDID + " text, "
	      + COL_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP); "; 
	  
	  private static final String TABLE_USR_CREATE = "create table "
		      + TABLE_USER
		      + "(" 
		      + COL_ID + " integer primary key autoincrement, " 
		      + COLUMN_FNAME + " text not null, "
		      + COLUMN_LNAME + " text, "
		      + COLUMN_GENDER + " text, "
		      + COLUMN_AREA + " text, "
		      + COLUMN_COUNTRY + " text, "
		      + COLUMN_CITY + " text, "
		      + COLUMN_DOB + " text, "
		      + COLUMN_BGRP + " text, "
		      + COLUMN_EMAIL + " text not null, "
		      + COLUMN_NICKN + " text not null, "
		      + COL_ISLOGGED + " integer default 0, "
		      + COL_PATIENTID + " integer default 0, "
		      + COLUMN_PH + " text not null, "
		      + COL_ISATHLETE + " integer default 0, "
		      + COL_ATHLETELEVEL + " integer default 0, "
		      + COL_ONBOARDWT + " float default 0.0, "
		      + COL_EXT_ID + " text, "
		      + COLUMN_PARENT + " integer, "
		      + COLUMN_HT + " float(4,2), "
		      + COL_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP); "; 
	  
	  private static final String TABLE_EMCONTACT_CREATE = " create table "
			  + TABLE_EMCONTACT
			  + "(" 
		      + COL_ID + " integer primary key autoincrement, " 
		      + COLUMN_PARENT + " integer, "
		      + COLUMN_EMAIL + " text not null, "
		      + COLUMN_PH + " text not null, "
		      + COLUMN_FNAME + " text not null, "
		      + COL_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ); "; 
	  
	  //TODO: profile created_on to be added
	  
	  private static final String TABLE_BP_CREATE = "create table "
			+ TABLE_BP
			+ "(" 
			+ COL_ID + " integer primary key autoincrement, " 
			+ USR_ID + " integer, "
			+ COL_SYS + " float(3,2), "
			+ COL_DIA + " float(3,2), "
			+ COL_PUL + " float(3,2), "
			+ COL_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP); ";
	  
	  
	  private static final String TABLE_DEV_CREATE = "create table " +
			TABLE_DEV 
			+ " (" +
			COL_ID + " integer primary key autoincrement, " +
			USR_ID + " integer, " +
			KEY_DEVICE_NAME + " text , " +
			KEY_DEVICE_ADDRESS+" text, "+
			KEY_DEVICE_TYPE+" text, "+			      
			KEY_DEVICE_ID + " text , " +
			KEY_DEVICE_SN + " text , " +
			KEY_DEVICE_PAIRFLAGS + " text , " +
			KEY_DEVICE_MODELNUMBER + " text , " +
			KEY_DEVICE_PASSWORD + " text , " +
			KEY_DEVICE_BROADCASTID + " text , " +
			KEY_DEVICE_SOFTWARE_VERSION + " text , " +
			KEY_DEVICE_HARDWARE_VERSION + " text , " +
			KEY_DEVICE_FIRMWARE_VERSION + " text , " +
			KEY_DEVICE_MANUFACTURENAME + " text , " +		     
			KEY_DEVICE_SYSTEMID + " text , " +
			KEY_DEVICE_MODEL + " text , " +
			KEY_DEVICE_USER_NUMBER + " int , " +
			KEY_PAIRSTATUS + " int, " +
			KEY_DEVICE_USER_NAME + " text , " +
			KEY_DEVICE_SERVICE_UUID + " text , " +
			KEY_PROTOCOL_TYPE + " text , " +
			KEY_MAX_USER_QUANTITY + " int , "
			+ KEY_DEVICE_STATUS + " text, "
			+ COL_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP); "; 
		
	  public CooeySQLHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }
	  
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_USR_CREATE);
		db.execSQL(TABLE_DEV_CREATE);
		db.execSQL(TABLE_BP_CREATE);
		db.execSQL(TABLE_WT_CREATE);
		db.execSQL(TABLE_MED_CREATE);
		db.execSQL(TABLE_EMCONTACT_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(CooeySQLHelper.class.getName(),
		        "Upgrading database from version " + oldVersion + " to "
		            + newVersion + ", which will destroy all old data");
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEV);
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_BP_CREATE);
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_WT_CREATE);
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_MED_CREATE);
		    db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMCONTACT_CREATE);
		    onCreate(db);

	}

}
