package com.biz.health.utils.db;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.biz.cooey.BPData;
import com.biz.cooey.WeightData;

public class CooeyWTDataSource {

	
	private SQLiteDatabase database;
	private CooeySQLHelper dbHelper;

	private String[] allColumns = { CooeySQLHelper.COL_ID,
			CooeySQLHelper.USR_ID, 
			CooeySQLHelper.COL_WT,
			CooeySQLHelper.COL_MUS, 
			CooeySQLHelper.COL_BMI,
			CooeySQLHelper.COL_FAT,
			CooeySQLHelper.COL_WTUNITS,
			CooeySQLHelper.COL_WTR,
			CooeySQLHelper.COL_IMP,
			CooeySQLHelper.COL_DATE,
			CooeySQLHelper.COL_BONE, CooeySQLHelper.COL_GOALWT, 
			CooeySQLHelper.COL_WAISTLINE, CooeySQLHelper.COL_DATE};

	public CooeyWTDataSource(Context context) {
		dbHelper = new CooeySQLHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	public void createWTForUser(WeightData wd, long uid){
	
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.USR_ID, uid);
		values.put(CooeySQLHelper.COL_WT, wd.getWeightKg());
		values.put(CooeySQLHelper.COL_MUS, wd.getMuscleMassRatio());
		values.put(CooeySQLHelper.COL_BMI, wd.getBmi());
		values.put(CooeySQLHelper.COL_FAT, wd.getBodyFatRatio());
		values.put(CooeySQLHelper.COL_WTUNITS, wd.getDeviceSelectedUnit());
		values.put(CooeySQLHelper.COL_WTR, wd.getBodyWaterRatio());
		values.put(CooeySQLHelper.COL_IMP, wd.getImp());
		values.put(CooeySQLHelper.COL_BONE, wd.getBoneDensity());
		//values.put(CooeySQLHelper.COL_DATE, wd.getDate());
		
		//TODO: what is the time on the device is wrong or not in sync. so add now()
		SimpleDateFormat DbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		SimpleDateFormat WtDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 
		try {
			Date tmp = WtDateFormat.parse(wd.getDate());
			values.put(CooeySQLHelper.COL_DATE, DbDateFormat.format(tmp));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

		long insertId = database.insert(CooeySQLHelper.TABLE_WT, null, values);
		Cursor cursor = database.query(CooeySQLHelper.TABLE_WT, allColumns,
				CooeySQLHelper.COL_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		cursor.close();		
	}
	
	public List<WeightData> getWTValuesForUser(long uid, String where, String orderby){
		
		List<WeightData> devList = new ArrayList<WeightData>();
		String sql = "";
		String whr = " WHERE " + CooeySQLHelper.USR_ID +"="+uid;
		if(where != null){
			whr += " AND " + where; 
		}
		if(orderby != null){
			 sql= "SELECT * FROM "+CooeySQLHelper.TABLE_WT + whr + " ORDER BY "+orderby +" ;";
		}
		else{
			sql= "SELECT * FROM "+CooeySQLHelper.TABLE_WT + whr + " ;";
		}
		Cursor cursor = database.rawQuery(sql, new String[] {});
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			WeightData prof = cursorToDevice(cursor);
			devList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return devList;		
	}
	
	public List<WeightData> getWTValuesForUser(long uid, String orderyby){
		List<WeightData> devList = new ArrayList<WeightData>();

		Cursor cursor = database.query(CooeySQLHelper.TABLE_WT, allColumns,
				CooeySQLHelper.USR_ID + "=" + uid, 
				null, null, null, orderyby);
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			WeightData prof = cursorToDevice(cursor);
			devList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return devList;		
	}
	
	private float get2DecimalFloat(float f){
		
		String s = String.format("%.2f", f);
		return Float.valueOf(s);		
	}
	
	public WeightData cursorToDevice(Cursor cursor) {
		WeightData dev = new WeightData();
		
		dev.setWeightKg(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_WT))));
		dev.setMuscleMassRatio(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_MUS))));
		dev.setBmi(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_BMI))));
		dev.setBodyFatRatio(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_FAT))));
		dev.setBodyWaterRatio(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_WTR))));
		dev.setImp(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_IMP))));
		dev.setBoneDensity(get2DecimalFloat(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_BONE))));
		dev.setDate(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_DATE)));
		dev.setDeviceSelectedUnit(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_WTUNITS)));
		return dev;
		
	}

	
}
