package com.biz.health.utils.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.biz.cooey.BPData;
import com.biz.cooey.WeightData;

public class CooeyBPDataSource {

	private SQLiteDatabase database;
	private CooeySQLHelper dbHelper;

	private String[] allColumns = { CooeySQLHelper.COL_ID,
			CooeySQLHelper.USR_ID, 
			CooeySQLHelper.COL_SYS,
			CooeySQLHelper.COL_DIA, 
			CooeySQLHelper.COL_PUL,
			CooeySQLHelper.COL_DATE};

	public CooeyBPDataSource(Context context) {
		dbHelper = new CooeySQLHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	private Date getDateTimeFromString(String d) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		try {  
		    Date date = format.parse(d);  
		    return date;
		} catch (java.text.ParseException e) {  
		    // TODO Auto-generated catch block  
		    e.printStackTrace();  
		}
		return new Date();
	}
	
	private String getStringFromDateTime(Date date){
		
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		String datetime = dateformat.format(date);
		return datetime;
	}
	public void createBPForUser(BPData bpd, long uid){
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.USR_ID, uid);
		values.put(CooeySQLHelper.COL_SYS, bpd.getSystolic());
		values.put(CooeySQLHelper.COL_DIA, bpd.getDiastolic());
		values.put(CooeySQLHelper.COL_PUL, bpd.getPulseRate());
		//values.put(CooeySQLHelper.COL_DATE, bpd.getDate());

		SimpleDateFormat DbDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		SimpleDateFormat BpDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 
		try {
			Date tmp = BpDateFormat.parse(bpd.getDate());
			values.put(CooeySQLHelper.COL_DATE, DbDateFormat.format(tmp));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		long insertId = database.insert(CooeySQLHelper.TABLE_BP, null, values);

		Cursor cursor = database.query(CooeySQLHelper.TABLE_BP, allColumns,
				CooeySQLHelper.COL_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		// CooeyProfile newComment = cursorToComment(cursor);
		cursor.close();
	}
	//String selectQuery = "SELECT  * FROM " + TABLE_TASKS + " WHERE "+ KEY_TIMESTAMP+">=date('now', '-1 day')";
	//date('now', 'start of day')
	//String sql = "SELECT * FROM myTable WHERE myDate >= date('now','-1 day')"; 
	// Cursor mycursor = db.rawQuery(sql);
	public List<BPData> getBPValuesForUser(long uid, String where, String orderby){
		
		List<BPData> devList = new ArrayList<BPData>();
		String sql = "";
		String whr = " WHERE " + CooeySQLHelper.USR_ID +"="+uid;
		if(where != null){
			whr += " AND " + where; 
		}
		//" WHERE date >= date('now','-1 day') "		
		if(orderby != null){
			 sql= "SELECT * FROM "+CooeySQLHelper.TABLE_BP + whr + " ORDER BY "+orderby +" ;";
		}
		else{
			sql= "SELECT * FROM "+CooeySQLHelper.TABLE_BP + whr + " ;";
		}
		Cursor cursor = database.rawQuery(sql, new String[] {});
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			BPData prof = cursorToBP(cursor);
			devList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return devList;
		
	}
	
	public List<BPData> getBPValuesForUser(long uid, String orderby){
		
		List<BPData> devList = new ArrayList<BPData>();

		Cursor cursor = database.query(CooeySQLHelper.TABLE_BP, allColumns,
				CooeySQLHelper.USR_ID + "=" + uid, 
				null, null, null, orderby);
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			BPData prof = cursorToBP(cursor);
			devList.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return devList;
		
	}
	
	public int getTotalBPEntriesForUser(long uid){
		
		Cursor cursor= database.rawQuery("SELECT COUNT (*) FROM " + CooeySQLHelper.TABLE_BP + " WHERE " + CooeySQLHelper.USR_ID + "=" + uid +";",
		         null);
		int count = 0;
		if(null != cursor){
		    if(cursor.getCount() > 0){
		      cursor.moveToFirst();    
		      count = cursor.getInt(0);
		    }
		    cursor.close();
		}
		return count;
	}
	
	public BPData cursorToBP(Cursor cursor) {
		BPData dev = new BPData();
		
		dev.setSystolic((int) cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_SYS)));
		dev.setDiastolic((int) cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_DIA)));
		dev.setDate(cursor.getString(cursor.getColumnIndex(CooeySQLHelper.COL_DATE)));
		dev.setPulseRate(cursor.getFloat(cursor.getColumnIndex(CooeySQLHelper.COL_PUL)));
		return dev;
		
	}

	
}
