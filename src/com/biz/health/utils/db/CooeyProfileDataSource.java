package com.biz.health.utils.db;

import java.util.ArrayList;
import java.util.List;

import com.biz.cooey.CooeyProfile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CooeyProfileDataSource {

	// Database fields
	private SQLiteDatabase database;
	private CooeySQLHelper dbHelper;
	private String[] allColumns = { CooeySQLHelper.COL_ID, CooeySQLHelper.COLUMN_NICKN,
			CooeySQLHelper.COLUMN_EMAIL, CooeySQLHelper.COLUMN_FNAME,
			CooeySQLHelper.COLUMN_PH, CooeySQLHelper.COLUMN_LNAME,
			CooeySQLHelper.COLUMN_GENDER, CooeySQLHelper.COLUMN_AREA,
			CooeySQLHelper.COLUMN_COUNTRY, CooeySQLHelper.COLUMN_CITY,
			CooeySQLHelper.COLUMN_DOB, CooeySQLHelper.COLUMN_BGRP,
			CooeySQLHelper.COL_ISLOGGED, CooeySQLHelper.COL_PATIENTID,
			CooeySQLHelper.COL_ISATHLETE, CooeySQLHelper.COL_ATHLETELEVEL,
			CooeySQLHelper.COL_ONBOARDWT, CooeySQLHelper.COLUMN_PH, 
			CooeySQLHelper.COL_EXT_ID, CooeySQLHelper.COLUMN_PARENT,
			CooeySQLHelper.COLUMN_HT};

    
	public CooeyProfileDataSource(Context context) {
		dbHelper = new CooeySQLHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	
	public CooeyProfile createProfile(CooeyProfile cp) {
		ContentValues values = new ContentValues();
		CooeyProfile newcp = null;
		values.put(CooeySQLHelper.COLUMN_FNAME, cp.getFirstName());
		values.put(CooeySQLHelper.COLUMN_LNAME, cp.getLastName());
		values.put(CooeySQLHelper.COLUMN_EMAIL, cp.getEmail());
		values.put(CooeySQLHelper.COLUMN_BGRP, cp.getBloodGroup());
		values.put(CooeySQLHelper.COLUMN_DOB, cp.getDob());
		values.put(CooeySQLHelper.COLUMN_GENDER, cp.getGender());
		values.put(CooeySQLHelper.COLUMN_PH, cp.getMobileNumber());
		values.put(CooeySQLHelper.COLUMN_AREA, cp.getLocationArea());
		values.put(CooeySQLHelper.COLUMN_COUNTRY, cp.getLocationCountry());
		values.put(CooeySQLHelper.COLUMN_CITY, cp.getLocationCity());
		values.put(CooeySQLHelper.COLUMN_NICKN, cp.getNickName());
		values.put(CooeySQLHelper.COL_PATIENTID, cp.getPatientId());
		values.put(CooeySQLHelper.COL_ISATHLETE, 0);
		values.put(CooeySQLHelper.COL_ATHLETELEVEL, 1);
		values.put(CooeySQLHelper.COL_ONBOARDWT, 0.0);
		values.put(CooeySQLHelper.COL_EXT_ID, cp.getExternalID());
		values.put(CooeySQLHelper.COLUMN_PARENT, cp.getParentId());
		values.put(CooeySQLHelper.COL_ISLOGGED, 0);
		values.put(CooeySQLHelper.COLUMN_HT, cp.getHeight());

		//CooeySQLHelper.COL_ISLOGGED is default 0

		long insertId = database
				.insert(CooeySQLHelper.TABLE_USER, null, values);

		Cursor cursor = database.query(CooeySQLHelper.TABLE_USER, allColumns,
				CooeySQLHelper.COL_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			newcp = cursorToProfile(cursor);
		}
		cursor.close();
		return newcp;
	}

	public List<CooeyProfile> getAllProfiles(long parentId) {
		List<CooeyProfile> profiles = new ArrayList<CooeyProfile>();

		Cursor cursor = null;
		if(parentId == -1){//all available profiles in DB
			cursor = database.query(CooeySQLHelper.TABLE_USER, allColumns,
					null, null, null, null, null);
		}else{
			cursor = database.query(CooeySQLHelper.TABLE_USER, allColumns,
					CooeySQLHelper.COLUMN_PARENT + "= "+String.valueOf(parentId), 
					new String[] {}, 
					null, null, null);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			CooeyProfile prof = cursorToProfile(cursor);
			profiles.add(prof);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return profiles;
	}

	public void setProfileLoggedInStatus(long uid){
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.COL_ISLOGGED, 0);
		
		int updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + ">= 0", null);
		if(updateId >=0){} // success
		values.put(CooeySQLHelper.COL_ISLOGGED, 1);
		updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + "=" + uid, null);
		if(updateId >= 0){}//success
		
	}
	
	public void resetProfileLoggedInStatus(long uid){
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.COL_ISLOGGED, 0);
		
		int updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + ">= 0", null);
		if(updateId >=0){} // success
		values.put(CooeySQLHelper.COL_ISLOGGED, 0);
		updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + "=" + uid, null);
		if(updateId >= 0){}//success
		
	}
	
	public void updateProfileWithPatient(long uid, int pid){
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.COL_PATIENTID, pid);
		
		int updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + "="+uid, null);
		if(updateId >=0){} // success
		
	}
	
	public void updateFullProfile(CooeyProfile cp){
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.COLUMN_HT, cp.getHeight());
		values.put(CooeySQLHelper.COLUMN_FNAME, cp.getFirstName());
		values.put(CooeySQLHelper.COLUMN_LNAME, cp.getLastName());
		values.put(CooeySQLHelper.COLUMN_BGRP, cp.getBloodGroup());
		values.put(CooeySQLHelper.COLUMN_DOB, cp.getDob());
		values.put(CooeySQLHelper.COLUMN_GENDER, cp.getGender());
		
		int updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + "="+ cp.getDbid(), null);
		if(updateId >=0){} // success
		
	}
	
	public void resetProfileLoggedInStatus(){
		
		ContentValues values = new ContentValues();
		values.put(CooeySQLHelper.COL_ISLOGGED, 0);
		
		int updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + ">= 0", null);
		if(updateId >=0){} // success
		//updateId = database.update(CooeySQLHelper.TABLE_USER, values, CooeySQLHelper.COL_ID + "=" + uid, null);
		//if(updateId >= 0){}//success
		
	}
	
	
	public CooeyProfile getLoggedInProfile(){
		
		Cursor cursor = database.query(CooeySQLHelper.TABLE_USER, allColumns,
				CooeySQLHelper.COL_ISLOGGED + "= 1", 
				new String[] {},
				null, null, null);
		cursor.moveToFirst();
		if(cursor.isAfterLast()){
			cursor.close();
			return null;//empty profile
		}
		CooeyProfile cp = cursorToProfile(cursor);
		cursor.close();
		return cp;
		
	}
	
	public CooeyProfile getProfile(long uid)
	{
		Cursor cursor = database.query(CooeySQLHelper.TABLE_USER, allColumns,
				CooeySQLHelper.COL_ID + "= " + uid, 
				new String[] {},
				null, null, null);
		cursor.moveToFirst();
		if(cursor.isAfterLast()){
			cursor.close();
			return null;//empty profile
		}
		CooeyProfile cp = cursorToProfile(cursor);
		cursor.close();
		return cp;
	}
	
	public CooeyProfile getProfile(String email){
		//email is like unique key from use space.
		Cursor cursor = database.query(CooeySQLHelper.TABLE_USER, allColumns,
				CooeySQLHelper.COLUMN_EMAIL + "=?", 
				new String[] {email},
				null, null, null);
		cursor.moveToFirst();
		if(cursor.isAfterLast()){
			cursor.close();
			return null;//empty profile
		}
		CooeyProfile cp = cursorToProfile(cursor);
		cursor.close();
		return cp;
	}
	
	private CooeyProfile cursorToProfile(Cursor cursor) {
		CooeyProfile prof = new CooeyProfile();
		// prof.setId(cursor.getLong(0));
		prof.setFirstName(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_FNAME)));
		prof.setLastName(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_LNAME)));
		prof.setEmail(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_EMAIL)));
		prof.setBloodGroup(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_BGRP)));
		prof.setDob(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_DOB)));
		prof.setGender(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_GENDER)));
		prof.setMobileNumber(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_PH)));
		prof.setLocationArea(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_AREA)));
		prof.setLocationCity(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_CITY)));
		prof.setLocationCountry(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_COUNTRY)));
		prof.setDbid(cursor.getLong(cursor
				.getColumnIndex(CooeySQLHelper.COL_ID)));
		prof.setNickName(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_NICKN)));
		prof.setIsloggedin(cursor.getInt(cursor
				.getColumnIndex(CooeySQLHelper.COL_ISLOGGED)));
		prof.setPatientId(cursor.getInt(cursor
				.getColumnIndex(CooeySQLHelper.COL_PATIENTID)));
		prof.setIsAthlete(cursor.getInt(cursor
				.getColumnIndex(CooeySQLHelper.COL_ISATHLETE)));
		prof.setAthleteLevel(cursor.getInt(cursor
				.getColumnIndex(CooeySQLHelper.COL_ATHLETELEVEL)));
		prof.setExternalID(cursor.getString(cursor
				.getColumnIndex(CooeySQLHelper.COL_EXT_ID)));
		prof.setParentId(cursor.getLong(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_PARENT)));
		prof.setHeight(cursor.getFloat(cursor
				.getColumnIndex(CooeySQLHelper.COLUMN_HT)));
		return prof;
	}

}
