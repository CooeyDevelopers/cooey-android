package com.biz.health.utils.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ImportExport {

	public static String APPDB_FILEPATH = "/data/data/com.biz.health.cooey_app/databases/cooeyprofile.db";
	public static String TO_FILEPATH = "/sdcard/data/cooeyprofile.db";
	private SQLiteDatabase database;
	private CooeySQLHelper dbHelper;
	
	public ImportExport(Context context) {
		dbHelper = new CooeySQLHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		database.close();
		dbHelper.close();
	}
	/**
	 * Copies the database file at the specified location over the current
	 * internal application database.
	 * */
	public boolean exportDatabase(String dbPath) throws IOException {

	    // Close the SQLiteOpenHelper so it will commit the created empty
	    // database to internal storage.
	    //close();
	    File toDb = new File(TO_FILEPATH);
	    File fromDb = new File(APPDB_FILEPATH);
	    if (fromDb.exists()) {
	        FileUtils.copyFile(new FileInputStream(fromDb), new FileOutputStream(toDb));
	        // Access the copied database so SQLiteHelper will cache it and mark
	        // it as created.
	        close();
	        return true;
	    }
	    return false;
	}
	
}
