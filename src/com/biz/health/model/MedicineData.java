package com.biz.health.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicineData {

	@Expose private String medicineId;
	@Expose @SerializedName("name") private String medicineName;
	private String baseComponent;
	@Expose private String dosage;
	private String frequency;
	private String reminders;
	private int isRemind = 0;
	@Expose private String dosage_unit;
	@Expose @SerializedName("intake") private String medicineType;
	@Expose private String tod;
	@Expose private String[] dosage_times;
	@Expose private String[] daysWeek; 
	@Expose @SerializedName("remind") private String remind;
	@Expose private Boolean active;
	
	private String date;
	private int dbId;
	
	public String getMedicineId() {
		return medicineId;
	}
	public void setMedicineId(String medicineId) {
		this.medicineId = medicineId;
	}
	public String getDosage_unit() {
		return dosage_unit;
	}
	public void setDosage_unit(String dosage_unit) {
		this.dosage_unit = dosage_unit;
	}
	public String getTod() {
		return tod;
	}
	public void setTod(String tod) {
		this.tod = tod;
	}
	public int getDbId() {
		return dbId;
	}
	public void setDbId(int dbId) {
		this.dbId = dbId;
	}
	public String getBaseComponent() {
		return baseComponent;
	}
	public String getDate() {
		return date;
	}
	
	public String getDosage() {
		return dosage;
	}
	public String getFrequency() {
		return frequency;
	}
	public int getIsRemind() {
		return isRemind;
	}
	public String getMedicineName() {
		return medicineName;
	}
	public String getMedicineType() {
		return medicineType;
	}
	public String getReminders() {
		return reminders;
	}
	public void setBaseComponent(String baseComponent) {
		this.baseComponent = baseComponent;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public void setIsRemind(int isRemind) {
		this.isRemind = isRemind;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	public void setMedicineType(String medicineType) {
		this.medicineType = medicineType;
	}
	public void setReminders(String reminders) {
		this.reminders = reminders;
	}
	public String[] getDosage_times() {
		return dosage_times;
	}
	public void setDosage_times(String[] dosage_times) {
		this.dosage_times = dosage_times;
	}
	public String getRemind() {
		return remind;
	}
	public void setRemind(String remind) {
		this.remind = remind;
	}
	public String[] getDaysWeek() {
		return daysWeek;
	}
	public void setDaysWeek(String[] daysWeek) {
		this.daysWeek = daysWeek;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
}
