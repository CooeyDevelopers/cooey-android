package com.biz.health.cooey_app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.biz.cooey.CooeyProfile;
import com.biz.cooey.SendJsonToCooeyTask;
import com.biz.health.servermodel.NewProfile;
import com.google.gson.GsonBuilder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class CooeyRegisterProgressDialog extends
		AsyncTask<String, Void, Boolean> {

	private NewProfile np = null;
	String extId = "0";
	private Context context;
	private String lastErrorMessage = "";
	private JSONObject successJo = null;

	public CooeyRegisterProgressDialog(Context ctx) {
		this.context = ctx;
		dialog = new ProgressDialog(ctx);
	}

	/** progress dialog to show user that the backup is processing. */
	private ProgressDialog dialog;

	protected void onPreExecute() {
		this.dialog.setMessage("Registering with Cooey");
		this.dialog.show();
		this.dialog.setCancelable(false);
	}

	@Override
	protected void onPostExecute(final Boolean success) {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		if (!success) {
			//Toast.makeText(context, "Failed to register\n" + lastErrorMessage,
					//Toast.LENGTH_SHORT).show();
		} else {

			// ((MainActivity)this.context).setuserOnBoarding();
			CooeyLoginProgressDialog loginTask;
			loginTask = new CooeyLoginProgressDialog(this.context, true);
			loginTask.setLoginParams(np.email, np.password);
			loginTask.execute();
		}
	}

	private Boolean createProfileWithNewRegistrationDetails(String responseBody) {

		((Activity) this.context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dialog.setMessage("Cooey is creating your profile, please wait.");
			}
		});

		String extId = "";
		try {
			JSONObject jo = new JSONObject(responseBody);
			extId = (String) jo.get("externalId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		final CooeyProfile cp = new CooeyProfile();
		cp.setEmail(np.email);
		cp.setFirstName(np.firstName);
		cp.setLastName(np.lastName);
		cp.setGender("");
		cp.setExternalID(extId);
		// cp.setDob(response.getBirthday() == "null" ? "1970-12-01" :
		// response.getAgeRange().toString());

		String mProfJson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation().create().toJson(cp);

		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
		HttpConnectionParams.setSoTimeout(httpParameters, 5000);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpResponse response;
		HttpPost request;
		String createProfresponseBody = "{}";

		try {
			request = new HttpPost(
					"http://api.cooey.co.in/ehealth/v1/profile/patient");
			StringEntity params = new StringEntity(mProfJson);
			request.addHeader("Content-Type", "application/json");
			request.setEntity(params);
			response = httpClient.execute(request);

			int responseCode = response.getStatusLine().getStatusCode();
			switch (responseCode) {
			case 200:
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					createProfresponseBody = EntityUtils.toString(entity);
				}
				// no need to take patientid here.
				return true;
			default:
				lastErrorMessage = "Error in profile creation after registration.";
				return false;
			}
		} catch (Exception ex) {
			httpClient.getConnectionManager().shutdown();
			lastErrorMessage = "Server exception";
			return false;
		}
	}

	private Boolean processRegistration(String url) {

		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
		HttpConnectionParams.setSoTimeout(httpParameters, 5000);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpResponse response;
		HttpPost request;
		String responseBody = "{}";
		String mProfJson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation().create().toJson(np);
		try {
			request = new HttpPost(url);
			StringEntity params = new StringEntity(mProfJson);
			request.addHeader("Content-Type", "application/json");
			request.setEntity(params);
			response = httpClient.execute(request);

			int responseCode = response.getStatusLine().getStatusCode();
			switch (responseCode) {
			case 200:
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					responseBody = EntityUtils.toString(entity);
				}
				String extId = "";
				try {
					JSONObject jo = new JSONObject(responseBody);
					extId = (String) jo.get("externalId");
				} catch (final JSONException e) {
					((Activity) context).runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									context,
									"Failed to start registration\n"
											+ e.getMessage(),
									Toast.LENGTH_SHORT).show();
						}
					});

					e.printStackTrace();
					return false;
				}
				if(extId.startsWith("coo_"))
					return createProfileWithNewRegistrationDetails(responseBody);
				else {
					((Activity) context).runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(
									context,
									"You are already registered.\nUse your email & password to login.\n"
											+ lastErrorMessage,
									Toast.LENGTH_SHORT).show();
						}
					});
					return false;
				}				
			default:
				lastErrorMessage = "Server error";
				return false;
			}
		} catch (Exception ex) {
			httpClient.getConnectionManager().shutdown();
			lastErrorMessage = "Server exception " + ex.getMessage();
			return false;
		}

	}

	@Override
	protected Boolean doInBackground(String... params) {
		String url = "http://manage.cooey.co.in/ehealth/v1/profile/cooey/account/";
		return processRegistration(url);
	}

	public void setRegistrationParams(String uname, String pass, String fname,
			String lname) {
		np = new NewProfile();
		np.email = uname;
		np.password = pass;
		np.firstName = fname;
		np.lastName = lname;
	}

}
