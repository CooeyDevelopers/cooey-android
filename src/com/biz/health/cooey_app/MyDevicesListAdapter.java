package com.biz.health.cooey_app;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.health.utils.image.*;
import com.lifesense.ble.bean.LsDeviceInfo;

public class MyDevicesListAdapter<T> extends ArrayAdapter<T> {

	private List<T> listDevices = null;
	private LayoutInflater inflater = null;
	private int layoutResource;
	private Context ctx;
	private static final int CORNER_RADIUS = 24; // dips
	private static final int MARGIN = 12;
	private float density;
	private Typeface QS_font;

	
	public MyDevicesListAdapter(Context context, int resource, List<T> objects) {
		super(context, resource, objects);
		ctx = context;
		listDevices = objects;
		layoutResource = resource;
		inflater = (LayoutInflater) context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		density = ctx.getResources().getDisplayMetrics().density;
		// TODO Auto-generated constructor stub
		QS_font = Typeface.createFromAsset(ctx.getAssets(),
				"fonts/Quicksand_Book.otf");
	}
	
	public int getCount() {
        return listDevices.size();
    }
 
    public T getItem(int position) {
        return listDevices.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
    
    public Bitmap getRoundedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth()+10, bitmap.getHeight()+10);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor( ((MainActivity)ctx).getResources().getColor(R.color.applight));
        canvas.drawOval(rectF, paint);
        //canvas.drawCircle(0, 0, 20, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
        
    }
    
    private static class BPMeterOnClickListener implements OnClickListener{
    	private Context ctx;
    	BPMeterOnClickListener(Context _ctx){
    		ctx = _ctx;
    	}
		@Override
		public void onClick(View v) {
			
		}
    	
    }
    private final BPMeterOnClickListener mybpm = new BPMeterOnClickListener(ctx);
    
	@SuppressWarnings("deprecation")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		
		Holder holder;
		//view.setBackground(d);
		if(convertView == null)
		{		
			holder = new Holder();
			convertView =inflater.inflate(layoutResource, parent, false);
			holder.imgView = (ImageView) convertView.findViewById(R.id.devImg);
			LsDeviceInfo dev = (LsDeviceInfo)getItem(position);
			//StreamDrawable d = new StreamDrawable(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.scale1),
			//		(int) (CORNER_RADIUS * density + 0.5f),
			//		(int) (MARGIN * density + 0.5f));
			//holder.imgView.setBackgroundResource(R.drawable.arlm_clk);
			//holder.imgView.setImageDrawable(d);
			//BitmapDrawable bitmapDrawable = ((BitmapDrawable) holder.imgView.getDrawable());
			//Bitmap bmp = bitmapDrawable.getBitmap();
			
			//holder.imgView.setImageResource(R.drawable.scale1);
			holder.text = (TextView) convertView.findViewById(R.id.devName);
			holder.text.setTypeface(QS_font);
			holder.text.setText(dev.getDeviceName());
			
			holder.text2 = (TextView) convertView.findViewById(R.id.devManName);
			holder.text2.setText("Cooey Device");
			holder.text2.setTypeface(QS_font);
			
			holder.text3 = (TextView) convertView.findViewById(R.id.devType);
			if(dev.getDeviceType() != null){
				String tmp = CooeyBleDeviceManager.deviceTypeToName(dev.getDeviceType());
				if(tmp.contains("WEIGHT")){
					//holder.imgView.setImageBitmap(getRoundedBitmap(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.scale1)));
					holder.imgView.setBackgroundResource(R.drawable.scale1);
					
				}else{
					//holder.imgView.setImageBitmap(getRoundedBitmap(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.bp)));
					holder.imgView.setBackgroundResource(R.drawable.bp);
				}
				holder.text3.setText(tmp);
			}
			holder.text3.setTypeface(QS_font);
			if(dev.getProtocolType().contains("A3")){
				holder.text4 = (TextView) convertView.findViewById(R.id.devUsrNum);
				holder.text4.setText(Html.fromHtml("Measure using profile <B>button " + String.valueOf(dev.getDeviceUserNumber()) + "</B>"));
				holder.text4.setVisibility(parent.VISIBLE);
				holder.text4.setTypeface(QS_font);
			}
			holder.tv_addedon = (TextView)convertView.findViewById(R.id.devaddOn);
			holder.tv_addedon.setTypeface(QS_font);
			
			
			holder.readbtn = (Button)convertView.findViewById(R.id.readDeviceBtn);
			holder.readbtn.setPaintFlags(holder.readbtn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			//convertView.setBackgroundDrawable(d);
			convertView.setTag(holder);
		}
		else {           
            holder = (Holder) convertView.getTag();
		}
		return convertView;
		
	}

	class Holder
	{
		public TextView text = null;
		public TextView text2 = null;
		public TextView text3 = null;
		public TextView text4 = null;
		public TextView tv_addedon = null;
		public ImageView imgView = null;
		public Button readbtn = null;
	}
}
