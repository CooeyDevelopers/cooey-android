package com.biz.health.cooey_app;

import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.biz.cooey.CooeyProfile;
//import com.google.android.gms.internal.ct;
//import com.google.android.gms.internal.fp;
//import com.facebook.widget.ProfilePictureView;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;

public class OnBoarding extends Fragment {

	private SimpleFacebook mSimpleFacebook;
	private CooeyProfile activeCp;
	ArrayAdapter<String> cia = null;
	private Profile fbProfileResp = null;
	private double lat, lon;
	private Boolean isParent = false, isNewChild = false;
	private Boolean editBtnEnabled = false;
	private int childlId = 0;
	private View rootView = null;
	private ResponseProfile customLoginProfile = null;
	private TextView tv_dob;
	private EditText tv_email, tv_gender, tv_name;
	private Spinner sp_bgrp, sp_htft, sp_htin, sp_gender;
	ArrayAdapter<String> genadapter = null;

	public OnBoarding(Boolean isPar, Boolean isNewCh, int id) {
		isParent = isPar;
		isNewChild = isNewCh;
		childlId = id;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		getActivity().getActionBar().setTitle("WELCOME");
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().getActionBar().setTitle("WELCOME");
	}

	private Properties getProfileProperties() {
		PictureAttributes pictureAttributes = Attributes
				.createPictureAttributes();
		pictureAttributes.setType(PictureType.SQUARE);
		pictureAttributes.setHeight(500);
		pictureAttributes.setWidth(500);

		Properties properties = new Properties.Builder().add(Properties.ID)
				.add(Properties.FIRST_NAME)
				.add(Properties.PICTURE, pictureAttributes)
				.add(Properties.LAST_NAME).add(Properties.BIRTHDAY)
				.add(Properties.AGE_RANGE).add(Properties.EMAIL)
				.add(Properties.GENDER).build();
		return properties;
	}

	private ArrayList<String> getCountryValues() {

		Locale[] locales = Locale.getAvailableLocales();
		ArrayList<String> countries = new ArrayList<String>();
		for (Locale locale : locales) {
			String country = locale.getDisplayCountry();
			if (country.trim().length() > 0 && !countries.contains(country)) {
				countries.add(country);
			}
		}
		countries.add("India");
		Collections.sort(countries);
		countries.add(0, "India");
		return countries;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.login, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	public class countryOnClickListener implements OnItemSelectedListener {

		private JsonObject jo;
		private ArrayList<String> cities;

		public countryOnClickListener(JsonObject _jo, ArrayList<String> _area) {
			jo = _jo;
			cities = _area;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if (arg1 == null)
				return;
			TextView tv = (TextView) arg1.findViewById(android.R.id.text1);

			JsonElement je = jo.get((String) tv.getText());
			if (je != null) {
				cities.clear();
				JsonArray ja = je.getAsJsonArray();

				for (int i = 0; i < ja.size(); ++i) {
					cities.add(ja.get(i).toString().replace("\"", ""));
				}
				Collections.sort(cities);
				cia.notifyDataSetChanged();
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}

	}

	/*
	 * //open the inputStream to the file InputStream inputStream =
	 * getAssets().open("chem_elements.json");
	 * 
	 * int sizeOfJSONFile = inputStream.available();
	 * 
	 * //array that will store all the data byte[] bytes = new
	 * byte[sizeOfJSONFile];
	 * 
	 * //reading data into the array from the file inputStream.read(bytes);
	 * 
	 * //close the input stream inputStream.close();
	 * 
	 * JSONString = new String(bytes, "UTF-8"); JSONObject = new
	 * JSONObject(JSONString);
	 */
	public static JsonObject convertFileToJSON(InputStreamReader isr) {

		// Read from File to String
		JsonObject jsonObject = new JsonObject();
		JsonParser parser = new JsonParser();
		// JsonElement jsonElement = parser.parse(new FileReader(fileName));
		JsonElement jsonElement = parser.parse(isr);
		jsonObject = jsonElement.getAsJsonObject();
		return jsonObject;
	}

	private void getLocation() {
		// Get the location manager
		LocationManager locationManager = (LocationManager) getActivity()
				.getSystemService(getActivity().LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		List<String> providers = locationManager.getProviders(criteria, false);
		// List<String> providers = locationManager.getAllProviders();
		Location location = null;
		for (int i = providers.size() - 1; i >= 0; i--) {
			// if(providers.get(i).contains("gps")) continue; //only coarse
			location = locationManager.getLastKnownLocation(providers.get(i));
			if (location != null)
				break;
		}

		// String bestProvider = locationManager.getBestProvider(criteria,
		// false);
		// location = locationManager.getLastKnownLocation(bestProvider);
		try {
			lat = location.getLatitude();
			lon = location.getLongitude();
		} catch (NullPointerException e) {
			lat = -1.0;
			lon = -1.0;
		}
	}

	private static final int GET_PHONE_NUMBER = 3007;

	public void getContact() {
		// getActivity().startActivityForResult(new Intent(getActivity(),
		// ContactsPickerActivity.class), GET_PHONE_NUMBER);
	}

	private void enableViewForEdit(Boolean flag) {
		sp_gender.setEnabled(flag);
		tv_email.setEnabled(flag);
		tv_dob.setEnabled(flag);
		sp_htft.setEnabled(flag);
		sp_htin.setEnabled(flag);
		sp_bgrp.setEnabled(flag);
	}

	private void prePopulateFields() {

		tv_name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);
		tv_name.setText(activeCp.getFirstName() + " "
				+ activeCp.getLastName());
		tv_dob.setText(activeCp.getDob());
		tv_email.setText(activeCp.getEmail());
		String gender = activeCp.getGender().equalsIgnoreCase("") ? "NA"
				: activeCp.getGender();
		sp_gender.setSelection(genadapter.getPosition(gender.toUpperCase()),
				true);
		int ft = (int) (activeCp.getHeight() / 30.48);		
		int in = (int) Math.round(((activeCp.getHeight() % 30.48) * 0.393701));
		if(ft > 8) ft = 2; //becomes 0
		if(in  == 12) {ft += 1; in = 0; }
		if(in > 12) in = 0;
		sp_htft.setSelection(ft -2, true);
		sp_htin.setSelection(in, true);
		enableViewForEdit(false);
	}

	

	private Boolean validateFields() {

		if (tv_email.getText().toString().length() == 0
				|| !tv_email.getText().toString().contains("@")) {
			Toast.makeText(getActivity(), "Email is required",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (tv_name.getText().toString().length() < 3) {
			Toast.makeText(getActivity(), "Name is required",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (Integer.valueOf(sp_htft.getSelectedItem().toString()) == 0) {
			Toast.makeText(getActivity(), "Enter a valid height (ft)",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (tv_dob.getText().toString().length() > 10) {
			Toast.makeText(getActivity(), "Invalid DOB, require yyyy-mm-dd.",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		
		String dob = tv_dob.getText().toString();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d;
		try {
			d = sdf.parse(dob);
			SimpleDateFormat to = new SimpleDateFormat("yyyy-MM-dd");
			tv_dob.setText(to.format(d));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
				 
		return true;

	}

	private CooeyProfile getProfileFromFields() {

		String[] tmp = tv_name.getText().toString().split(" ");
		if (tmp.length != 2)
			activeCp.setLastName("");
		activeCp.setFirstName(tmp[0]);
		activeCp.setEmail(tv_email.getText().toString());
		activeCp.setBloodGroup(sp_bgrp.getSelectedItem().toString());
		activeCp.setGender(sp_gender.getSelectedItem().toString());

		activeCp.setDob(tv_dob.getText().toString().isEmpty() ? "1970-01-01"
				: tv_dob.getText().toString());
		String inches = sp_htin.getSelectedItem().toString();
		
		float ft = (float) Integer.valueOf((String) sp_htft.getSelectedItem());
		ft *= 30.48;
		
		float in = (float)(Integer.valueOf(inches));
		in *= 2.54;
		
		//Float fh = (float) ((float) Integer.valueOf(sp_htft.getSelectedItem()
		//		.toString()) * 1.0) + (Integer.valueOf(inches) / 100f);
		
		activeCp.setHeight(ft + in); //goes in cms.
		return activeCp;

	}

	/*
	 * For Adding a child profile, Profile API POST is called to create a new
	 * profile
	 */
	private void configureNewUserProfile() {

		enableViewForEdit(true);
		tv_name.setEnabled(true);

		((ImageView) rootView.findViewById(R.id.imgeditprof))
				.setVisibility(View.INVISIBLE);
		Button btnProfSave = (Button) rootView.findViewById(R.id.profBtnNxt);
		btnProfSave.setText("Save");
		
		((Button) rootView.findViewById(R.id.profBtnNxt))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (!validateFields()) {
					return;
				}
				((MainActivity) getActivity())
						.addChildProfile(getProfileFromFields());
				// TODO: add progress dialog.
			}
		});
	}

	private void configureShowUserProfile() {

	}

	private void populateFBProfileResponse() {
		final ProfilePictureView pv = (ProfilePictureView) rootView
				.findViewById(R.id.profView);
		mSimpleFacebook.getProfile(getProfileProperties(),
				new OnProfileListener() {

					@Override
					public void onComplete(Profile response) {
						fbProfileResp = response;
						pv.setCropped(true);
						pv.setPresetSize(ProfilePictureView.CUSTOM);
						pv.setProfileId(response.getId());
						super.onComplete(response);
					}

					@Override
					public void onFail(String reason) {
						super.onFail(reason);
						Toast.makeText(getActivity(), reason,
								Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onThinking() {
						super.onThinking();
					}
				});
	}

	private void goToDashboard() {

		getFragmentManager()
				.beginTransaction()
				.replace(R.id.container,
						((MainActivity) getActivity()).getDashboard(), "")
				.addToBackStack(null).commit();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.onboard, container, false);

		tv_name = (EditText) rootView.findViewById(R.id.profName);
		tv_dob = (TextView) rootView.findViewById(R.id.profDOB);
		tv_email = (EditText) rootView.findViewById(R.id.profemail);

		sp_bgrp = (Spinner) rootView.findViewById(R.id.profBgrp);
		sp_htft = (Spinner) rootView.findViewById(R.id.profhtft);
		sp_htin = (Spinner) rootView.findViewById(R.id.profhtin);
		sp_gender = (Spinner) rootView.findViewById(R.id.spnrprofgend);
		ImageView editBtn = (ImageView) rootView.findViewById(R.id.imgeditprof);

		mSimpleFacebook = SimpleFacebook.getInstance();

		activeCp = ((MainActivity) getActivity()).getActiveProfile();
		customLoginProfile = ((MainActivity) getActivity())
				.getActiveResponseProfile();

		ImageView addfirstcontact = (ImageView) rootView
				.findViewById(R.id.imgbtnaddfirst);
		addfirstcontact.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getContact();
			}
		});

		ArrayAdapter<String> ba = new ArrayAdapter<String>(getActivity()
				.getApplicationContext(), R.layout.spinner_cooey_dropdown_item,
				getResources().getStringArray(R.array.blood_groups));
		sp_bgrp.setAdapter(ba);

		ArrayAdapter<String> htfta = new ArrayAdapter<String>(getActivity()
				.getApplicationContext(), R.layout.spinner_cooey_dropdown_item,
				getResources().getStringArray(R.array.ht_ft));
		sp_htft.setAdapter(htfta);

		ArrayAdapter<String> htina = new ArrayAdapter<String>(getActivity()
				.getApplicationContext(), R.layout.spinner_cooey_dropdown_item,
				getResources().getStringArray(R.array.ht_int));
		sp_htin.setAdapter(htina);

		genadapter = new ArrayAdapter<String>(getActivity()
				.getApplicationContext(), R.layout.spinner_cooey_dropdown_item,
				getResources().getStringArray(R.array.genders));
		sp_gender.setAdapter(genadapter);

		getLocation();

		

		if (isParent) {
			prePopulateFields();
			if (mSimpleFacebook.isLogin()) {					
				populateFBProfileResponse();
			} else {
				// nothing ignore.
			}
		} else {
			configureShowUserProfile();
		}
		

		if (((MainActivity) getActivity()).isUserOnboarding()) {
			// always parent profile, child profiles don't get onboarded.
			enableViewForEdit(true);
			editBtn.setOnClickListener(null);
			tv_email.setEnabled(false);

			setHasOptionsMenu(false);
			
			((Button) rootView.findViewById(R.id.profBtnNxt)).setText("Next");
			((Button) rootView.findViewById(R.id.profBtnNxt))
					.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							if (!validateFields()) {
								return;
							}
							if (customLoginProfile != null) {

								((MainActivity) getActivity())
										.updateCurrentProfile(
												getProfileFromFields(), false);

							}
							if (mSimpleFacebook.isLogin()) {

								activeCp.setNickName("nickname not given");
								activeCp.setEmail(fbProfileResp.getEmail());
								activeCp.setFirstName(fbProfileResp
										.getFirstName());
								activeCp.setLastName(fbProfileResp
										.getLastName());
								activeCp.setDob(fbProfileResp.getBirthday());
							}
							((MainActivity) getActivity())
									.updateActiveProfile(activeCp);
							Fragment f = ((MainActivity) getActivity())
									.getNextFlowOnBoard();
							getFragmentManager().beginTransaction()
									.replace(R.id.container, f)
									.addToBackStack(null).commit();
						}
					});

		} else {
			setHasOptionsMenu(true);
			// default, before the user selects edit button
			Button nxtback = ((Button) rootView.findViewById(R.id.profBtnNxt));
			nxtback.setText("Back");
			nxtback.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							goToDashboard();
						}
					});
			nxtback.setBackgroundResource(R.drawable.lt2brdvwdark2);
			nxtback.setTextColor( getActivity().getResources().getColor(R.color.applt3));
			
			editBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Button nxtback = ((Button) rootView.findViewById(R.id.profBtnNxt));
					
					if (editBtnEnabled) {
						editBtnEnabled = false;
						enableViewForEdit(false);
						
						nxtback.setText("Back");
						nxtback.setBackgroundResource(R.drawable.lt2brdvwdark2);
						nxtback.setTextColor( getActivity().getResources().getColor(R.color.applt3));
						
						((Button) rootView.findViewById(R.id.profBtnNxt))
								.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {
										goToDashboard();
									}
								});
						return;
					}
					if (!((MainActivity) getActivity()).haveNetworkConnection()) {
						Toast.makeText(getActivity(),
								"Profile update requires internet connection.",
								Toast.LENGTH_SHORT).show();
						return;
					}
					editBtnEnabled = true;
					enableViewForEdit(true);
					
					nxtback.setText("Save");
					nxtback.setBackgroundResource(R.color.greenreading);
					nxtback.setTextColor(((MainActivity) getActivity()).getResources().getColor(R.color.Black));
					if (isParent) {
						tv_email.setEnabled(false);
						nxtback.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {

										if (!validateFields()) {
											return;
										}
										((MainActivity) getActivity())
												.updateCurrentProfile(
														getProfileFromFields(),
														true);
									}

								});
					} else if (isNewChild) {
						nxtback.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {

										if (!validateFields()) {
											return;
										}
										((MainActivity) getActivity())
												.addChildProfile(getProfileFromFields());
										// TODO: add progress dialog.
									}
								});

					}
				}
			});
		}

		if (isNewChild) {
			configureNewUserProfile();
		}
		tv_dob.setOnClickListener(new OnClickListener() {
			
			final Calendar c = Calendar.getInstance();
			final int mYr = c.get(Calendar.YEAR);
            final int mMn = c.get(Calendar.MONTH);
            
			@Override
			public void onClick(View v) {
				
				DatePickerDialog tpd = null;
				
	            tpd = new DatePickerDialog(getActivity(), 
	            		new DatePickerDialog.OnDateSetListener() {
					
							@Override
							public void onDateSet(DatePicker view, int year, int monthOfYear,
									int dayOfMonth) {
								int x = 0;
								tv_dob.setText(String.valueOf(year) + "-" 
												+ String.valueOf(monthOfYear) + "-"
												+ String.valueOf(dayOfMonth));								
							}
							
						}, mYr, mMn, c.get(Calendar.DAY_OF_MONTH));
	            tpd.show();				
			}
		});
		return rootView;

		/*
		 * ArrayList<String> area = new ArrayList<String>(); ArrayList<String>
		 * cities = new ArrayList<String>(); ArrayAdapter<String> ca = new
		 * ArrayAdapter<String>(getActivity().getApplicationContext(),
		 * R.layout.spinner_cooey_dropdown_item, getCountryValues());
		 * sp_country.setAdapter(ca); sp_country.setOnItemSelectedListener(new
		 * countryOnClickListener(jo, cities));
		 * 
		 * 
		 * 
		 * area.add("Indiranagar"); area.add("Jayanagar");
		 * 
		 * ArrayAdapter<String> aa = new
		 * ArrayAdapter<String>(getActivity().getApplicationContext(),
		 * R.layout.spinner_cooey_dropdown_item, area); sp_area.setAdapter(aa);
		 * 
		 * cities.add("Bangalore"); cities.add("Chennai");
		 * 
		 * cia = new ArrayAdapter<String>(getActivity().getApplicationContext(),
		 * R.layout.spinner_cooey_dropdown_item, cities);
		 * sp_city.setAdapter(cia);
		 */
		// getLocation();

		// pv.set
		// setHasOptionsMenu(false);
		// pv.setProfileId(SimpleFacebook.getInstance().getProfile(properties,
		// onProfileListener);
		// return rootView;
	}

}
