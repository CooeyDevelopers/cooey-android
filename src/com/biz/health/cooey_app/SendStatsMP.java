package com.biz.health.cooey_app;

import java.util.HashMap;

import org.json.JSONObject;

import android.content.Context;

import com.biz.cooey.CooeyProfile;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class SendStatsMP {
	
	private static final String MIXPANEL_TOKEN = "d054b2d9c0bd9bd58a5cac9708a8b0ed";
	private static SendStatsMP _instance = null;
	MixpanelAPI mMixpanel = null;
	private Context context;
	
	private SendStatsMP(Context ctx){
		mMixpanel = MixpanelAPI.getInstance(ctx, MIXPANEL_TOKEN);
		context = ctx;
	}
	
	public static SendStatsMP getInstanceWithContext(Context ctx){
		if(_instance == null){
			_instance = new SendStatsMP(ctx);
		}
		return _instance;
	}
	
	public MixpanelAPI getMixpanel(){
		return mMixpanel;
	}
	
	public void sendStats(String evName, HashMap<String, Object> map){
		
		CooeyProfile tmp = ((MainActivity) context).getActiveProfile();
		if(tmp!=null){
			HashMap<String, Object> mphmap = new HashMap<String, Object>();	
			//set patientid once for every event
			mphmap.put("patientId", tmp.getPatientId());
			mMixpanel.registerSuperPropertiesOnce( new JSONObject(mphmap));
		}
		JSONObject props = new JSONObject(map);
		mMixpanel.track(evName, props);		
	}
	
	public void flush(){
		mMixpanel.flush();
	}
	
	public String getId(){
		return mMixpanel.getDistinctId();
	}
}
