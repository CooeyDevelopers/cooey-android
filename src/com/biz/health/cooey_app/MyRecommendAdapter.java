package com.biz.health.cooey_app;

import java.util.List;

import com.biz.health.cooey_app.MyMedicinesAdapter.Holder;
import com.biz.health.model.MedicineData;
import com.biz.health.model.RecommendationData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MyRecommendAdapter<T> extends ArrayAdapter<T> {

	private List<T> listRecs = null;
	private LayoutInflater inflater = null;
	private int layoutResource;
	private Context ctx;
	private Typeface QS_font, QS_fontbook;
	private static final int CORNER_RADIUS = 24; // dips
	private static final int MARGIN = 12;
	
	
	public MyRecommendAdapter(Context context, int resource, List<T> objects) {
		super(context, resource, objects);
		ctx = context;
		listRecs = objects;
		layoutResource = resource;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		QS_font = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Light.otf");
		QS_fontbook =Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Book.otf");
	}

	public int getCount() {
        return listRecs.size();
    }
 
    public T getItem(int position) {
        return listRecs.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

    	Holder holder;
		//view.setBackground(d);
		if(convertView == null)
		{		
			holder = new Holder();
			convertView =inflater.inflate(layoutResource, parent, false);
			
			final RecommendationData rec = (RecommendationData)getItem(position);
			/*
			holder.txtreccat = (TextView) convertView.findViewById(R.id.txtreccat);
			holder.txtreccat.setText(rec.getCategory());
			holder.txtreccat.setTypeface(QS_font);
			
			holder.txtrectype = (TextView) convertView.findViewById(R.id.txtrectype);
			holder.txtrectype.setText(rec.getType());
			holder.txtrectype.setTypeface(QS_font);
			
			holder.btnhide = (ImageButton) convertView.findViewById(R.id.btnreclsthide);
			*/
			
			holder.txtrectext = (TextView) convertView.findViewById(R.id.txtrectext);
			holder.txtrectext.setText(rec.getText());
			holder.txtrectext.setTypeface(QS_fontbook);
			
			holder.sharerec = (ImageView)convertView.findViewById(R.id.imgvwrecshare);
			holder.sharerec.setVisibility(View.VISIBLE);
			holder.sharerec.setOnClickListener(new OnClickListener() {
				String recommendTxt = rec.getText();
				@Override
				public void onClick(View v) {
					String header = "Hey, I would like to share this Tip from Cooey, with you.\n\n" + recommendTxt + "\n http://cooey.co ";
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, header);
					sendIntent.setType("text/*");
					ctx.startActivity(Intent.createChooser(sendIntent, "Share this tip with.."));					
				}
			});
			
			holder.likrec = (ImageView)convertView.findViewById(R.id.imgvwreclike);
			holder.likrec.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					v.setOnClickListener(null);
					v.setBackgroundResource(R.drawable.likerecdone);
					Toast.makeText(ctx, "Liked on Cooey!", Toast.LENGTH_SHORT).show();
				}
			});	
			
			convertView.setTag(holder);
			
		}else{
			holder = (Holder) convertView.getTag();
		}		
		return convertView;
    }
	
    class Holder
	{
		public TextView txtreccat = null;
		public TextView txtrectext = null;
		public TextView txtrectype = null;
		public ImageButton btnhide = null;
		public ImageView sharerec, likrec;
	}
    
}
