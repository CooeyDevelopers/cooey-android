package com.biz.health.cooey_app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;

import com.biz.cooey.CooeyProfile;

public class StatsUtil {

	
	private static HashMap<String, Object> build_stat_map(Context mainActvty){
		HashMap<String, Object> mphmap = new HashMap<String, Object>();	
		CooeyProfile cp = ((MainActivity) mainActvty).getActiveProfile();
		if( cp != null){						
			//mphmap.put("patientId", cp.getPatientId());
			mphmap.put("email", cp.getEmail());
			mphmap.put("evttime", new Date().toString());
			mphmap.put("newlocaluser", false);
		}else{
			mphmap.put("evttime", new Date().toString());
			mphmap.put("newlocaluser", true);
			mphmap.put("patientId", "");
			mphmap.put("email", "");
		}
		return mphmap;
	}
	
	public static void stat_FBLoginAttempt(Context ctx){		
		//MP Track		
		SendStatsMP.getInstanceWithContext(ctx).sendStats("FBLoginScr", build_stat_map(ctx));				
	}
	public static void stat_FBLoginSuccess(Context ctx){
		SendStatsMP.getInstanceWithContext(ctx).sendStats("FBLoginScrSuccess", build_stat_map(ctx));
	}
	
	public static void stat_FBLogout(Context ctx){
		SendStatsMP.getInstanceWithContext(ctx).sendStats("FBLogout", build_stat_map(ctx));
	}
	
	public static void stat_CustomLogout(Context ctx){
		SendStatsMP.getInstanceWithContext(ctx).sendStats("CustomLogout", build_stat_map(ctx));
	}
	
	private static SimpleDateFormat getDateTimeFormat(){
		return new SimpleDateFormat("MM-dd_HH:mm:ss");
	}
	
	public static void stat_takeWeightReading(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkTakeWgReading", mphmap);
	}
	
	public static void stat_takeBPReading(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();	
		//mphmap.put("patientId", ((MainActivity) ctx).getActiveProfile().getPatientId());
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkTakeBpReading", mphmap);
	}
	
	public static void stat_triedBuyWeight(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();	
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkBuyWeight", mphmap);
	}
	
	public static void stat_triedBuyBp(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkBuyBp", mphmap);
	}
	
	public static void stat_triedAddMed(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();	
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkAddMed", mphmap);
	}
	
	public static void stat_triedAddDev(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkAddDev", mphmap);
	}
	
	public static void stat_triedDelMed(Context ctx){
		
		HashMap<String, Object> mphmap = new HashMap<String, Object>();	
		mphmap.put("evttime", getDateTimeFormat().format(new Date()));
		SendStatsMP.getInstanceWithContext(ctx).sendStats("ClkDelMed", mphmap);
	}
	
}
