package com.biz.health.cooey_app;

public class FragmentToActivityPayload {

	private String fragment_name;

	public String getFragment_name() {
		return fragment_name;
	}

	public void setFragment_name(String fragment_name) {
		this.fragment_name = fragment_name;
	}

}
