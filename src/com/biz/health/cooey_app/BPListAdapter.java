package com.biz.health.cooey_app;

import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.biz.cooey.BPData;
import com.biz.cooey.WeightData;
import com.biz.health.cooey_app.MyMedicinesAdapter.Holder;

public class BPListAdapter<T> extends ArrayAdapter<T> {

	private List<T> listBps = null;
	private LayoutInflater inflater = null;
	private int layoutResource;
	private Context ctx;
	
	public BPListAdapter(Context context, List<T> objects) {
		super(context, R.layout.darklistitem, objects);
		ctx = context;
		listBps = objects;
		layoutResource = R.layout.darklistitem;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public int getCount() {
        return listBps.size();
    }
 
    public T getItem(int position) {
        return listBps.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
	
      
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null)
		{		
			convertView =inflater.inflate(layoutResource, parent, false);

			BPData bp = (BPData)getItem(position);
			((TextView) convertView.findViewById(R.id.txtval)).setText(bp.getSystolic() + " / " + bp.getDiastolic() + "  ["+bp.getPulseRate()+"]");
			
			((TextView) convertView.findViewById(R.id.txtvaltime)).setText(bp.getDate());
		}
		return convertView;
    }
    
}
