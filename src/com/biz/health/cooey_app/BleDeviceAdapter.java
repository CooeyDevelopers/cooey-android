package com.biz.health.cooey_app;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifesense.ble.bean.LsDeviceInfo;


@SuppressWarnings("rawtypes")
public class BleDeviceAdapter extends ArrayAdapter {

	  private ArrayList<LsDeviceInfo> modelsArrayList;
	  private Context context;
	  @SuppressWarnings("unchecked")
	public BleDeviceAdapter(Context context, ArrayList<LsDeviceInfo> modelsArrayList) {
		
	         super(context, R.layout.adddevadapteritems, modelsArrayList);

	         this.context = context;
	         this.modelsArrayList = modelsArrayList;
	     }
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView=null;
        if(modelsArrayList.size()!=0)
        {
		  rowView =inflater.inflate(R.layout.adddevadapteritems, parent, false);
		  TextView nameView = (TextView) rowView.findViewById(R.id.txtnewdevname);
		  TextView addressView=(TextView) rowView.findViewById(R.id.txtnewdevtype);
		  //TextView pairView=(TextView)rowView.findViewById(R.id.s_PairTextView);
		  ImageView add = (ImageView)rowView.findViewById(R.id.imgdevaddnow);
		  ImageView help = (ImageView)rowView.findViewById(R.id.imghelp);
		  //final TextView txtinfo = (TextView)rowView.findViewById(R.id.txtnewdevinfo);
		  
		  String sensorName=modelsArrayList.get(position).getDeviceName();
		 
		  nameView.setText("Name: "+sensorName);
		  addressView.setText("Protocol Type: "+modelsArrayList.get(position).getProtocolType() 
				  + "# profiles" + Integer.toString(modelsArrayList.get(position).getMaxUserQuantity()));
		
		  if(modelsArrayList.get(position).getPairStatus()==1)
		  {
			  //pairView.setText("Tap to add");  
		  //pairView.setBackgroundColor(Color.RED);
		  }
		  else{ 
			  //pairView.setText("Added");
			  add.setImageDrawable(this.context.getResources().getDrawable(R.drawable.added48));
		  }
		final TextView devinfo = (TextView)rowView.findViewById(R.id.txtnewdevinfo);
		devinfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//take to help fragment
			}
		});
		
		final TextView devinfom = (TextView)rowView.findViewById(R.id.txtnewdevinfomore);
		devinfom.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//take to help fragment
			}
		});
    		
              help.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(devinfo.getVisibility() == View.INVISIBLE){
						devinfo.setVisibility(View.VISIBLE);
						devinfom.setVisibility(View.VISIBLE);
					}else{
						devinfo.setVisibility(View.INVISIBLE);
						devinfom.setVisibility(View.INVISIBLE);
					}
//						AlphaAnimation fadeIn = new AlphaAnimation( 1.0f , 0.0f );			
//						AlphaAnimation fadeOut = new AlphaAnimation(0.0f , 1.0f ); 						
//						fadeIn.setDuration(500);
//						fadeIn.setFillAfter(true);
//						fadeOut.setDuration(500);
//						fadeOut.setFillAfter(true);
//						fadeOut.setStartOffset(500+fadeIn.getStartOffset());	
//						txtinfo.startAnimation(fadeIn);
//						txtinfo.startAnimation(fadeOut);
					//}else{
					//	txtinfo.setVisibility(View.INVISIBLE);
					//}					
					//txtinfo.setAnimation(AnimationUtils.loadAnimation(context, R.animator.translate));
				}
			});
        }
       
        // 5. retrn rowView
        return rowView;
    }
	
}

