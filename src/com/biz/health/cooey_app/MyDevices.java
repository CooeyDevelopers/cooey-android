package com.biz.health.cooey_app;

import java.util.ArrayList;
import java.util.List;

import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.health.utils.db.CooeyDeviceDataSource;
import com.lifesense.ble.bean.LsDeviceInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyDevices extends Fragment {

	private List<LsDeviceInfo> currentDevs = null;
	private MyDevicesListAdapter<LsDeviceInfo> myDevAdapter = null;
	public MyDevices()
	{
		
	}
	
	private void setDefaultActionBarTitle(){
		getActivity().getActionBar().setTitle("MY DEVICES");
	}
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		setDefaultActionBarTitle();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setDefaultActionBarTitle();
	}
	
	private class TakeReadingButtonClickListener implements OnClickListener{

		private LsDeviceInfo actDev = null;
		public TakeReadingButtonClickListener(LsDeviceInfo _dev){
			actDev = _dev;
			((MainActivity)getActivity()).setCurrentDeviceToRead(actDev);
		}
		@Override
		public void onClick(View v) {
			//take to another screen to show reading
			//push the device to BleManager to start scanning for that device value.
			String tmp = CooeyBleDeviceManager.deviceTypeToName(actDev.getDeviceType());
			if(tmp.contains("WEIGHT")){
				getFragmentManager().beginTransaction().replace(R.id.container, new TakeWtReading(), "deviceList").addToBackStack(null).commit();
			}
			else{
				getFragmentManager().beginTransaction().replace(R.id.container, new TakeBPReading(), "deviceList").addToBackStack(null).commit();
			}
		}
	}
	
	private class DeleteDeviceButtonClickListener implements OnClickListener{

		private LsDeviceInfo actDev = null;
		private int pos2del = -1;
		public DeleteDeviceButtonClickListener(LsDeviceInfo _dev, int position){
			actDev = _dev;
			pos2del = position;
		}
		@Override
		public void onClick(View v) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(
					getActivity()).setTitle("Do you want to delete?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							currentDevs.remove(pos2del);
							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									((MainActivity)getActivity()).removeDevice(actDev);
									myDevAdapter.notifyDataSetChanged();
								}
							});
						}

					}).setNegativeButton("No", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}

					}).setMessage("Device can be added again by \"Add New\"");
			builder.create().show();
		}

	}
	
	private class DeviceListItemClickListener implements OnItemClickListener{

		private int lastPos = -1;
		private View lastvw = null;
		
		private void resetLastPos(){
			lastPos = -1;
		}
		public DeviceListItemClickListener(){}
		
		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int pos,
				long arg3) {
			
			LinearLayout lyt = (LinearLayout)v.findViewById(R.id.lytmyDevxtra);
			Button takeReading = (Button)v.findViewById(R.id.readDeviceBtn);
			ImageButton del = (ImageButton)v.findViewById(R.id.btndel);
			if(lastPos == pos){
				if(lastvw != null){
					if(lastvw.getVisibility() == v.GONE){
						lastvw.setVisibility(v.VISIBLE);
					}
					else{
						lastvw.setVisibility(v.GONE); 
					}
				}
			}
			else{
				if(lastvw != null){
					((Button)lastvw.findViewById(R.id.readDeviceBtn)).setOnClickListener(null);
					
					lastvw.setVisibility(v.GONE);
					
					((ImageButton)lastvw.findViewById(R.id.btndel)).setOnClickListener(null);
				}
				lyt.setVisibility(v.VISIBLE);
				TakeReadingButtonClickListener tr = new TakeReadingButtonClickListener((LsDeviceInfo) arg0.getItemAtPosition(pos));				
				takeReading.setOnClickListener(tr);
				del.setOnClickListener(new DeleteDeviceButtonClickListener((LsDeviceInfo) arg0.getItemAtPosition(pos), pos));	
			}
			lastPos = pos;
			lastvw = lyt;
		}
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		//menu.clear();
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.my_devices, container,
				false);
		
		ListView myDevList = (ListView)rootView.findViewById(R.id.MyDevLst);
		Button addNewDev = (Button)rootView.findViewById(R.id.addNewDev);
		DeviceListItemClickListener devLstItmClk = new DeviceListItemClickListener();
		
		//read from the content providers and populate the view with the devices already registered
		currentDevs = new ArrayList<LsDeviceInfo>();
		//currentDevs.add(new LsDeviceInfo());
		//currentDevs.add(new LsDeviceInfo());
		setHasOptionsMenu(true);
		getActivity().getActionBar().show();
		
		List<LsDeviceInfo>lst = ((MainActivity)getActivity()).getDevicesForCurrentUser();
		
		if(lst.size() == 0){
			Toast.makeText(getActivity(), "You have 0 devices, \n please \"start scan\" to add", Toast.LENGTH_SHORT).show();
			//DeviceList dl = new DeviceList();
			//getFragmentManager().beginTransaction().replace(R.id.container, dl, "deviceList").addToBackStack(null).commit();
		}
		else{
			for(LsDeviceInfo d : lst){
				currentDevs.add(d);
			}
		}
		
		myDevAdapter = new MyDevicesListAdapter<LsDeviceInfo>(getActivity(), R.layout.mydevices_listitem, currentDevs);
		myDevList.setAdapter(myDevAdapter);
		myDevList.setOnItemClickListener(devLstItmClk);
		
		addNewDev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DeviceList dl = new DeviceList();
				getFragmentManager().beginTransaction().replace(R.id.container, dl, "deviceList").addToBackStack(null).commit();
			}
		});
		//if no device, take to 
		return rootView;
	}
}
