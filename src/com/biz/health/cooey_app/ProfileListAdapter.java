package com.biz.health.cooey_app;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biz.cooey.CooeyProfile;

public class ProfileListAdapter extends BaseAdapter {

	private Context ctx;
	private int layoutResource;
	private List<CooeyProfile> allData;
	private LayoutInflater inflater;
	private Typeface QS_font;

	public ProfileListAdapter(Context context, List<CooeyProfile> profiles) {

		ctx = context;
		layoutResource = R.layout.profilelistitem;
		allData = profiles;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		CooeyProfile actcp = ((MainActivity)context).getActiveProfile();
		
		CooeyProfile addprofdummy = new CooeyProfile();
		if(actcp.getParentId() > 0){
			addprofdummy.setFirstName("Parent Profile");
			allData.add(addprofdummy);
			
		}else{
			addprofdummy.setFirstName("Add New Profile");
			allData.add(addprofdummy);
		}

		QS_font = Typeface.createFromAsset(ctx.getAssets(),
				"fonts/Quicksand_Book.otf");
	}

	@Override
	public int getCount() {
		return allData.size();
	}

	@Override
	public Object getItem(int position) {
		return allData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(layoutResource, parent, false);
		}

		TextView profName = (TextView) convertView
				.findViewById(R.id.txtprofname);
		CooeyProfile tmp = (CooeyProfile) getItem(position);
		profName.setText(tmp.getFirstName());
		profName.setTextSize(20f);
		profName.setTypeface(QS_font);
		
		if (tmp.getFirstName().equalsIgnoreCase("Add New Profile")) {
			convertView.findViewById(R.id.imgvwprofadd).setVisibility(
					View.VISIBLE);
			profName.setTextSize(18f);
		} else {
			convertView.findViewById(R.id.imgvwprofadd)
					.setVisibility(View.GONE);
		}

		return convertView;
	}

}
