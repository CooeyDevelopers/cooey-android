package com.biz.health.cooey_app;

import java.util.List;

import com.biz.health.model.MedicineData;
import com.biz.health.model.RecommendationData;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyRecentMedsAdapter<T> extends ArrayAdapter<T> {

	private List<T> listMeds = null;
	private LayoutInflater inflater = null;
	private int layoutResource;
	private Context ctx;
	private Typeface QS_font;
	
	public MyRecentMedsAdapter(Context context, int resource, List<T> objects) {
		super(context, resource, objects);
		ctx = context;
		listMeds = objects;
		layoutResource = resource;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);	
		QS_font = Typeface.createFromAsset(ctx.getAssets(), "fonts/Quicksand_Light.otf");
	}

	public int getCount() {
        if(listMeds.size() > 5){
        	return 5;
        }
        return listMeds.size();
    }
 
    public T getItem(int position) {
        return listMeds.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	
    	if(convertView == null)
		{		
    		convertView =inflater.inflate(layoutResource, parent, false);
		}
    	TextView tv = (TextView) convertView.findViewById(R.id.txtmedname);
    	//if(position == 0){
    	//	String header = (String) getItem(position);
    	//	tv.setText(header);
		//	tv.setTypeface(null, Typeface.BOLD);
		//}else{
	    	MedicineData med = (MedicineData)getItem(position);		
			
			tv.setText(med.getMedicineName() + "("+med.getDosage()+")");
			tv.setTypeface(QS_font);
			if(position == 0){
				tv.setTypeface(QS_font, Typeface.BOLD_ITALIC);
			}
		//}
		return convertView;    	
    }

}
