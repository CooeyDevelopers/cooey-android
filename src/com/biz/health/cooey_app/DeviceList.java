package com.biz.health.cooey_app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceFactory;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyDeviceDataReceiveCallback;
import com.biz.cooey.CooeyDevicePairCallback;
import com.biz.cooey.CooeyDeviceSearchCallback;
import com.biz.cooey.CooeyDeviceType;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.CooeyStatus;
import com.biz.cooey.WeightData;
import com.biz.health.utils.db.CooeyDeviceDataSource;
import com.biz.health.utils.db.CooeySQLHelper;
import com.github.mikephil.charting.charts.LineChart;
import com.lifesense.ble.LsBleManager;
import com.lifesense.ble.PairCallback;
import com.lifesense.ble.SearchCallback;
import com.lifesense.ble.bean.BloodPressureData;
import com.lifesense.ble.bean.DeviceUserInfo;
import com.lifesense.ble.bean.LsDeviceInfo;
import com.lifesense.ble.commom.BroadcastType;
import com.lifesense.ble.commom.DeviceType;


public class DeviceList extends Fragment{
	
	private LsBleManager mLsBleManager;
	private ListView scanResultsListView;
	private ArrayList<LsDeviceInfo> mBleDeviceList;
	private List<String> deviceTypeList;
	private BleDeviceAdapter mBleDeviceAdapter;
	private Button mScanButton, mStopButton, mSkipButton;
	private BroadcastType broadcastType;
	private StringBuffer scanDeviceBuffer;
	private ArrayList<LsDeviceInfo> tempList;
	private ProgressDialog scanProgressDialog;
	private final static Map<String,DeviceType> deviceMap;
	private boolean scanResult = false;
	private LineChart lc;
	private ArrayList<BloodPressureData> bpList;
	private CooeyProfile activeProfile;
	//test
	private CooeyBleDeviceManager mManager = null;
	private CooeyBleDeviceFactory mFactory = null;
	Boolean flag = true;
	private Boolean scanFlag = false;
	private UserProfileDialogHandler usrProfHandler = null;//new UserProfileDialogHandler();
	private List<LsDeviceInfo> lstDevices = null;
	
	static{
		Map<String,DeviceType> map=new HashMap<String,DeviceType>();
		map.put("Sphygmometer", DeviceType.SPHYGMOMANOMETER);
		map.put("FatScale", DeviceType.FAT_SCALE);
		map.put("WeightScale", DeviceType.WEIGHT_SCALE);
		map.put("Pedometer", DeviceType.PEDOMETER);
		map.put("HeightRuler", DeviceType.HEIGHT_RULER);
		deviceMap=Collections.unmodifiableMap(map);
	}
	
	private void cleanup(){
		//mLsBleManager.stopDataReceiveService();
		mManager.shutdown();
		
	}
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		//cleanup();
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
	
	@Override
	public void onAttach(Activity activity) {

		getActivity().getActionBar().setTitle("ADD A NEW DEVICE");
		super.onAttach(activity);
	}
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.newdevices, menu);	
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		switch (id) {
		case R.id.mydashfromdev:
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container,
							((MainActivity) getActivity()).getDashboard())
					.addToBackStack(null)
					.commit();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;			
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		View rootView = inflater.inflate(R.layout.device_list, container, false);
		scanResultsListView = (ListView)rootView.findViewById(R.id.scanResultsView);
		mBleDeviceList = new ArrayList<LsDeviceInfo>();
		deviceTypeList=new ArrayList<String>();
		mScanButton = (Button)rootView.findViewById(R.id.scanBtn);
		mSkipButton = (Button)rootView.findViewById(R.id.skipBtn);	
		mManager = ((MainActivity)getActivity()).getBleLeManager();
		
		/*CooeyProfile cp = new CooeyProfile();
		cp.setBloodGroup("B+");
		cp.setEmail("a@a.com");
		cp.setFirstName("fn");
		cp.setLastName("ln");
		cp.setGender("m");
		cp.setLocationArea("area");
		cp.setLocationCity("city");
		cp.setLocationCountry("country");
		cp.setMobileNumber("59435087450");*/
		
		scanDeviceBuffer= new StringBuffer();
		
		bpList = new ArrayList<BloodPressureData>();
		initListView();
		
		setHasOptionsMenu(false);
		
		populateAllDevices();
		activeProfile = ((MainActivity)getActivity()).getActiveProfile();
		
		CooeyStatus cs = mManager.checkPlatformCapability();
		mScanButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(scanFlag){
					//stop
					mScanButton.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
					mScanButton.setBackgroundResource(R.drawable.lt2brdvwlt2);
					scanFlag = false;
					mScanButton.setText("Start Scan");
					//reset for next possible scan
					usrProfHandler = new UserProfileDialogHandler();
					return;
				}
				//one time
				usrProfHandler = new UserProfileDialogHandler();
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						mBleDeviceList.clear();
						mBleDeviceAdapter.notifyDataSetChanged();
					}
				});			
				mScanButton.setCompoundDrawablesWithIntrinsicBounds( R.drawable.bl32, 0, 0, 0);
				mScanButton.setBackgroundResource(R.drawable.grn1brdvwlt2);
				//mStopButton.setBackgroundResource(R.drawable.lt2brdvwlt2);
				scanFlag = true;
				mScanButton.setText("Stop Scan");
				mManager.searchDevicesForPairing(new CooeyDeviceSearchCallback() {
					
					@Override
					public void onDeviceFound(LsDeviceInfo arg0) {
						// TODO Auto-generated method stub
						Log.i("DEVICE FOUND", arg0.toString());
						if (!deviceExists(arg0.getDeviceName())) {							
							mBleDeviceList.add(arg0);
							getActivity().runOnUiThread(new Runnable() {								
								@Override
								public void run() {
									mBleDeviceAdapter.notifyDataSetChanged();									
								}
							});
						}
					}
				});
			}
		});
		
		
		/*mStopButton.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				mScanButton.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
				mStopButton.setBackgroundResource(R.drawable.grn1brdvwlt2);
				mScanButton.setBackgroundResource(R.drawable.lt2brdvwlt2);
				mManager.stopSearchForDevices();
			}
		});*/
		
		if( ((MainActivity)getActivity()).isUserOnboarding() )
		{
			mSkipButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MyRecommendations f = new MyRecommendations();
					f.setRecommendationtype(1);
					getFragmentManager().beginTransaction().replace(R.id.container, f , "")
					.addToBackStack(null)
					.commit();
					
				}
			});
		}else{
			getActivity().getActionBar().show();
			setHasOptionsMenu(true);
			mSkipButton.setText("Back");
			mSkipButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Fragment f = ((MainActivity)getActivity()).getDashboard();
					getFragmentManager().beginTransaction().replace(R.id.container, f, "")
					.addToBackStack(null)
					.commit();
				}
			});
		}
		((MainActivity)getActivity()).changeBluetoothState(true);		
		return rootView;
	}
	
	//@Override
	public View onCreateView1(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		View rootView = inflater.inflate(R.layout.device_list, container, false);

		scanResultsListView = (ListView)rootView.findViewById(R.id.scanResultsView);
		mBleDeviceList = new ArrayList<LsDeviceInfo>();
		deviceTypeList=new ArrayList<String>();
		mScanButton = (Button)rootView.findViewById(R.id.scanBtn);
		mStopButton = (Button)rootView.findViewById(R.id.stopBtn);
		
		//mLsBleManager= ((MainActivity) getActivity()).getBleLeManager();
				/*LsBleManager.newInstance();
		mLsBleManager.initialize(getActivity());*/
		scanDeviceBuffer= new StringBuffer();
		lc = (LineChart) rootView.findViewById(R.id.chart);
		lc.setTouchEnabled(true);
		lc.setDragEnabled(true);
		
		mFactory = new CooeyBleDeviceFactory(new int [] {CooeyDeviceType.BP_METER});
		mManager= ((MainActivity)getActivity()).getBleLeManager();
		/*mManager = (CooeyBleDeviceManager) mFactory.getBleDeviceManager();
		mManager.initialize(new CooeyProfile(), getActivity());
		*/
		
		bpList = new ArrayList<BloodPressureData>();
		
		mScanButton.setOnClickListener(new OnClickListener() {
		
			@Override
			public void onClick(View v) {
				mBleDeviceList.clear();
				mScanButton.setCompoundDrawablesWithIntrinsicBounds( R.drawable.bl32, 0, 0, 0);
				if (mLsBleManager != null && mLsBleManager.isOpenBluetooth())
				{
					if (mLsBleManager.isSupportLowEnergy()) 
					{
						if (!mLsBleManager.searchLsDevice(
								mLsScanCallback, getScanDeviceList(), getBroadcastType())) 
						{
							showPromptDialog("Ku:i", "Other tasks are running, please try again later",-1);
						} else 
						{
							//showScanDialog();
						}
	
					} else {
						showPromptDialog("Ku:i", "Not support Bluetooth Low Energy",-1);
					}
	
				} else {
					showPromptDialog("Ku:i", "Please turn on the Bluetooth function",-1);
				}
			}
		});
		mStopButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mLsBleManager != null) 
				{
					mLsBleManager.stopSearch();
				}
			}
		});
		initListView();
		mLsBleManager.stopDataReceiveService();
		
		return rootView;
	}
	
	private class UserProfileDialogHandler implements Runnable{
		private List<DeviceUserInfo> curDevInfo = null;
		private Boolean isRunning = false;
		public Boolean wasCancelled = null;
		
		public UserProfileDialogHandler(){
			//curDevInfo = new ArrayList<DeviceUserInfo>(dui); //copy, dont refer
			
		}
		public void setDeviceUserInfo(List<DeviceUserInfo> dui){
			if(isRunning) return;
			curDevInfo = new ArrayList<DeviceUserInfo>(dui);
		}
		
		@Override
		public void run() {
			
			if(isRunning) return;
			isRunning = true;
			final ArrayList<String>items = new ArrayList<String>();
			
			for(int i =0; i < curDevInfo.size(); ++i){
				items.add("Profile Button " + (i+1));//((DeviceUserInfo)curDevInfo.get(i)).getUserName());
			}		
			
			AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
	        
    		builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   
            	   dialog.dismiss();
                   int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                   String s = items.get(selectedPosition);
                   mManager.bindUserProfileWithName(selectedPosition + 1 , activeProfile.getFirstName());
                   isRunning = false;
                   wasCancelled = false;
               }
           })
           .setTitle("Choose profile button for '"+ activeProfile.getFirstName() + "'")
           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   //mManager.bindUserProfileWithName(0 , activeProfile.getFirstName());
            	   wasCancelled = true;
            	   dialog.dismiss();
               }
           }).setSingleChoiceItems(items.toArray(new String[items.size()]), 0, null)               
           .create()
           .show();
		}
		
	}
	
	
	private void addDeviceToUser(LsDeviceInfo arg0, long uid){
		
		//
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(getActivity());
		ds.open();
		ds.createDeviceForUser(arg0, uid);
		ds.close();
	}
	
	private void replaceAndAddDevice(LsDeviceInfo devtodel, LsDeviceInfo arg0, long uidtodel, long uidtoadd){
		
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(getActivity());
		ds.open();
		ds.getRemoveDeviceForUser(devtodel, uidtodel);		
		ds.close();
		//add to new user
		addDeviceToUser(arg0, uidtoadd);
	}
	
	private void addDeviceToStore(final LsDeviceInfo arg0){
		//get list of devices for current user, 
		//delete the duplicate device
		//add/update the DB appropriately.
		Boolean foundFlag = false;
		String proto = arg0.getProtocolType();
		for(final LsDeviceInfo dev : lstDevices){
			if(dev.getProtocolType().contains(proto)){				
				if(dev.getProtocolType().contains("A3")){
					if( (dev.getDeviceUserNumber() == arg0.getDeviceUserNumber()) && 
						dev.getDeviceId().equalsIgnoreCase(arg0.getDeviceId())	&&
						dev.getDeviceSn().equalsIgnoreCase(arg0.getDeviceSn()) ){
								
						foundFlag = true;
							//match found.
						getActivity().runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());						        
					    		builder2.setPositiveButton("Replace", new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int id) {
					            	   replaceAndAddDevice(dev, arg0, dev.getMaxUserQuantity(), activeProfile.getDbid());
					            	   dialog.dismiss();					                
					               }
					           })
					           .setTitle("This device was already added,  ")
					           .setNegativeButton("Don't replace", new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int id) {
					            	   addDeviceToUser(arg0, dev.getMaxUserQuantity());
					            	   dialog.dismiss();
					               }
					           })
					           .create()
					           .show();
							}
						});
					}
				}else if(dev.getProtocolType().contains("A2")){	
					if( dev.getDeviceId().equalsIgnoreCase(arg0.getDeviceId())	&&
					dev.getDeviceSn().equalsIgnoreCase(arg0.getDeviceSn()) ){
						foundFlag = true;
						//A2 matched
					}
				}
			}
		}
		if(!foundFlag){//fresh device
			 addDeviceToUser(arg0, activeProfile.getDbid());
		}
		
	}
	
	private class paircallbk extends CooeyDevicePairCallback{

		private int pos = -1;
		public paircallbk(int _pos) {
			pos = _pos;
		}
		@Override
		public void onPairingResult(LsDeviceInfo arg0, final int status) {
			if(status == -1){
				Toast.makeText(getActivity(), "Pairing failed, please try again", Toast.LENGTH_SHORT).show();
				return;
			}
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					((LsDeviceInfo)mBleDeviceAdapter.getItem(pos)).setPairStatus(status);
					mBleDeviceAdapter.notifyDataSetChanged();
				}
			});
			//mManager.registerPairedDevices(arg0);
			usrProfHandler = new UserProfileDialogHandler();//for adding the next device with the current scan.
			addDeviceToStore(arg0);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public void onDiscoverUserInfo(final List arg0) {
			usrProfHandler.setDeviceUserInfo(arg0);
			getActivity().runOnUiThread(usrProfHandler);
		}
		
	}
	private void populateAllDevices(){
		
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(getActivity());
		ds.open();
		lstDevices = ds.getAllDevices();
		ds.close();		
	}
	
	private void initListView() 
	{
		mBleDeviceAdapter = new BleDeviceAdapter(getActivity().getApplicationContext(),
				mBleDeviceList);
		scanResultsListView.setAdapter(mBleDeviceAdapter);
		scanResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, 
					final View view,
					int position, 
					long id)
			{
				if (mLsBleManager != null) 
				{
					mLsBleManager.stopSearch();
				}

				LsDeviceInfo de = (LsDeviceInfo) parent.getAdapter().getItem(position);
				if(de.getPairStatus() == 0){
					Toast.makeText(getActivity(), "Already Paired", Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(usrProfHandler.wasCancelled == null || usrProfHandler.wasCancelled)
					mManager.pairDevice( de, new paircallbk(position));			
				
			}
		});
	}
	
	private void startPairDevice(LsDeviceInfo de) 
	{
		LsDeviceInfo lsDevice=new LsDeviceInfo();
		lsDevice.setDeviceName(de.getDeviceName());
		lsDevice.setBroadcastID(de.getBroadcastID());
		lsDevice.setDeviceType(de.getDeviceType());
		lsDevice.setProtocolType(de.getProtocolType());
		lsDevice.setModelNumber(de.getModelNumber());
		
		mLsBleManager.startPairing(lsDevice,new PairCallback() 
		{
			public void onDiscoverUserInfo(final List userList) 
			{
				getActivity().runOnUiThread(new Runnable() 
				{
					@Override
					public void run() 
					{
						//deUserInfos=userList;
						//ringProgressDialog.dismiss();
						//showDeviceUserInfo(userList);		
					}		
				});		
			}

			public  void onPairResults(final LsDeviceInfo lsDevice,final int status)
			{
				if(status!=0)
				{
					getActivity().runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							mBleDeviceAdapter.clear();
							
							showPromptDialog("Prompt", "Failed to paired device,please try again",-1);
						}
					});
				}
				else if(status==0)
				{
					getActivity().runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							
							mLsBleManager.addMeasureDevice(lsDevice);
							mBleDeviceList.remove(lsDevice);
							mBleDeviceAdapter.notifyDataSetChanged();
							//Intent resultsIntent=new Intent(getActivity(), PairResultsActivity.class);
							//resultsIntent.putExtra("LsDeviceInfo",lsDevice);
							//startActivity(resultsIntent);
						}
					});
				}
			}
		});
		
	}
	
	private BroadcastType getBroadcastType() {
		if(broadcastType!=null)
		{
			return broadcastType;
		}
		else return broadcastType=BroadcastType.ALL;
	}
	
	
	private void showPromptDialog(String title, String message,final int userNumber) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				getActivity()).setTitle(title)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(userNumber!=-1)
						{
							//showInputDialog(userNumber);
						}
					}

				}).setMessage(message);
		builder.create().show();
	}
	
	private List<DeviceType> getScanDeviceList()
	{
		scanDeviceBuffer.setLength(0);
		List<DeviceType> typeList=new ArrayList<DeviceType>();
		if(deviceTypeList.size()==0)
		{		
			typeList.add(DeviceType.SPHYGMOMANOMETER);
			typeList.add(DeviceType.FAT_SCALE);
			typeList.add(DeviceType.WEIGHT_SCALE);
			typeList.add(DeviceType.HEIGHT_RULER);
			typeList.add(DeviceType.PEDOMETER);
			scanDeviceBuffer.append("scan all device type");
		}
		else 
		{
			for(String type:deviceTypeList)
			{
				typeList.add(deviceMap.get(type));
				scanDeviceBuffer.append(type);
				scanDeviceBuffer.append("、");
			}		
		}	
		return typeList;
	}
	
	private boolean deviceExists(String name) 
	{
		if(name==null || name.length()==0)
		{
			return false;
		}
		if(mBleDeviceList!=null && mBleDeviceList.size()>0)
		{
			for (int i = 0; i < mBleDeviceList.size(); i++)
			{
				if (mBleDeviceList.get(i)!=null && mBleDeviceList.get(i).getDeviceName()!=null && mBleDeviceList.get(i).getDeviceName().equals(name)) 
				{
					return true;
				}
			}
			return false;
		}
		else return false;
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	private void updateListViewBackground(String name) 
	{
		scanResult = true;
		if (!tempList.isEmpty()) 
		{
			for (LsDeviceInfo dev : tempList) 
			{
				if (dev != null && dev.getDeviceName()!=null && dev.getDeviceName().equals(name)) 
				{
					final View view = scanResultsListView.getChildAt(mBleDeviceAdapter.getPosition(dev));
					if (view != null) 
					{
						//setListViewBackgroundColor(view);
					}
				}
			}
		}
	}
	
	private SearchCallback mLsScanCallback = new SearchCallback() 
	{
		public void onSearchResults(final LsDeviceInfo lsDevice) 
		{
			getActivity().runOnUiThread(new Runnable() 
			{
				@SuppressWarnings("unchecked")
				@Override
				public void run() {
					if (!deviceExists(lsDevice.getDeviceName())) {
						scanResult = true;
						
						//scanProgressDialog.dismiss();
						
						//tempList.add(bleDevice);
						mBleDeviceList.add(lsDevice);
						//mBleDeviceAdapter.add(bleDevice);
						mBleDeviceAdapter.notifyDataSetChanged();
						
					} else {
						//updateListViewBackground(lsDevice.getDeviceName());
					}
				}
			});
		}
	};

}
