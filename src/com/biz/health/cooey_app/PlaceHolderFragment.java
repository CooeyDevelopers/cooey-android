package com.biz.health.cooey_app;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.CooeyProfile;
import com.facebook.widget.LoginButton;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceHolderFragment extends Fragment {
	
	private LoginButton mButtonLogin;
	private Button mButtonLogout;
	private ImageButton mButtonFBLogin, mBtnClear, mButtonGPlusLogin;
	private SimpleFacebook mSimpleFacebook;
	private MessageFromFragments callbk;
	private View rootView;
	private NetworkInfo netInfo;
	private TextView txtnewuser;
	private Button btnlogin;
	private EditText etxtcofrmpass, newpasswd, etxtloginname, etxtfname, etxtlname;
	private CooeyLoginProgressDialog loginTask;
	private CooeyRegisterProgressDialog registerTask;
	private LinearLayout registerfields;
	private ImageView logo;
	private String lastErrorMessage = "";
	private Typeface QS_font;
	protected static final String TAG = MainActivity.class.getName();
	
	public PlaceHolderFragment() {
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		callbk = (MessageFromFragments) activity;		
	};
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		//getActivity().getMenuInflater().inflate(R.menu.login, menu);
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.login, menu);
	}

	@Override
	public void onResume() {
		if(rootView != null){
			makeViewFullScreen();
		}
		super.onResume();
	}
	
	@Override
	public void onDestroy() {
		loginTask.cancel(true);
		super.onDestroy();
	}
	
	private void makeViewFullScreen() {
		rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY 
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		getActivity().getActionBar().hide();
		
	}
	
	private void handleOfflineLoginMode(){
		
		//check preference if that device id is present				
		SharedPreferences prefs = getActivity().getPreferences(Activity.MODE_PRIVATE);
		String prefDevId = prefs.getString("com.cooey.deviceid", null);
		if(prefDevId == null){
			//put the deviceid 
			String devid = Secure.getString(getActivity().getContentResolver(), Secure.ANDROID_ID);
			Editor editor = prefs.edit();
			editor.putString("com.cooey.deviceid", devid).apply();
			TelephonyManager tMgr = (TelephonyManager)getActivity().getSystemService(Activity.TELEPHONY_SERVICE);
			if(tMgr.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
		         //tablet, so no phone number just go ahead with deviceid and make email mandatory
		    }else{
		        
		    	String mPhoneNumber = tMgr.getLine1Number();
				if(mPhoneNumber.length() == 0 || mPhoneNumber == null){
					//ask for phone number in the next screen
					//getFragmentManager().beginTransaction().replace(R.id.container, new GetPhoneDetails())
					//					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
					//					.commit();
				}//else, chk if phone number stored in prefs is same as this.
				
		    }
			
		}else{
			String phNum = prefs.getString("com.cooey.phonenumber", null);
			if(phNum == null){
				//ask for phone number in the next screen
				//getFragmentManager().beginTransaction().replace(R.id.container, new GetPhoneDetails())
				//					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				//					.commit();
			}
			else{
				//check if he/she is onboarded
				//if already onboarded, go to dashboard.
			}
		}
	}
	private void setLogoPosition(int by){
		
		RelativeLayout.LayoutParams orig = (LayoutParams) logo.getLayoutParams();
		LayoutParams params = new LayoutParams(
		       orig.width, orig.height);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		params.setMargins(orig.leftMargin, orig.topMargin + by, orig.rightMargin, orig.bottomMargin);
		logo.setLayoutParams(params);		
	}
	
	private Boolean validateLoginFields(){
		
		if(etxtloginname.getText().toString().contains(" ")){
			lastErrorMessage = "Invalid email.";
			return false;
		}
		if(etxtloginname.getText().toString().length() < 3 &&
				!etxtloginname.getText().toString().contains("@")){
			lastErrorMessage = "Invalid email.";
			return false;
		}
		if(newpasswd.getText().toString().length() == 0 || newpasswd.getText().toString().length() < 4){
			lastErrorMessage = "Password cannot be empty (min 4 chars)";
			return false;
		}
		return true;
	}
	
	private Boolean validateRegisterFields(){
		if(!validateLoginFields()) return false;
	
		if(!etxtcofrmpass.getText().toString().equals(newpasswd.getText().toString())){
			lastErrorMessage = "Passwords dont match";
			return false;
		}		
		if(etxtfname.getText().toString().length() < 3 ||
				etxtlname.getText().toString().length() < 3)
		{
			lastErrorMessage = "Names too short";
			return false;
		}
		return true;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		rootView = inflater.inflate(R.layout.fragment_main, container, false);
		
		//View fbbkngd = rootView.findViewById(R.id.circlebkngdfb);
		//fbbkngd.setBackgroundColor(getActivity().getResources().getColor(R.color.Blue));
		QS_font = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/Quicksand_Book.otf");
		//mSimpleFacebook = SimpleFacebook.getInstance();
		mButtonLogin = (LoginButton) rootView.findViewById(R.id.button_login);
		mButtonLogin.setReadPermissions(Arrays.asList("public_profile","email", "user_birthday"));
		mButtonFBLogin = (ImageButton) rootView.findViewById(R.id.button_login1);
		mButtonGPlusLogin = (ImageButton) rootView.findViewById(R.id.button_login2);
		txtnewuser = (TextView)rootView.findViewById(R.id.txtregistertxt);		
		mBtnClear = (ImageButton) rootView.findViewById(R.id.buttonclear);
		btnlogin = (Button)rootView.findViewById(R.id.btnlogin);
		//txtlogin.setTypeface(QS_font);
		txtnewuser.setTypeface(QS_font);
		logo = (ImageView)rootView.findViewById(R.id.imglogo);
		
		etxtcofrmpass = (EditText)rootView.findViewById(R.id.txtloginpasswdconfrm);
		etxtcofrmpass.setTypeface(QS_font);
		newpasswd = (EditText)rootView.findViewById(R.id.txtloginpasswd);
		newpasswd.setTypeface(QS_font);
		etxtloginname = (EditText)rootView.findViewById(R.id.txtloginname);
		etxtloginname.setTypeface(QS_font);
		etxtfname=(EditText)rootView.findViewById(R.id.txtloginfname);
		etxtfname.setTypeface(QS_font);
		etxtlname=(EditText)rootView.findViewById(R.id.txtloginlname);
		etxtlname.setTypeface(QS_font);
		
		//read phone number	
		loginTask = new CooeyLoginProgressDialog(getActivity(), false);
		registerTask = new CooeyRegisterProgressDialog(getActivity());
		registerfields = (LinearLayout)rootView.findViewById(R.id.lytregisterfields);
		
		ConnectivityManager conMgr = (ConnectivityManager)getActivity().getSystemService(Activity.CONNECTIVITY_SERVICE);
		netInfo = conMgr.getActiveNetworkInfo();		
		
		//it should have a number too
		//if not, go to the screen to collect phone number
		//store phone number and DeviceID in sharedPreferences
		//
		//If present, and if not onboarded 
		//go to screen to collect email, immediate contact, password
		//send deviceid,email, phonenumber to server (nned this for web login)
		//	
		mButtonGPlusLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), "Sorry, we do not support Gmail login yet.", Toast.LENGTH_SHORT).show();
			}
		});
		
		mButtonFBLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				StatsUtil.stat_FBLoginAttempt(getActivity());
				mButtonLogin.performClick();
			}
			public void onClick1(View v) {
				
				if ( netInfo.getState() == NetworkInfo.State.CONNECTED) {
					//go to server to check if profile for this devid exists
				}
				else{				
					handleOfflineLoginMode();			
				}
			}
		});
		
		mBtnClear.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				etxtcofrmpass.setVisibility(View.GONE);
				v.setVisibility(View.GONE);
				registerfields.setVisibility(View.GONE);
				txtnewuser.setText("New User");
				newpasswd.setHint("Password");
				btnlogin.setText("Sign-in");
				setLogoPosition(250);
				txtnewuser.setVisibility(View.VISIBLE);				
			}
		});
		btnlogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(btnlogin.getText().toString().equalsIgnoreCase("register")){
					
					if(!validateRegisterFields()) {
						Toast.makeText(getActivity(), lastErrorMessage, Toast.LENGTH_SHORT).show();
						return;
					}
					registerTask = new CooeyRegisterProgressDialog(getActivity());
					registerTask.setRegistrationParams(etxtloginname.getText().toString().trim(), 
														newpasswd.getText().toString().trim(), 
														etxtfname.getText().toString().trim(), 
														etxtlname.getText().toString().trim());
					registerTask.execute();
					return;
				}
				if(loginTask.getStatus() == AsyncTask.Status.RUNNING ){
					return;
				}
				if(((MainActivity)getActivity()).haveNetworkConnection() == false){
					Toast.makeText(getActivity(), "Need internet connection to login", Toast.LENGTH_SHORT).show();
					return;
				}
				if(!validateLoginFields()){ 					
					Toast.makeText(getActivity(), lastErrorMessage, Toast.LENGTH_SHORT).show();
					return;			
				}
				
				loginTask = new CooeyLoginProgressDialog(getActivity(), false);
				loginTask.setLoginParams(etxtloginname.getText().toString().trim(), 
										 newpasswd.getText().toString().trim());
				loginTask.execute();
				
			}
		});
		
		
		//txtlogin.setPaintFlags(txtlogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		txtnewuser.setPaintFlags(txtnewuser.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		txtnewuser.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				/*if(txtnewuser.getText().toString().equalsIgnoreCase("register")){
					//TOFO; start registering
					if(!validateRegisterFields()) {
						Toast.makeText(getActivity(), lastErrorMessage, Toast.LENGTH_SHORT).show();
						return;
					}
					registerTask = new CooeyRegisterProgressDialog(getActivity());
					registerTask.setRegistrationParams(etxtloginname.getText().toString(), 
														newpasswd.getText().toString(), 
														etxtfname.getText().toString(), etxtlname.getText().toString());
					registerTask.execute();
				}else{*/
					etxtcofrmpass.setVisibility(View.VISIBLE);
					txtnewuser.setText("Register");
					newpasswd.setText("");
					newpasswd.setHint("Choose a password");
					mBtnClear.setVisibility(View.VISIBLE);
					registerfields.setVisibility(View.VISIBLE);
					setLogoPosition(-250);
					btnlogin.setText("Register");
					txtnewuser.setVisibility(View.INVISIBLE);
				//}
			}
		});
		
		getActivity().getActionBar().hide();
		setHasOptionsMenu(false);	
		
		//setLogin();
		makeViewFullScreen();
		return rootView;
	}
	
	private void loggedInUIState() {
		//mButtonLogin.setEnabled(false);
		//mButtonLogout.setEnabled(true);
	}
	
	/**
	 * Login example.
	 */
	private void setLogin() {
		// Login listener
		final OnLoginListener onLoginListener = new OnLoginListener() {

			@Override
			public void onFail(String reason) {
				
				Log.w(TAG, "Failed to login");
			}

			@Override
			public void onException(Throwable throwable) {
				
				Log.e(TAG, "Bad thing happened", throwable);
			}

			@Override
			public void onThinking() {
				// show progress bar or something to the user while login is
				// happening
				
			}

			@Override
			public void onLogin() {
				// change the state of the button or do whatever you want
				loggedInUIState();
				FragmentToActivityPayload p = new FragmentToActivityPayload();
				p.setFragment_name(this.getClass().getName());
				callbk.OnMessage(p);				
			}

			@Override
			public void onNotAcceptingPermissions(Permission.Type type) {
				
				Toast.makeText(getActivity(), String.format("You didn't accept %s permissions", type.name()), Toast.LENGTH_SHORT).show();
			}
		};
		
		mButtonLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mSimpleFacebook.login(onLoginListener);
			}
		});
	}
	
	/**
	 * Logout example
	 */
	@SuppressWarnings("unused")
	private void setLogout() {
		final OnLogoutListener onLogoutListener = new OnLogoutListener() {

			@Override
			public void onFail(String reason) {
				
				Log.w(TAG, "Failed to login");
			}

			@Override
			public void onException(Throwable throwable) {
				
				Log.e(TAG, "Bad thing happened", throwable);
			}

			@Override
			public void onThinking() {
				// show progress bar or something to the user while login is
				// happening
				
			}

			@Override
			public void onLogout() {
				// change the state of the button or do whatever you want
				
				Log.e(TAG, "logged out");
				//getFragmentManager().beginTransaction().show(fragmentA).commit;
			}

		};

		mButtonLogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mSimpleFacebook.logout(onLogoutListener);
			}
		});
	}
}