package com.biz.health.cooey_app;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class MedicineAdapter extends ArrayAdapter<MedicineModel> implements Filterable{

	private List<MedicineModel> _objs;
	private int res;
	private Context ctx;
	public MedicineAdapter(Context context, int resource, List<MedicineModel> objects) {
		
		super(context, resource);
		this.ctx = context;
		this.res = resource;
		this._objs = objects;
	}
	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		//super.clear();
		for(int i =0; i <_objs.size(); ++i){
			_objs.remove(i);
		}
		_objs.clear();
		super.clear();
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null){
			LayoutInflater vi = (LayoutInflater)this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=vi.inflate(this.res, null);
		}
		MedicineModel m = getItem(position);
		
		TextView tv = (TextView)convertView.findViewById(android.R.id.text1);
		tv.setPadding(0, 0, 15, 15);
	    tv.setText(m.getMedName());
	    
		return convertView;
	}
	
	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		//return super.getFilter();
		Filter nameFilter = new Filter() {
			
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				// TODO Auto-generated method stub
				if(results != null && results.count > 0)
			    {
					notifyDataSetChanged();
			    }
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub
				
				FilterResults results = new FilterResults();
			    // We implement here the filter logic
			    if (constraint == null || constraint.length() == 0) {
			        // No filter implemented we return all the list
			        results.values = _objs;
			        results.count = _objs.size();
			    }
			    else {
			        // We perform filtering operation
			        List<MedicineModel> nList = new ArrayList<MedicineModel>();
			         
			        for (MedicineModel p : _objs) {
			            if (p.getMedName().toUpperCase(Locale.getDefault()).contains(constraint.toString().toUpperCase()))
			                nList.add(p);
			        }
			         
			        results.values = nList;
			        results.count = nList.size();
			    }
			    return results;
			}
		};
		return nameFilter;
	}
	
}