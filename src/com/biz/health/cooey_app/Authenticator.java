package com.biz.health.cooey_app;

import com.sromku.simple.fb.SimpleFacebook;

import android.app.Activity;
import android.content.Context;

public class Authenticator{	
	private static Authenticator _instance = new Authenticator();
	private SimpleFacebook mSimpleFacebook;
	private Authenticator(){
		
	}
	
	public Authenticator getInstance(Context ctx){
		mSimpleFacebook = SimpleFacebook.getInstance((Activity)ctx);
		return _instance;
	}
	
	public Boolean isAuthenticated(){
		if(mSimpleFacebook.isLogin()){
			return true;
		}
		//TODO: is local login authenticated
		return false;
	}
	
	public void setActiveAuthentication(){
		
	}
	
}
