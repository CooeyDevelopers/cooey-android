package com.biz.health.cooey_app;

public class MedicineModel {

	private String medName;
	private String medComp;
	private String medId;
	
	public String getMedName() {
		return medName;
	}
	public void setMedName(String medName) {
		this.medName = medName;
	}
	public String getMedComp() {
		return medComp;
	}
	public void setMedComp(String medComp) {
		this.medComp = medComp;
	}
	public String getMedId() {
		return medId;
	}
	public void setMedId(String medId) {
		this.medId = medId;
	}
	
	
}
