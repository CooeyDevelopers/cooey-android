package com.biz.health.cooey_app;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceFactory;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyCode;
import com.biz.cooey.CooeyDeviceType;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.CooeyStatus;
import com.biz.cooey.GetJsonFromCooeyTask;
import com.biz.cooey.SendJsonToCooeyTask;
import com.biz.cooey.SendToCooeyCallbackOnResponse;
import com.biz.cooey.WeightData;
import com.biz.cooey.platform.servermodel.WTServerDataPut;
import com.biz.health.model.MedicineData;
import com.biz.health.servermodel.LinkProfile;
import com.biz.health.servermodel.MedServerDataDel;
import com.biz.health.servermodel.UpdateProfile;
import com.biz.health.utils.db.CooeyBPDataSource;
import com.biz.health.utils.db.CooeyDeviceDataSource;
import com.biz.health.utils.db.CooeyMedDataSource;
import com.biz.health.utils.db.CooeyProfileDataSource;
import com.biz.health.utils.db.CooeyWTDataSource;
import com.biz.health.utils.db.ImportExport;
import com.facebook.Session;
import com.google.android.gms.internal.ct;
import com.google.gson.GsonBuilder;
import com.lifesense.ble.bean.LsDeviceInfo;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;
import com.sromku.simple.fb.utils.PictureAttributes.PictureType;

//import android.support.v4.
public class MainActivity extends Activity implements MessageFromFragments {
	private SimpleFacebook mSimpleFacebook;

	protected static final String TAG = MainActivity.class.getName();
	private CooeyBleDeviceManager mCooeyBleManager = null;
	private CooeyProfile activeProfile = null;
	private LsDeviceInfo currentReadingDev = null;
	private PlaceHolderFragment fbPlaceHolder = new PlaceHolderFragment();
	private ProgressBar progbar = null;
	int optionsMenuId = R.menu.login;
	private Boolean isOnBoarding = true;

	private OnBoarding onBrdFrag = null;
	private AddMedicine addMedFrag = null;
	private DeviceList addDevFrag = null;
	private DashBoard dashBrdFrag = null;
	private MyRecommendations recomFrag = null;
	private ProgressDialog ringProgressDialog = null;
	private SendStatsMP mpStats = null;
	private TakeBPReading takeBp = null;
	private TakeWtReading takeWt = null;
	private List<CooeyProfile> childProfiles = null;
	private CooeyProfile activeChild = null;
	private Typeface QS_font, QS_fontbook;
	private ResponseProfile customLoginProfile = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSimpleFacebook = SimpleFacebook.getInstance(this);
		// onBrd = new OnBoarding(new AddMedicine());

		// test local language
		updateLanguage(getApplicationContext(), "en");
		printHashKey(getApplicationContext());

		setContentView(R.layout.activity_main);
		// progbar = (ProgressBar) findViewById(R.id.progbar);
		if (savedInstanceState == null) {

		}
		createFragmentsAndFlow(0);
		QS_fontbook = Typeface.createFromAsset(getAssets(),
				"fonts/Quicksand_Book.otf");

	}

	public ResponseProfile getActiveResponseProfile() {

		return customLoginProfile;
	}

	public Boolean changeBluetoothState(Boolean enable) {
		BluetoothAdapter bluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		boolean isEnabled = bluetoothAdapter.isEnabled();
		if (enable && !isEnabled) {
			return bluetoothAdapter.enable();
		} else if (!enable && isEnabled) {
			return bluetoothAdapter.disable();
		}
		return true;
	}

	private void createFragmentsAndFlow(int flow) {

		if (onBrdFrag == null) {
			onBrdFrag = new OnBoarding(true, false, -1);
			addMedFrag = new AddMedicine();
			addDevFrag = new DeviceList();
			dashBrdFrag = new DashBoard();
			recomFrag = new MyRecommendations();
		}
	}

	public Fragment getNextFlowAddMed() {
		return addDevFrag;
	}

	public Fragment getNextFlowOnBoard() {
		return addMedFrag;
	}

	public Fragment getNextFlowAddDev() {
		return recomFrag;
	}

	public void setActionBarText(String txt) {

		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		LayoutInflater li = LayoutInflater.from(this);
		View rl = (View) li.inflate(R.layout.actionbarlayout, null);

		TextView tv = (TextView) rl.findViewById(R.id.txttitle);
		tv.setText(txt);
		getActionBar().setCustomView(rl);
		tv.setTypeface(QS_fontbook);
	}

	public Fragment getNextFlowRecom() {
		// for(int i =0; i < getFragmentManager().getBackStackEntryCount();
		// ++i){
		// getFragmentManager().popBackStackImmediate(getFragmentManager().getBackStackEntryAt(i).getId(),
		// FragmentManager.POP_BACK_STACK_INCLUSIVE);
		// }
		getFragmentManager().popBackStack(null,
				FragmentManager.POP_BACK_STACK_INCLUSIVE);
		return new DashBoard();
	}

	public Fragment getAddMed() {
		StatsUtil.stat_triedAddMed(this);
		return addMedFrag;
	}

	public Fragment getTakeBp() {
		StatsUtil.stat_takeBPReading(this);
		if (takeBp == null)
			takeBp = new TakeBPReading();
		return takeBp;
	}

	public Fragment getTakeWt() {
		StatsUtil.stat_takeWeightReading(this);
		if (takeWt == null)
			takeWt = new TakeWtReading();
		return takeWt;
	}

	public Fragment getDashboard() {
		// getFragmentManager().popBackStack(null,
		// FragmentManager.POP_BACK_STACK_INCLUSIVE);
		// dashBrdFrag = new DashBoard();
		return dashBrdFrag;
	}

	public Fragment getAddDevice() {
		StatsUtil.stat_triedAddDev(this);
		return addDevFrag;
	}

	public Fragment getOnBoard() {
		return onBrdFrag;
	}

	public Boolean isUserOnboarding() {
		return this.isOnBoarding;
	}

	public void unsetuserOnBoarding() {
		this.isOnBoarding = false;
	}

	public void setuserOnBoarding() {
		this.isOnBoarding = true;
	}

	private void loadLoginActivity() {
		// clear active profile
		// load FB
		getFragmentManager().beginTransaction()
				.replace(R.id.container, fbPlaceHolder)
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();

	}

	@Override
	public void onBackPressed() {

		if (getFragmentManager().getBackStackEntryCount() == 1) {
			this.finish();
		}
		super.onBackPressed();
	}

	private void loadStartActivity() {
		// reset active profile again
		// get active profile
		// dashBrdFrag
		mpStats = SendStatsMP.getInstanceWithContext(this);
		mpStats.getMixpanel().getPeople().showNotificationIfAvailable(this);

		getFragmentManager().beginTransaction()
				.replace(R.id.container, dashBrdFrag).addToBackStack(null)
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();
		optionsMenuId = R.menu.dashboardmenu;
	}

	public void initUsersForNotif(int pid) {
		MixpanelAPI.People people = mpStats.getMixpanel().getPeople();
		people.identify(String.valueOf(pid));
		people.set("pid", pid);
		people.set("email", "a@.com");
		people.initPushHandling("137820779380");
		// people.
	}

	private void loadOnBoardActvity() {
		// TEST
		// clear active profile and re-instantiate object
		mpStats = SendStatsMP.getInstanceWithContext(this);
		getFragmentManager().beginTransaction()
				.replace(R.id.container, onBrdFrag).addToBackStack(null)
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();
		optionsMenuId = R.menu.login;
	}

	private Properties getProfileProperties() {
		PictureAttributes pictureAttributes = Attributes
				.createPictureAttributes();
		pictureAttributes.setType(PictureType.SQUARE);
		pictureAttributes.setHeight(500);
		pictureAttributes.setWidth(500);

		Properties properties = new Properties.Builder().add(Properties.ID)
				.add(Properties.FIRST_NAME)
				.add(Properties.PICTURE, pictureAttributes)
				.add(Properties.LAST_NAME).add(Properties.BIRTHDAY)
				.add(Properties.AGE_RANGE).add(Properties.EMAIL)
				.add(Properties.GENDER).build();
		return properties;
	}

	private class ProfileFromCooeyResponse implements
			SendToCooeyCallbackOnResponse {
		private String url = "http://api.cooey.co.in/ehealth/v1/profile/patients?patientIds=";
		private String patientId;

		public ProfileFromCooeyResponse(String pid) {
			patientId = pid;
		}

		@Override
		public void callme(String resp, int code) {
			try {
				JSONObject jo = new JSONObject(resp);
				// int p = (int) jo.get("patientId");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public String getUrl() {
			url = url + patientId;
			return url;
		}

		@Override
		public void callmeWithContext(String resp, Context context) {
			// TODO Auto-generated method stub

		}
	}

	private class PatientFromCooeyResponse implements
			SendToCooeyCallbackOnResponse {
		private String url = "http://api.cooey.co.in/ehealth/v1/profile/exist?";
		private String email, fname;

		public PatientFromCooeyResponse(String e, String fn) {
			email = e;
			fname = fn;
		}

		@Override
		public void callme(String resp, int code) {
			try {
				JSONObject jo = new JSONObject(resp);
				int p = (int) jo.get("patientId");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public String getUrl() {
			url = url + "externalId=" + email + "&firstName=" + fname.replace(" ", "%20");
			return url;
		}

		@Override
		public void callmeWithContext(String resp, Context context) {
			// TODO Auto-generated method stub

		}

	}

	private int checkProfileInServer(String email, String fname) {
		int pid = 0;
		try {
			String s = new GetJsonFromCooeyTask(new PatientFromCooeyResponse(
					email, fname)).execute("").get();
			JSONObject jo = new JSONObject(s);
			pid = (int) jo.get("patientId");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pid;
	}

	public CooeyProfile getProfileFromServer(String patientId) {
		CooeyProfile cp = null;
		try {
			String profile = new GetJsonFromCooeyTask(
					new ProfileFromCooeyResponse(patientId)).execute("").get();

			JSONObject jo = new JSONObject(profile);
			JSONArray ja = (JSONArray) jo.get("profiles");
			JSONObject prof = (JSONObject) ja.get(0);
			cp = new CooeyProfile();
			cp.setFirstName((String) prof.get("firstName"));
			cp.setLastName((String) prof.get("lastName"));
			cp.setEmail((String) prof.get("email"));
			cp.setWeight((int) prof.get("weight"));
			cp.setGender((String) prof.get("gender"));
			cp.setBloodGroup((String) prof.get("bloodGroup"));
			double ht = (double) prof.get("height");
			cp.setHeight((float) ht);
			cp.setExternalID((String) prof.get("externalID"));
			cp.setDob((String) prof.get("DOB"));

		} catch (InterruptedException | ExecutionException | JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return cp;
	}

	private void retrieveAndSetUserProfile(Profile response) {

		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		CooeyProfile localcp = ds.getProfile(response.getEmail());
		ds.close();
		if (localcp == null) {
			CooeyProfile srvcp = null;
			int pid = 0;
			try {
				pid = checkProfileInServer(response.getId(),
						response.getFirstName());
			} catch (Exception e) {
				ringProgressDialog.dismiss();
			}
			if (pid != 0) {
				srvcp = getProfileFromServer(String.valueOf(pid));
				srvcp.setPatientId(pid);
				srvcp.setExternalID(response.getId());
				isOnBoarding = false;
				// TODO: fill other params too. IMP
				setActiveProfile(createLocalCooeyProfile(srvcp));
				loadStartActivity();
			}
			// if not, then go to onboarding screen
			// else
			// create one from response
			else if (srvcp == null) {
				final CooeyProfile cp = new CooeyProfile();
				cp.setEmail(response.getEmail());
				cp.setFirstName(response.getFirstName());
				cp.setLastName(response.getLastName());
				cp.setGender(response.getGender());
				cp.setExternalID(response.getId());
				cp.setDob(response.getBirthday() == "null" ? "1970-12-01"
						: response.getAgeRange().toString());
				if (response.getBirthday() != "null") {
					SimpleDateFormat FBDateFormat = new SimpleDateFormat(
							"dd/MM/yyyy");
					SimpleDateFormat SrvDateFormat = new SimpleDateFormat(
							"yyyy-MM-dd");
					Date tmp = null;
					try {
						tmp = FBDateFormat.parse(response.getBirthday());
						cp.setDob(SrvDateFormat.format(tmp));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				cp.setMobileNumber("000");
				String mProfJson = new GsonBuilder()
						.excludeFieldsWithoutExposeAnnotation().create()
						.toJson(cp);

				try {
					new SendJsonToCooeyTask(
							new SendToCooeyCallbackOnResponse() {

								@Override
								public String getUrl() {
									return "http://api.cooey.co.in/ehealth/v1/profile/patient";
								}

								@Override
								public void callme(String resp, int code) {
									try {
										JSONObject jo = new JSONObject(resp);
										int pid = (int) jo.get("patientId");
										cp.setPatientId(pid);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									setActiveProfile(createLocalCooeyProfile(cp));
								}

								@Override
								public void callmeWithContext(String resp,
										Context context) {
									// TODO Auto-generated method stub

								}
							}).execute(mProfJson).get();
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// setActiveProfile(createLocalCooeyProfile(cp));
				// send to server to create the profile and get the active
				// patientId.
				loadOnBoardActvity();
			}
		} else {
			localcp.getDbid();
			setActiveProfile(localcp);
			loadStartActivity();
			isOnBoarding = false;
			// take to dasboard
		}
		return;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.login, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private CooeyProfile getLoggedInProfile() {
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		CooeyProfile cp = ds.getLoggedInProfile();
		ds.close();
		return cp;
	}

	private void initiateAppStartPostLogin(CooeyProfile cp) {

		isOnBoarding = false;
		setActiveProfile(cp);
		loadStartActivity();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// check google play services
		// int gplayservice =
		// com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		mSimpleFacebook = SimpleFacebook.getInstance(this);
		createFragmentsAndFlow(0);

		if (mSimpleFacebook.isLogin()) {
			// get the last active login
			CooeyProfile cp = getLoggedInProfile();
			if (cp == null) {

				ringProgressDialog = ProgressDialog.show(MainActivity.this,
						"Please wait ...", "Cooey is retreiving your profile",
						true);
				ringProgressDialog.setCancelable(true);

				mSimpleFacebook.getProfile(getProfileProperties(),
						new OnProfileListener() {

							@Override
							public void onComplete(final Profile response) {
								runOnUiThread(new Runnable() {
									public void run() {
										retrieveAndSetUserProfile(response);
										ringProgressDialog.dismiss();
									}
								});
							}

							@Override
							public void onFail(String reason) {
								// TODO Auto-generated method stub
								super.onFail(reason);
								Toast.makeText(
										MainActivity.this,
										"Unable to read your profile from facebook, try again",
										Toast.LENGTH_SHORT).show();
								ringProgressDialog.dismiss();
							}

							@Override
							public void onThinking() {
								// TODO Auto-generated method stub
								super.onThinking();
							}
						});

			} else {
				initiateAppStartPostLogin(cp);
			}

		} else if (customLoginProfile != null) {

			CooeyProfile cp = getLoggedInProfile();
			if (cp == null) {

				CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
				ds.open();
				CooeyProfile localcp = ds.getProfile(customLoginProfile
						.getEmail());
				ds.close();
				if (localcp == null) {
					ringProgressDialog = ProgressDialog.show(MainActivity.this,
							"Please wait ...",
							"Cooey is retreiving your profile", true);
					ringProgressDialog.setCancelable(true);

					CooeyProfile srvcp = null;
					int pid = 0;
					try {
						pid = checkProfileInServer(customLoginProfile.getId(),
								customLoginProfile.getFirstname());
					} catch (Exception e) {
						// ringProgressDialog.dismiss();
					}
					if (pid != 0) {
						srvcp = getProfileFromServer(String.valueOf(pid));
						srvcp.setPatientId(pid);
						srvcp.setExternalID(customLoginProfile.getId());
						setActiveProfile(createLocalCooeyProfile(srvcp));
						if (isOnBoarding) {
							loadOnBoardActvity();
						} else {
							isOnBoarding = false;
							// TODO: fill other params too. IMP
							loadStartActivity();
						}
					} else {
						// something went wrong
						Toast.makeText(this,
								"Oops, something went wrong,\n Contact Cooey.",
								Toast.LENGTH_SHORT).show();
					}
					ringProgressDialog.dismiss();
				} else {
					localcp.getDbid();
					setActiveProfile(localcp);
					loadStartActivity();
					isOnBoarding = false;
				}
			} else {
				initiateAppStartPostLogin(cp);
			}
		} else if (customLoginProfile == null) {// this is our HASH to find who
												// is logged in. TODO: change
												// it.
			CooeyProfile cp = getLoggedInProfile();
			if (cp == null) {
				loadLoginActivity();
			} else {
				initiateAppStartPostLogin(cp);
			}
		} else {
			loadLoginActivity();
		}

	}

	public void onCustomLoginResult(ResponseProfile rp, Boolean onboard) {
		customLoginProfile = rp;
		isOnBoarding = onboard;
		onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);

		// login successful
		if (resultCode != 0) { // real, 0

			Toast.makeText(this, "Successfully logged in", Toast.LENGTH_SHORT)
					.show();
			getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
					WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
			StatsUtil.stat_FBLoginSuccess(this);
			// else onboard screen
			// loadOnBoardActvity();
			// getFragmentManager().beginTransaction().replace(R.id.container,
			// new DashBoard() ).setTransition(
			// FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
		} else {
			Toast.makeText(this, "Unable to login, try again",
					Toast.LENGTH_SHORT).show();
		}
	}

	public List<LsDeviceInfo> getDevicesForCurrentUser() {
		// getCurrent active users' uid.
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(this);
		ds.open();
		List<LsDeviceInfo> lst = ds.getDevicesForUser(activeProfile.getDbid()); // uid
		ds.close();

		return lst;
	}

	public List<MedicineData> getMedicinesForCurrentUser() {
		CooeyMedDataSource ds = new CooeyMedDataSource(this);
		ds.open();
		List<MedicineData> lst = ds.getMedicinesForUser(getActiveProfile()
				.getDbid(), null);
		ds.close();
		return lst;
	}

	public void setCurrentDeviceToRead(LsDeviceInfo dev) {
		currentReadingDev = dev;
	}

	public LsDeviceInfo getCurrentDeviceToRead() {
		return currentReadingDev;
	}

	public void removeDevice(LsDeviceInfo dev) {
		CooeyDeviceDataSource ds = new CooeyDeviceDataSource(this);
		ds.open();
		ds.getRemoveDeviceForUser(dev, getActiveProfile().getDbid()); // uid
		ds.close();
	}

	public void removeMedicine(MedicineData med) {
		CooeyMedDataSource ds = new CooeyMedDataSource(this);
		ds.open();
		CooeyStatus ret = ds.removeMedicineForUser(med, 1);
		ds.close();
		if (ret.getStatus() != CooeyCode.COOEY_OK) {
			Toast.makeText(this, "Error in deleting medicine",
					Toast.LENGTH_SHORT).show();
		}
		// send to server for removal
		MedServerDataDel del = new MedServerDataDel();
		del.medicineId = med.getMedicineId();
		del.profileId = getActiveProfile().getPatientId();
		String mDelJson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation().create().toJson(del);
		new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {

			@Override
			public String getUrl() {
				return "http://api.cooey.co.in/ehealth/v1/profile/medicine/delete";
			}

			@Override
			public void callme(String resp, int code) {
				// TODO Auto-generated method stub
				int x = 0;
			}

			@Override
			public void callmeWithContext(String resp, Context context) {
				// TODO Auto-generated method stub

			}
		}).execute(mDelJson);

	}

	public void addBPForUser(BPData bpd) {
		// get current active users' id
		// update bp table for that user
		CooeyBPDataSource ds = new CooeyBPDataSource(this);
		ds.open();
		ds.createBPForUser(bpd, getActiveProfile().getDbid());
		ds.close();
	}

	public void addWtForUser(WeightData wd) {
		// get current active users' id
		// update bp table for that user
		CooeyWTDataSource ds = new CooeyWTDataSource(this);
		ds.open();
		ds.createWTForUser(wd, 1);
		ds.close();

		//
		WTServerDataPut wtsrv = new WTServerDataPut();
		wtsrv.weight = (float) wd.getWeightKg();
		wtsrv.BMI = (float) wd.getBmi();
		if (wd.getBodyFatRatio() > 0) {
			wtsrv.bodyFat = (float) wd.getBodyFatRatio();
		}
		;
		wtsrv.boneDensity = (float) wd.getBoneDensity();
		wtsrv.muscleMass = (float) wd.getMuscleMassRatio();
		wtsrv.patientId = getActiveProfile().getPatientId();
		wtsrv.TID = "androidapp";
		wtsrv.trackingId = "android";
		wtsrv.timeStamp = new Date().getTime();
		wtsrv.totalWater = (float) wd.getBodyWaterRatio();

		String mWtJson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation().create().toJson(wtsrv);
		Log.d(" Weight DATA TO POST ", mWtJson);
		new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {

			@Override
			public String getUrl() {
				return "http://api.cooey.co.in/ehealth/v1/actvity/weight";
			}

			@Override
			public void callme(String resp, int code) {
				// TODO Auto-generated method stub
				int x = 0;
			}

			@Override
			public void callmeWithContext(String resp, Context context) {
				// TODO Auto-generated method stub

			}
		}).execute(mWtJson);
	}

	public void registerOrFindUser(CooeyProfile cp) {

		// find the user if already present
		// if present, retrieve and create a CooeyProfile
		// if not, make an entry in the DB and create a CooeyProfile
		// finally, make that as active profile
	}

	public CooeyBleDeviceManager getBleLeManager() {
		if (mCooeyBleManager != null)
			return mCooeyBleManager;

		CooeyBleDeviceFactory mFactory = new CooeyBleDeviceFactory(
				new int[] { CooeyDeviceType.BP_METER,
						CooeyDeviceType.WEIGHT_AND_FAT_SCALE });
		mCooeyBleManager = (CooeyBleDeviceManager) mFactory
				.getBleDeviceManager();
		mCooeyBleManager.initialize(getActiveProfile(), this);

		return mCooeyBleManager;
	}

	@Override
	protected void onPause() {
		if (mCooeyBleManager != null)
			mCooeyBleManager.shutdown();
		changeBluetoothState(false);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (mCooeyBleManager != null)
			mCooeyBleManager.shutdown();
		changeBluetoothState(false);
		super.onDestroy();
	}

	public void updateActiveProfile(CooeyProfile cp) {
		CooeyProfile newcp = getActiveProfile();
		// newcp.setBloodGroup(cp.getBloodGroup());
		// TODO, all the remaining & set it back to active
		setActiveProfile(newcp);
	}

	public CooeyProfile createLocalCooeyProfile(CooeyProfile cp) {
		if (cp.getNickName() == "")
			cp.setNickName(cp.getFirstName());
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		CooeyProfile cp1 = ds.createProfile(cp);
		ds.close();
		return cp1;
	}

	/*
	 * @Override protected void onSaveInstanceState(Bundle outState) {
	 * 
	 * CooeyProfile cp = getActiveProfile(); outState.putLong("lastParent",
	 * cp.getParentId()); super.onSaveInstanceState(outState); }
	 * 
	 * 
	 * @Override protected void onRestoreInstanceState(Bundle
	 * savedInstanceState) { long parentId =
	 * savedInstanceState.getLong("lastParent"); if(parentId > 0){ activeProfile
	 * = null; getActiveProfile(); }
	 * super.onRestoreInstanceState(savedInstanceState); }
	 */

	public void switchToParent() {
		CooeyProfile current = getActiveProfile();

		CooeyProfile newprof = current;
		if (current.getParentId() > 0) {// when switch activity happens.
			CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
			ds.open();
			newprof = ds.getProfile(current.getParentId());
			ds.close();
		}

		setActiveProfile(newprof);
		dashBrdFrag = new DashBoard();
		// bring back to Dashboard
		getFragmentManager().popBackStack();
		getFragmentManager().beginTransaction()
				.replace(R.id.container, dashBrdFrag).addToBackStack(null)
				.commit();

	}

	public void loadAllChildProfiles() {

		CooeyProfile parent = getActiveProfile();
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		childProfiles = ds.getAllProfiles(parent.getDbid());
		ds.close();
	}

	private void updateChildProfilesForCurrentParent(CooeyProfile cp) {
		childProfiles.add(cp);
	}

	public List<CooeyProfile> getAllChildProfiles() {
		return childProfiles;
	}

	public void setCurrentChildProfile(CooeyProfile cp) {
		// TODO clear all logins
		resetActiveProfile(cp);

		// bring back to Dashboard
		dashBrdFrag = new DashBoard();
		// getFragmentManager().popBackStack();
		getFragmentManager().beginTransaction()
				.replace(R.id.container, dashBrdFrag).addToBackStack(null)
				.commit();
	}

	private void updateActiveChildPatientId(final int pid) {

		activeChild.setPatientId(pid);
		for (int i = 0; i < childProfiles.size(); ++i) {
			if (childProfiles.get(i).getDbid() == activeChild.getDbid()) {
				childProfiles.get(i).setPatientId(pid);
				break;
			}
		}
		// TODO update DB
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		ds.updateProfileWithPatient(activeChild.getDbid(), pid);
		ds.close();

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				LinkProfile lp = new LinkProfile();
				lp.baseRelationId = getActiveProfile().getPatientId();
				lp.toRelationId = pid;
				lp.trackingId = "androidapp";
				lp.tid = "TID";
				String linkJson = new GsonBuilder()
						.excludeFieldsWithoutExposeAnnotation().create()
						.toJson(lp);

				try {
					new SendJsonToCooeyTask(
							new SendToCooeyCallbackOnResponse() {
								@Override
								public String getUrl() {
									return "http://api.cooey.co.in/ehealth/v1/profile/patient/relation";
								}

								@Override
								public void callme(String resp, int code) {
									JSONObject jo;
									int srvpid = 0;
									try {
										jo = new JSONObject(resp);
										srvpid = (int) jo.get("patientId");
									} catch (JSONException e) {

										e.printStackTrace();
									}
									if (srvpid == pid) {
										Toast.makeText(getApplicationContext(),
												"Profile created successfully",
												Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(getApplicationContext(),
												"Profile link failed",
												Toast.LENGTH_SHORT).show();
									}
								}

								@Override
								public void callmeWithContext(String resp,
										Context context) {
									// TODO Auto-generated method stub

								}
							}).execute(linkJson).get();

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	public void updateCurrentProfile(final CooeyProfile cp, final Boolean doesHandleTrans) {

		resetActiveProfile(cp);
		// TODO: update DB
		updateActiveProfileDB(cp);
		
		// TODO: update server
		UpdateProfile up = new UpdateProfile();
		up.setBloodGroup(cp.getBloodGroup());
		up.setDob(cp.getDob());
		up.setEmail(cp.getEmail());
		up.setExternalID(cp.getExternalID());
		up.setFirstName(cp.getFirstName());
		up.setLastName(cp.getLastName());
		up.setGender(cp.getGender());
		up.setHeight(cp.getHeight());
		up.setId(cp.getPatientId());
		up.setLocationCity(cp.getLocationCity());
		up.setLocationCountry(cp.getLocationCountry());
		up.setMobileNumber(cp.getMobileNumber());
		up.setProfilePic("");
		up.setWeight(cp.getWeight());
		String mUpdProfJson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation().create().toJson(up);
		new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {

			@Override
			public String getUrl() {
				// TODO Auto-generated method stub
				return "http://api.cooey.co.in/ehealth/v1/profile/patient/update";
			}

			@Override
			public void callme(String resp, int code) {
			}

			@Override
			public void callmeWithContext(String resp, final Context context) {
				try {
					JSONObject jo = new JSONObject(resp);
					int pid = (int) jo.get("patientId");
					if (pid == cp.getPatientId()) {
						if(!doesHandleTrans) return;
						((Activity) context).runOnUiThread(new Runnable() {

							@Override
							public void run() {
								getFragmentManager()
										.beginTransaction()
										.replace(
												R.id.container,
												((MainActivity) context)
														.getDashboard(), "")
										.addToBackStack(null).commit();
							}
						});
					} else {
						((Activity) context).runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(context,
										"Failed to update profile.",
										Toast.LENGTH_SHORT).show();
							}
						});
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}, this, true, "Updating Profile, please wait.").execute(mUpdProfJson);

	}

	public void addChildProfile(CooeyProfile newcp) {
		long parentId = getActiveProfile().getDbid();
		String extId = getActiveProfile().getExternalID();
		newcp.setParentId(parentId);
		newcp.setType(2);
		// newcp.setExternalID(extId);

		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		CooeyProfile cp1 = ds.createProfile(newcp);
		ds.close();

		updateChildProfilesForCurrentParent(cp1);

		// populate server
		activeChild = cp1;

		String mProfJson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation().create()
				.toJson(activeChild);
		try {
			new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {
				@Override
				public String getUrl() {
					return "http://api.cooey.co.in/ehealth/v1/profile/patient";
				}

				@Override
				public void callme(String resp, int code) {
					try {
						JSONObject jo = new JSONObject(resp);
						int pid = (int) jo.get("patientId");
						updateActiveChildPatientId(pid);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				@Override
				public void callmeWithContext(String resp, Context context) {
					// TODO Auto-generated method stub

				}
			}).execute(mProfJson).get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dashBrdFrag = new DashBoard();
		// bring back to Dashboard
		getFragmentManager().popBackStack();
		getFragmentManager().beginTransaction()
				.replace(R.id.container, dashBrdFrag).addToBackStack(null)
				.commit();
	}

	public void setActiveProfile(CooeyProfile cp) {
		activeProfile = cp;
		// update DB to have this user's loggedin flag
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		ds.setProfileLoggedInStatus(cp.getDbid());
		ds.close();
	}

	public void logoutActiveProfile(long uid) {
		activeProfile = null;
		// update DB to have this user's loggedin flag
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		ds.resetProfileLoggedInStatus(uid);
		ds.close();
		customLoginProfile = null;
	}

	public void resetActiveProfile(CooeyProfile cp) {
		activeProfile = cp;
	}

	public CooeyProfile getActiveProfile() {
		if (activeProfile == null) {
			CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
			ds.open();
			activeProfile = ds.getLoggedInProfile();
			ds.close();
		}
		return activeProfile;
	}

	public CooeyProfile updateActiveProfileDB(CooeyProfile newcp) {
		
		CooeyProfileDataSource ds = new CooeyProfileDataSource(this);
		ds.open();
		ds.updateFullProfile(newcp);
		ds.close();
		return activeProfile;
	}

	private void popAllFragments() {

		int count = getFragmentManager().getBackStackEntryCount();
		for (int i = 0; i < count; ++i) {
			getFragmentManager().popBackStack(
					getFragmentManager().getBackStackEntryAt(i).getId(),
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}

	}

	public void fbLogout() {

		final OnLogoutListener onLogoutListener = new OnLogoutListener() {

			@Override
			public void onFail(String reason) {
				Log.w(TAG, "Failed to login");
			}

			@Override
			public void onException(Throwable throwable) {
				Log.e(TAG, "Bad thing happened", throwable);
			}

			@Override
			public void onThinking() {
				// show progress bar or something to the user while login is
				// happening
			}

			@Override
			public void onLogout() {
				// change the state of the button or do whatever you want
				Log.e(TAG, "logged out");
				popAllFragments();
			}

		};
		mSimpleFacebook.logout(onLogoutListener);
		loadLoginActivity();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.menulogout) {

			AlertDialog.Builder builder = new AlertDialog.Builder(this)
					.setTitle("Do you want to logout?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									// if(mSimpleFacebook.isLogin()){
									fbLogout();
									// }
									getActionBar().setTitle("Cooey");
									CooeyProfile cp = getActiveProfile();
									long uid = 0;
									if (cp.getParentId() > 0) {
										uid = cp.getParentId();
									} else {
										uid = cp.getDbid();
									}
									logoutActiveProfile(uid);
									dialog.dismiss();
								}

							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}

							});
			builder.create().show();
			StatsUtil.stat_FBLogout(this);
			StatsUtil.stat_CustomLogout(this);
			return true;
		}
		if (id == R.id.viewmydevices) {
			getFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			getFragmentManager().beginTransaction()
					.replace(R.id.container, new MyDevices())
					.addToBackStack(null).commit();
		}
		return super.onOptionsItemSelected(item);
	}

	public static void printHashKey(Context context) {
		try {
			String TAG = "com.biz.health.cooey_app";
			PackageInfo info = context.getPackageManager().getPackageInfo(TAG,
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String keyHash = Base64.encodeToString(md.digest(),
						Base64.DEFAULT);
				Log.d(TAG, "keyHash: " + keyHash);
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
	}

	public boolean haveNetworkConnection() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	@Override
	public void OnMessage(FragmentToActivityPayload p) {
		// TODO Auto-generated method stub

		if (p.getFragment_name() == PlaceHolderFragment.class.getName()) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, onBrdFrag).commit();
		}

	}

	/**
	 * Update language
	 * 
	 * @param code
	 *            The language code. Like: en, cz, iw, ...
	 */
	public static void updateLanguage(Context context, String code) {
		Locale locale = new Locale(code);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getResources().updateConfiguration(config,
				context.getResources().getDisplayMetrics());
	}

}
