package com.biz.health.cooey_app;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.biz.cooey.GetJsonFromCooeyTask;
import com.biz.cooey.SendToCooeyCallbackOnResponse;
import com.biz.health.model.RecommendationData;

public class MyRecommendations extends Fragment {

	private List<RecommendationData> currentRecs = null;
	private MyRecommendAdapter<RecommendationData> myRecAdapter = null;
	private Button mNxtButton;
	private int recType = 0; //full
	private Typeface QS_font;
	private Typeface QS_fontbook;
	
	public MyRecommendations()
	{
		
	}
	
	public void setRecommendationtype(int type){
		recType = type;
	}
	private void setDefaultActionBarTitle(){
		getActivity().getActionBar().setTitle("RECOMMENDATIONS");
	}
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		setDefaultActionBarTitle();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setDefaultActionBarTitle();
	}
	
	private class RecommendsFromCooeyResponse implements SendToCooeyCallbackOnResponse {
		private String url = "http://manage.cooey.co.in/ehealth/v1/suggest/onboard/recommendations/";
		private String patientId;
		
		public RecommendsFromCooeyResponse(String pid){
			url += pid;
			patientId = pid;			
		}
		@Override
		public void callme(String resp, int code) {
			try {
				JSONObject jo = new JSONObject(resp);
				//ask the server to send empty array, not null.
				if(jo.isNull("recommendations")){
					return;
				}
				JSONArray recs = (JSONArray)jo.get("recommendations");
				for( int i =0; i < recs.length(); ++i){
					JSONObject jorec = (JSONObject) recs.get(i);
					if(recType == 0){
						RecommendationData tmp = new RecommendationData();
						tmp.setCategory(jorec.getString("Category"));
						tmp.setText(jorec.getString("text"));
						tmp.setType(jorec.getString("Type"));
						currentRecs.add(tmp);
					}else{//only onboarding recommendations
						if(jorec.getString("Category").toLowerCase().equals("onboard")){
							RecommendationData tmp = new RecommendationData();
							tmp.setText(jorec.getString("text"));
							tmp.setType(jorec.getString("Type"));
							currentRecs.add(tmp);
						}
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(getActivity() == null) return;//the fragment is destroyed before the response comes.
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
						myRecAdapter.notifyDataSetChanged();					
				}
			});
			
		}
		
		@Override
		public String getUrl() {
			return url;
		}
		@Override
		public void callmeWithContext(String resp, Context context) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private void getRecommendationsForCurrentUser(){
		//get active profile
		int pid = ((MainActivity)getActivity()).getActiveProfile().getPatientId();
		new GetJsonFromCooeyTask(new RecommendsFromCooeyResponse(String.valueOf(pid))).execute("");
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.my_recommends, container,
				false);
		
		ListView myMedList = (ListView)rootView.findViewById(R.id.MyRecLst);
		mNxtButton = (Button)rootView.findViewById(R.id.btnRecomNext);

		currentRecs = new ArrayList<RecommendationData>();
		getRecommendationsForCurrentUser();
		
		Toast.makeText(getActivity(), "Fetching from Cooey...", Toast.LENGTH_SHORT).show();		
		
		myRecAdapter = new MyRecommendAdapter<RecommendationData>(getActivity(), R.layout.dashboardrec, currentRecs);
		myMedList.setAdapter(myRecAdapter);
		
		if(!((MainActivity)getActivity()).isUserOnboarding()){
			mNxtButton.setText("Back");
			mNxtButton.setBackgroundResource(R.drawable.lt2brdvwdark2);
			mNxtButton.setTextColor( getActivity().getResources().getColor(R.color.applt3));
		}
		mNxtButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Fragment f = ((MainActivity)getActivity()).getNextFlowRecom();
				
				//if(context.isUserOnboarding()){
					//getActivity().getFragmentManager().popBackStackImmediate(getFragmentManager().getBackStackEntryAt(0).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
					//context.unsetuserOnBoarding();
				//}
				
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.container, f, "");//.commit();
				ft.addToBackStack(null).commit();
			}
		});
		
		return rootView;
	}
	
	
	
}
