package com.biz.health.cooey_app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyDeviceDataReceiveCallback;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.WeightData;
import com.biz.health.model.MedicineData;
import com.biz.health.utils.db.CooeyBPDataSource;
import com.biz.health.utils.db.CooeyWTDataSource;
import com.github.mikephil.charting.charts.BarLineChartBase.BorderPosition;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.LineRadarDataSet;
import com.github.mikephil.charting.interfaces.OnChartGestureListener;
import com.github.mikephil.charting.interfaces.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.YLabels;
import com.lifesense.ble.bean.LsDeviceInfo;

public class TakeWtReading extends Fragment implements OnChartGestureListener,
		OnChartValueSelectedListener {

	private CooeyBleDeviceManager mManager = null;
	private LineChart mChart;
	private WeightData mLastKnownWt = null;
	private LineData LnData = null;
	ArrayList<Entry> yWts = new ArrayList<Entry>();
	ArrayList<String> xVals = new ArrayList<String>();
	private ArrayList<LineDataSet> dataSets = null;
	private Activity mCtx;
	TabHost tabHost = null;
	private int chartValueToDisplay = 0; // 0-weight, 1-bmi
	private LineChart lineChart = null;
	private ListView lvWeights;
	private Button btnch = null;
	private Button btnlv = null;
	private CooeyProfile activeProfile = null;
	private View rootView = null;
	private List<WeightData> lstwd = null;
	private WeightListAdapter<WeightData> wtadapter = null;
	TextView tvw, tvbmi, tvbmidec, tvfat ,tvfatdec , tvbone ,tvbonedec, tvwtr, tvwtrdec, tvmus, tvmusdec;

	@Override
	public void onAttach(Activity activity) {
		mCtx = activity;
		super.onAttach(activity);
	}

	@Override
	public void onDestroyView() {

		super.onDestroyView();
		mManager.shutdown();
		((MainActivity) getActivity()).changeBluetoothState(false);
	}

	private void prepareChart(View rootView) {
		mChart = (LineChart) rootView.findViewById(R.id.wtchart);
		mChart.setOnChartGestureListener(this);
		mChart.setOnChartValueSelectedListener(this);
		mChart.setValueTextColor(Color.WHITE);

		mChart.setUnit("");
		mChart.setDrawUnitsInChart(true);

		// if enabled, the chart will always start at zero on the y-axis
		mChart.setStartAtZero(false);

		// disable the drawing of values into the chart
		mChart.setDrawYValues(true);

		mChart.setDrawBorder(true);
		mChart.setBorderPositions(new BorderPosition[] { BorderPosition.BOTTOM });

		// no description text
		mChart.setDescription("");
		mChart.setNoDataTextDescription("No data available yet for the chart.");

		// enable value highlighting
		mChart.setHighlightEnabled(true);
		mChart.setMaxVisibleValueCount(5);

		// enable touch gestures
		mChart.setTouchEnabled(true);

		// enable scaling and dragging
		mChart.setDragEnabled(true);
		mChart.setScaleEnabled(true);
		mChart.setDrawGridBackground(false);
		mChart.setDrawVerticalGrid(false);
		mChart.setDrawHorizontalGrid(true);

		// if disabled, scaling can be done on x- and y-axis separately
		mChart.setPinchZoom(true);
	}

	public void initializeLineDataSetForPlotting() {

		// yWts.clear(); xVals.clear();
		// yWts.add(new Entry((float) 70.0, 0));
		// xVals.add(" ");

		LineDataSet set1 = new LineDataSet(yWts, "Weight (in Kgs)");
		set1.setColor(ColorTemplate.getHoloBlue());
		set1.setCircleColor(ColorTemplate.getHoloBlue());
		set1.setLineWidth(2f);
		set1.setCircleSize(4f);
		set1.setFillAlpha(65);
		set1.setFillColor(ColorTemplate.getHoloBlue());
		set1.setHighLightColor(Color.rgb(244, 117, 117));

		dataSets = new ArrayList<LineDataSet>();
		dataSets.add(set1); // add the datasets
		set1.setColor(mCtx.getResources().getColor(R.color.DarkGreen));
		set1.setCircleColor(mCtx.getResources().getColor(R.color.DarkGreen));
		// create a data object with the datasets
		LnData = new LineData(xVals, dataSets);

	}

	private LineData prepareLineDataSetForPlotting() {
		CooeyWTDataSource ds = new CooeyWTDataSource(getActivity());
		ds.open();
		List<WeightData> lst = ds.getWTValuesForUser(1, null);

		ds.close();

		if (lst.size() == 0) {
			yWts.add(new Entry((float) 0.0, 0));
			xVals.add("");
		}
		for (int i = 0; i < lst.size(); ++i) {

			yWts.add(new Entry((float) lst.get(i).getWeightKg(), i));
			xVals.add(lst.get(i).getDate());
		}
		// create a dataset and give it a type
		LineDataSet set1 = new LineDataSet(yWts, "Weight");
		set1.setColor(ColorTemplate.getHoloBlue());
		set1.setCircleColor(ColorTemplate.getHoloBlue());
		set1.setLineWidth(2f);
		set1.setCircleSize(4f);
		set1.setFillAlpha(65);
		set1.setFillColor(ColorTemplate.getHoloBlue());
		set1.setHighLightColor(Color.rgb(244, 117, 117));

		ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
		dataSets.add(set1); // add the datasets

		// create a data object with the datasets
		LnData = new LineData(xVals, dataSets);
		return LnData;
	}

	private String getSQLForToday() {
		return " date(date, 'start of day') > date('now', 'start of day') ";
	}

	private String getSQLForLastWeek() {
		return " date(date, 'start of day') >= date('now', '-7 days') ";
	}

	private String getSQLForLastMonth() {
		return " date(date, 'start of day') >= date('now', '-30 days') ";// start
																			// of
																			// month
	}

	private void showChartFor(String dayrange) {

		CooeyWTDataSource ds = new CooeyWTDataSource(getActivity());
		ds.open();
		// List<WeightData> lst = null;
		if (dayrange.contains("today")) {
			lstwd = ds.getWTValuesForUser(activeProfile.getDbid(),
					getSQLForToday(), null);// " date DESC ");
			//
		} else if (dayrange.contains("week")) {
			lstwd = ds.getWTValuesForUser(activeProfile.getDbid(),
					getSQLForLastWeek(), null);
		} else if (dayrange.contains("month")) {
			lstwd = ds.getWTValuesForUser(activeProfile.getDbid(),
					getSQLForLastMonth(), null);
		} else {
			lstwd = ds.getWTValuesForUser(activeProfile.getDbid(), null, null);
		}
		ds.close();
		if (lstwd.size() == 0)
			return;

		yWts.clear();
		xVals.clear();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat todf = new SimpleDateFormat("MMM dd");
		for (int i = 0; i < lstwd.size(); ++i) {

			yWts.add(new Entry((float) lstwd.get(i).getWeightKg(), i));

			Date d;
			try {
				d = df.parse(lstwd.get(i).getDate());
				xVals.add(todf.format(d));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/*
		 * Collections.sort(lst, new Comparator<WeightData>() {
		 * 
		 * @Override public int compare(WeightData w1, WeightData w2) {
		 * if(w1.getWeightKg() > w2.getWeightKg()) return 1; else
		 * if(w2.getWeightKg() > w1.getWeightKg()) return -1; else return 0; }
		 * });
		 */
		if (LnData == null) {
			initializeLineDataSetForPlotting();
		}
		mChart.setData(LnData);

		XLabels xl = mChart.getXLabels();
		xl.setTextColor(getActivity().getResources().getColor(
				R.color.greycircle));

		YLabels yl = mChart.getYLabels();
		yl.setTextColor(getActivity().getResources().getColor(
				R.color.greycircle));
		yl.setLabelCount(5);

		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// mChart.setYRange(min - 20, max + 20, true);
				mChart.invalidate();
				//mChart.animateX(1000);
				// mChart.invalidate();
				wtadapter.notifyDataSetChanged();
			}
		});
		
	}

	private void showChartForToday() {

		CooeyWTDataSource ds = new CooeyWTDataSource(getActivity());
		ds.open();
		List<WeightData> lst = ds.getWTValuesForUser(activeProfile.getDbid(),
				" date(date, 'start of day') = date('now', 'start of day') ",
				null);
		ds.close();

		if (lst.size() == 0) {
			return;
		}

		yWts.clear();
		xVals.clear();
		for (int i = 0; i < lst.size(); ++i) {

			yWts.add(new Entry((float) lst.get(i).getWeightKg(), i));
			xVals.add(lst.get(i).getDate());
		}

		/*
		 * Collections.sort(lst, new Comparator<WeightData>() {
		 * 
		 * @Override public int compare(WeightData w1, WeightData w2) {
		 * if(w1.getWeightKg() > w2.getWeightKg()) return 1; else
		 * if(w2.getWeightKg() > w1.getWeightKg()) return -1; else return 0; }
		 * });
		 */
		if (LnData == null) {
			initializeLineDataSetForPlotting();
		}
		mChart.setData(LnData);

		XLabels xl = mChart.getXLabels();
		xl.setTextColor(getActivity().getResources().getColor(
				R.color.greycircle));

		YLabels yl = mChart.getYLabels();
		yl.setTextColor(getActivity().getResources().getColor(
				R.color.greycircle));
		yl.setLabelCount(5);

		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// mChart.setYRange(min - 20, max + 20, true);
				mChart.animateX(1000);
				// mChart.invalidate();
			}
		});

	}

	private void showChartForAllCurrent() {
		CooeyWTDataSource ds = new CooeyWTDataSource(getActivity());
		ds.open();
		List<WeightData> lst = ds.getWTValuesForUser(1, null);
		ds.close();

		if (lst.size() == 0) {
			return;
		}
		yWts.clear();
		xVals.clear();
		for (int i = 0; i < lst.size(); ++i) {
			if (chartValueToDisplay == 1) // bmi
			{
				yWts.add(new Entry((float) lst.get(i).getBmi(), i));
			}
			if (chartValueToDisplay == 0)
				yWts.add(new Entry((float) lst.get(i).getWeightKg(), i));
			xVals.add(lst.get(i).getDate());
		}

		/*
		 * Collections.sort(lst, new Comparator<WeightData>() {
		 * 
		 * @Override public int compare(WeightData w1, WeightData w2) {
		 * if(w1.getWeightKg() > w2.getWeightKg()) return 1; else
		 * if(w2.getWeightKg() > w1.getWeightKg()) return -1; else return 0; }
		 * });
		 */
		if (LnData == null) {
			initializeLineDataSetForPlotting();
		}
		mChart.setData(LnData);

		XLabels xl = mChart.getXLabels();
		xl.setTextColor(getActivity().getResources().getColor(
				R.color.greycircle));

		YLabels yl = mChart.getYLabels();
		yl.setTextColor(getActivity().getResources().getColor(
				R.color.greycircle));
		yl.setLabelCount(5);

		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// mChart.setYRange(min - 20, max + 20, true);
				mChart.animateX(1000);
				// mChart.invalidate();
			}
		});
	}

	private void selectListView() {

		lvWeights.setVisibility(View.VISIBLE);
		btnch.setBackgroundColor(getActivity().getResources().getColor(
				R.color.applt2bkgnd));
		btnlv.setBackgroundColor(getActivity().getResources().getColor(
				R.color.appdarkbgnd));
		lineChart.setVisibility(View.GONE);
	}

	private void selectGraphView() {
		btnlv.setBackgroundColor(getActivity().getResources().getColor(
				R.color.applt2bkgnd));
		btnch.setBackgroundColor(getActivity().getResources().getColor(
				R.color.appdarkbgnd));
		lvWeights.setVisibility(View.GONE);
		lineChart.setVisibility(View.VISIBLE);
	}

	private void resetTrendButtons() {
		Button btn1d = (Button) rootView.findViewById(R.id.btn1d);
		Button btn1w = (Button) rootView.findViewById(R.id.btn1w);
		Button btn1m = (Button) rootView.findViewById(R.id.btn1m);
		btn1d.setBackgroundResource(R.drawable.gr2brdvwgr2);
		btn1w.setBackgroundResource(R.drawable.gr2brdvwgr2);
		btn1m.setBackgroundResource(R.drawable.gr2brdvwgr2);
	}

	private void displayValueOnScreen(WeightData wd) {

		tvw.setText(String.format("%.2f", wd.getWeightKg()) + " kgs");

		String[] tmp = String.format("%.2f", wd.getBmi()).split("\\.");
		if (wd.getBmi() < 0) {
			tmp[0] = "00";
			Toast.makeText(
					getActivity(),
					"Cooey recommends standing on bare foot for correct values.",
					Toast.LENGTH_LONG).show();
		}
		if (tmp.length != 2)
			tmp[1] = ".00";
		tvbmi.setText(tmp[0]);
		tvbmidec.setText("." + tmp[1]);

		tmp = String.format("%.2f", wd.getBodyFatRatio()).split("\\.");
		if (wd.getBodyFatRatio() < 0) {
			tmp[0] = "00";
			tmp[1] = "00";
			Toast.makeText(
					getActivity(),
					"Cooey recommends standing on bare foot for correct values.",
					Toast.LENGTH_LONG).show();
		}
		if (tmp.length != 2)
			tmp[1] = ".00";
		tvfat.setText(tmp[0]);
		tvfatdec.setText("." + tmp[1]);

		tmp = String.format("%.2f", wd.getBoneDensity()).split("\\.");
		if (tmp.length != 2)
			tmp[1] = ".00";
		tvbone.setText(tmp[0]);
		tvbonedec.setText("." + tmp[1]);

		tmp = String.format("%.2f", wd.getBodyWaterRatio()).split("\\.");
		if (tmp.length != 2)
			tmp[1] = ".00";
		tvwtr.setText(tmp[0]);
		tvwtrdec.setText("." + tmp[1]);

		tmp =String.format("%.2f", wd.getMuscleMassRatio()).split("\\.");
		if (tmp.length != 2)
			tmp[1] = ".00";
		tvmus.setText(tmp[0]);
		tvmusdec.setText("." + tmp[1]);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.takeweightreading, container,
				false);

		lineChart = (LineChart) rootView.findViewById(R.id.wtchart);
		lvWeights = (ListView) rootView.findViewById(R.id.lvwtlist);

		tvw = (TextView) rootView.findViewById(R.id.txtwt);

		tvbmi = (TextView) rootView.findViewById(R.id.txtwtbmi);
		tvbmidec = (TextView) rootView.findViewById(R.id.txtwtbmidec);
		tvfat = (TextView) rootView.findViewById(R.id.txtwtfat);
		tvfatdec = (TextView) rootView.findViewById(R.id.txtwtfatdec);
		tvbone = (TextView) rootView.findViewById(R.id.txtwtbone);
		tvbonedec = (TextView) rootView.findViewById(R.id.txtwtbonedec);
		tvwtr = (TextView) rootView.findViewById(R.id.txtwtwater);
		tvwtrdec = (TextView) rootView.findViewById(R.id.txtwtwaterdec);
		tvmus = (TextView) rootView.findViewById(R.id.txtwtmus);
		tvmusdec = (TextView) rootView.findViewById(R.id.txtwtmusdec);
		
		LsDeviceInfo dev = ((MainActivity) getActivity())
				.getCurrentDeviceToRead();
		activeProfile = ((MainActivity) getActivity()).getActiveProfile();

		if(dev.getProtocolType().contains("A3")){
			getActivity().getActionBar().setTitle("  Profile " + dev.getDeviceUserNumber() );
		}else{
			getActivity().getActionBar().setTitle("  Take Weight Reading ");
		}
		prepareChart(rootView);

		long uid = ((MainActivity) getActivity()).getActiveProfile().getDbid();
		CooeyWTDataSource ds = new CooeyWTDataSource(getActivity());
		ds.open();
		lstwd = ds.getWTValuesForUser(uid, " date DESC ");
		/*
		 * WeightData tmp1 = new WeightData(); tmp1.setWeightKg(70.3);
		 * tmp1.setDate("1"); WeightData tmp2 = new WeightData();
		 * tmp2.setWeightKg(71.3); tmp2.setDate("2"); lstwd.add(tmp1);
		 * lstwd.add(tmp2);
		 */
		ds.close();

		wtadapter = new WeightListAdapter<WeightData>(getActivity(),
				lstwd);
		lvWeights.setAdapter(wtadapter);

		mManager = ((MainActivity) getActivity()).getBleLeManager();
		mManager.deRegisterAllDevices();
		mManager.registerPairedDevice(dev);
		mManager.receiveData(new CooeyDeviceDataReceiveCallback() {

			@Override
			public void onReceiveWeightdata(final WeightData wd) {
				// TODO Auto-generated method stub

				((MainActivity) getActivity()).addWtForUser(wd);
				lstwd.add(wd);
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						resetTrendButtons();
						displayValueOnScreen(wd);
						((Button)rootView.findViewById(R.id.btn1d)).setBackgroundResource(R.drawable.grnbrdvwgr2);
					}
				});
				
				showChartFor("today");
			}

			@Override
			public void onReceiveBPData(final BPData bpd) {

			}
		});
		RelativeLayout bmilayout = (RelativeLayout) rootView
				.findViewById(R.id.lytbmicoll);
		bmilayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				chartValueToDisplay = 1;
				selectGraphView();
			}
		});

		Button btn1d = (Button) rootView.findViewById(R.id.btn1d);
		btn1d.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetTrendButtons();
				showChartFor("today");
				selectGraphView();
				v.setBackgroundResource(R.drawable.grnbrdvwgr2);
			}
		});

		Button btn1w = (Button) rootView.findViewById(R.id.btn1w);
		btn1w.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetTrendButtons();
				showChartFor("week");
				selectGraphView();
				v.setBackgroundResource(R.drawable.grnbrdvwgr2);
			}
		});

		Button btn1m = (Button) rootView.findViewById(R.id.btn1m);
		btn1m.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetTrendButtons();
				showChartFor("month");
				v.setBackgroundResource(R.drawable.grnbrdvwgr2);
			}
		});

		Button btn3m = (Button) rootView.findViewById(R.id.btn3m);
		btn3m.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// showChartForLast3Month();
			}
		});

		btnch = (Button) rootView.findViewById(R.id.btngraph);
		btnlv = (Button) rootView.findViewById(R.id.btnList);

		btnlv.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectListView();
			}
		});

		btnch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				selectGraphView();
			}
		});
		btn1w.setBackgroundResource(R.drawable.grnbrdvwgr2);
		((MainActivity) getActivity()).changeBluetoothState(true);
		showChartFor("week");// default
		return rootView;
	}

	@Override
	public void onValueSelected(Entry e, int dataSetIndex) {
		Toast.makeText(
				getActivity(),
				"Weight was " + e.getVal() + " on "
						+ mChart.getXValue(e.getXIndex()), Toast.LENGTH_SHORT)
				.show();
		// mChart.getXValue(e.getXIndex());
		//if(lstwd.size() == 0) return;
		WeightData tempWd = lstwd.get(e.getXIndex());
		if (tempWd != null) {
			displayValueOnScreen(tempWd);
		}
	}

	@Override
	public void onNothingSelected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChartLongPressed(MotionEvent me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChartDoubleTapped(MotionEvent me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChartSingleTapped(MotionEvent me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub

	}

}
