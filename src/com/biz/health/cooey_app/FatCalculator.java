package com.biz.health.cooey_app;


public class FatCalculator {
    public final double getBmi(double w, double hm) {
        return w / (hm * hm);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public final double getBodyWater(int n, double d, int n2, double d2, boolean bl) {
        if (bl) {
            switch (n) {
                default: {
                    do {
                        return 0.0;
                    } while (true);
                }
                case 0: {
                    return 91.305 + (-1.191 * d2 - 0.00768 * d + 0.08148 * (double)n2);
                }
                case 1: 
                	return 80.286 + (-1.132 * d2 - 0.0052 * d + 0.07152 * (double)n2);
            }
            
        }
        switch (n) {
            default: {
                return 0.0;
            }
            case 0: {
                return 87.51 + (-1.162 * d2 - 0.00813 * d + 0.07594 * (double)n2);
            }
            case 1: 
            	return 77.721 + (-1.148 * d2 - 0.00573 * d + 0.06448 * (double)n2);
        }
        
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public final double getBone(int n, double d, int n2, double d2, boolean bl) {
        if (bl) {
            switch (n) {
                default: {
                    do {
                        return 0.0;
                    } while (true);
                }
                case 0: {
                    return 8.091 + (-0.0856 * d2 - 5.25E-4 * d - 0.0403 * (double)n2);
                }
                case 1:
                	return 8.309 + (-0.0965 * d2 - 4.02E-4 * d - 0.0389 * (double)n2);
            }
            
        }
        switch (n) {
            default: {
                return 0.0;
            }
            case 0: {
                return 7.829 + (-0.0855 * d2 - 5.92E-4 * d - 0.0389 * (double)n2);
            }
            case 1: 
            	return 7.98 + (-0.0973 * d2 - 4.84E-4 * d - 0.036 * (double)n2);
        }
        
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public final double getCalorie(int n, double d, double d2, int n2, boolean bl) {
        if (bl) {
            switch (n) {
                default: {
                    do {
                        return 0.0;
                    } while (true);
                }
                case 0: {
                    return 101.828 + (25.893 * d + 929.104 * d2 - 8.972 * (double)n2);
                }
                case 1: 
                	return 1015.802 + (19.532 * d + 433.227 * d2 - 5.724 * (double)n2);
            }
            
        }
        switch (n) {
            default: {
                return 0.0;
            }
            case 0: {
                return 101.46 + (21.23 * d + 775.105 * d2 - 10.553 * (double)n2);
            }
            case 1: 
            	return 1014.35 + (14.877 * d + 279.277 * d2 - 7.285 * (double)n2);
        }
        
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public final double getFat(int n, double imp, int age, double bmi, boolean isAth) {
        if (isAth) {
            switch (n) {
                default: {
                    do {
                        return 0.0;
                    } while (true);
                }
                case 0: {
                    return bmi * (1.504 + 3.835E-4 * imp) + 0.102 * (double)age - 26.565;
                }
                case 1: 
                	return bmi * (1.511 + 3.296E-4 * imp) + 0.104 * (double)age - 17.241;
            }
            
        }
        switch (n) {
            default: {
                return 0.0;
            }
            case 0: {
                return bmi * (1.479 + 4.4E-4 * imp) + 0.1 * (double)age - 21.764;
            }
            case 1: 
            	return bmi * (1.506 + 3.908E-4 * imp) + 0.1 * (double)age - 12.834;
        }
        
    }

    public final double getImp(int n) {
        if (n >= 410) {
            return 0.3 * (double)(n - 400);
        }
        return 3.0;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public final double getMuscle(int n, double d, int n2, double d2, boolean bl) {
        if (bl) {
            switch (n) {
                default: {
                    do {
                        return 0.0;
                    } while (true);
                }
                case 0: {
                    return 77.389 + (-0.819 * d2 - 0.00486 * d - 0.382 * (double)n2);
                }
                case 1: 
                	return 59.225 + (-0.685 * d2 - 0.00283 * d - 0.274 * (double)n2);
            }
            
        }
        switch (n) {
            default: {
                return 0.0;
            }
            case 0: {
                return 74.627 + (-0.811 * d2 - 0.00565 * d - 0.367 * (double)n2);
            }
            case 1: 
            	return 57.0 + (-0.694 * d2 - 0.00344 * d - 0.255 * (double)n2);
        }
        
    }
}

