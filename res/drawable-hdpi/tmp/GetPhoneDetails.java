package com.biz.health.cooey_app;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class GetPhoneDetails extends Fragment{

	private View rootView;
	private Button btnNext;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		rootView = inflater.inflate(R.layout.phonedetails, container,
				false);
		
		btnNext = (Button)rootView.findViewById(R.id.btnNxtPhNum);
		btnNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SharedPreferences prefs = getActivity().getPreferences(Activity.MODE_PRIVATE);
				String prefDevId = prefs.getString("com.cooey.deviceid", null);
				String prefPhNum = prefs.getString("com.cooey.deviceid", null);
				// getProfileFromServer()
				
				//if empty, that means the user is new and not onboarded.
					//show the emailid, password, emerg contact screen
				//else
					//has he onboarded?
				
			}
		});
		
		EditText phNum = (EditText) rootView.findViewById(R.id.txtphNumber);
		phNum.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) { 
					if(v.getText().length() > 10)
						btnNext.setVisibility(View.VISIBLE);
				}
				return false;
			}
		});
		
		return rootView;
	}
}
