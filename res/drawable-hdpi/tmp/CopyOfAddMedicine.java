package com.biz.health.cooey_app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.biz.cooey.SendJsonToCooeyTask;
import com.biz.cooey.SendToCooeyCallbackOnResponse;
import com.biz.health.model.MedicineData;
import com.biz.health.servermodel.MedServerDataPut;
import com.biz.health.utils.db.CooeyMedDataSource;
import com.google.gson.GsonBuilder;


public class CopyOfAddMedicine extends Fragment {
	
	private FetchDataFromUrl fetch = new FetchDataFromUrl();
	private List<MedicineModel> COUNTRIES;
	private MedicineAdapter medAdapter;
	private ExpandableListAdapter listAdapter, listAdapterD;
    private ExpandableListView expListView, expListViewD;
    private Button addMedicine, skipMedicine, clearMedicine;
    private View rootView = null;
    private AlertDialog dosageDialog = null, reminderDialog = null;
    private InputMethodManager imm = null;
    private List<String> reminderdays = new ArrayList<String>();
    private List<String> dosages = new ArrayList<String>();
    private List<MedicineData> mLstRecentMeds = new ArrayList<MedicineData>();
    private MyRecentMedsAdapter<MedicineData> recmedsadapter = null;
    private ListView recentmedslv = null;
    private MedicineModel currentSelectedMedModel = null;
    private ChosenFoodTimes foodtimes;
    
    private class ChosenFoodTimes{
    	public String bb="0", ab="0", bl="0", al="0", bd="0", ad="0";
    	
    	public String getFoodTimesString(){
    		String freq = bb+"-"+ab+","+bl+"-"+al+","+bd+"-"+ad;
    		return freq;
    	}
    	
    	public void resetTimes(){
    		bb="0"; ab="0"; bl="0"; al="0"; bd="0"; ad="0";
    	}
    }
	public CopyOfAddMedicine() {
		//onSuccess = _onSuccess;
		COUNTRIES = new ArrayList<MedicineModel>();
		MedicineModel hm = new MedicineModel();
		hm.setMedName("Crocin");
		hm.setMedComp("Paracetamol");
		dosages.add("0"); dosages.add("mg");
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		activity.getActionBar().setTitle("ADD MEDICATION");
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AutoCompleteTextView ac = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		if( !ac.getHint().equals("")){
			hideViews(rootView);
		}
		else if(!ac.getText().equals("")){
			showViews(rootView);
		}
		getActivity().getActionBar().setTitle("ADD MEDICATION");
		clearForNewMedicine();
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {		
		//super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.medicine, menu);	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch(id){
			case R.id.mydashfrommed:
				getFragmentManager().popBackStack();
				getFragmentManager().beginTransaction().replace(R.id.container, 
						((MainActivity)getActivity()).getDashboard(), "").addToBackStack(null).commit();
				return true;
			case R.id.listfrommed:
				MyMedicines mm = new MyMedicines();
				getFragmentManager().beginTransaction().replace(R.id.container, mm, "medList").addToBackStack(null).commit();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	public class FetchDataFromUrl extends AsyncTask<String, Void, JSONObject>{
		InputStream is = null;
		String json = "";
		JSONObject jObj;
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			if(isCancelled()) return null;
			String param0 = params[0];
			 try {
			      // defaultHttpClient
			      DefaultHttpClient httpClient = new DefaultHttpClient();
			      HttpGet httpGet = new HttpGet(param0);
			      HttpResponse httpResponse = httpClient.execute(httpGet);
			      HttpEntity httpEntity = httpResponse.getEntity();
			      is = httpEntity.getContent();
			    } catch (UnsupportedEncodingException e) {
			      e.printStackTrace();
			    } catch (ClientProtocolException e) {
			      e.printStackTrace();
			    } catch (IOException e) {
			      e.printStackTrace();
			    }
			 try {
				  BufferedReader reader = new BufferedReader(new InputStreamReader(
				      is, "iso-8859-1"), 8);
				  StringBuilder sb = new StringBuilder();
				  String line = null;
				  while ((line = reader.readLine()) != null) {
				    sb.append(line + "n");
				  }
				  is.close();
				  json = sb.toString();
			    } catch (Exception e) {
			      Log.e("Buffer Error", "Error converting result " + e.toString());
			    }
			    // try parse the string to a JSON object
			    try {
			      jObj = new JSONObject(json);
			    } catch (JSONException e) {
			      Log.e("JSON Parser", "Error parsing data " + e.toString());
			    }
			    // return JSON String
			    return jObj;
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result == null){
				medAdapter.clear();
				return;
			}
			try {
				
				if(result.get("medication") == null){
					medAdapter.clear();
					return;
				}
				if(result.get("medication").toString() == "null"){
					medAdapter.clear();
					return;
				}
				JSONArray meds = result.getJSONArray("medication");
				medAdapter.clear();
				for (int i=0; i< meds.length(); ++i){
					Log.d("FOR EACH", (String) ((JSONObject) meds.get(i)).get("name"));
					MedicineModel m = new MedicineModel();
					m.setMedName((String) ((JSONObject) meds.get(i)).get("name"));
					if(((JSONObject) meds.get(i)).get("base_component")!=null)
					{
						JSONArray tmp = (JSONArray) ((JSONObject) meds.get(i)).get("base_component");
						m.setMedComp(tmp.toString());
					}
					m.setMedId((String) ((JSONObject) meds.get(i)).get("medicineId"));
					medAdapter.add(m);
					//medAdapter.add((String) ((JSONObject) meds.get(i)).get("name"));
				}
				Log.d("COUNT", ""+medAdapter.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//update UI
			//medAdapter.notifyDataSetChanged();
		}
	}

	private void searchMedicine(String name){
		if(name.startsWith("{") && name.endsWith("}")){
			name = name.replace("{", "").replace("}", "");
			name = name.split("=")[0];
		}
		//String srch = "http://asl002.cloudapp.net:9010/ehealth/v1/actvity/medicine/search/"+name;
		//http://api.cooey.co.in/ehealth/v1/actvity/medicine/search/test/5
		String srch = "http://api.cooey.co.in/ehealth/v1/actvity/medicine/search/"+name+"/0";
		if(!fetch.isCancelled()){ 
			fetch.cancel(true);
		
			fetch = new FetchDataFromUrl(); 
			fetch.execute(new String[] {srch});
		}
		
		return;
	}
	
	private String[] getWeekDaysInNums(){
		List<String> daysWeek = new ArrayList<String>(7);
	
		for (String string : reminderdays) {
			if(string.equalsIgnoreCase("mon")){ daysWeek.add("1");}
			if(string.equalsIgnoreCase("tue")){ daysWeek.add("2");}
			if(string.equalsIgnoreCase("wed")){ daysWeek.add("3");}
			if(string.equalsIgnoreCase("thu")){ daysWeek.add("4");}
			if(string.equalsIgnoreCase("fri")){ daysWeek.add("5");}
			if(string.equalsIgnoreCase("sat")){ daysWeek.add("6");}
			if(string.equalsIgnoreCase("sun")){ daysWeek.add("7");}			
		}
		String[] stockArr = new String[daysWeek.size()];	
		stockArr = daysWeek.toArray(stockArr);
		return stockArr;
	}
	
	private MedicineData getUserChosenMedicine(){
		MedicineData md = new MedicineData();
		//String freq = "0-0,0-0,0-0";
		AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		md.setMedicineName(currentSelectedMedModel.getMedName()) ;//actv.getText().toString());
		
		TextView tv = (TextView)rootView.findViewById(R.id.medComp);
		md.setBaseComponent(tv.getText().toString()); //TODO: use currentSelectedMedModel
		
		md.setMedicineId(currentSelectedMedModel.getMedId());
		
		String bb="0", ab="0", bl="0", al="0", bd="0", ad="0";
		ToggleButton tb = (ToggleButton)rootView.findViewById(R.id.tglBfrBfast);
		if(tb.isChecked()) bb = "1";
		
		tb = (ToggleButton)rootView.findViewById(R.id.tglAftBfast);
		if(tb.isChecked()) ab = "1";
		
		tb = (ToggleButton)rootView.findViewById(R.id.tglBfrLunch);
		if(tb.isChecked()) bl = "1";
		
		tb = (ToggleButton)rootView.findViewById(R.id.tglAftLunch);
		if(tb.isChecked()) al = "1";
		
		tb = (ToggleButton)rootView.findViewById(R.id.tglBfrDnr);
		if(tb.isChecked()) bd = "1";
		
		tb = (ToggleButton)rootView.findViewById(R.id.tglAftDnr);
		if(tb.isChecked()) ad = "1";
		
		String freq = bb+"-"+ab+","+bl+"-"+al+","+bd+"-"+ad;
		md.setFrequency(freq);
		
		if(reminderdays.size() != 0) md.setIsRemind(1);
		md.setReminders(TextUtils.join(",", reminderdays));
		
		md.setDosage_unit(dosages.get(1));
		md.setRemind("true");
		md.setDosage_times(new String[] {"time1", "time2"});		
		md.setActive(true);
		md.setDosage(dosages.get(0));//+" "+dosages.get(1));
	
		md.setDaysWeek(getWeekDaysInNums());
		md.setTod("1-0-0");
		
		return md;
	}
	
	private void addMedicineWrapper(long uid){
		
		MedicineData toadd = getUserChosenMedicine();
		CooeyMedDataSource ds = new CooeyMedDataSource(getActivity());
		ds.open();
		ds.createMedicineForuser(toadd, uid);
		ds.close();
		//send to server
		mLstRecentMeds.add(0, toadd);
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(getActivity(), "Medicine Added", Toast.LENGTH_SHORT).show();	
				//recmedsadapter.notifyDataSetChanged();
			}
		});
		MedServerDataPut msd = new MedServerDataPut();
		msd.md = toadd;
		msd.patientId = ((MainActivity)getActivity()).getActiveProfile().getPatientId();
		
		String mMedJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(msd);
		Log.d(" MEDICINE DATA TO POST ", mMedJson);
		SendJsonToCooeyTask sendMed = (SendJsonToCooeyTask) new SendJsonToCooeyTask(new SendToCooeyCallbackOnResponse() {
			
			@Override
			public String getUrl() {
				return "http://api.cooey.co.in/ehealth/v1/profile/medicine";
			}
			
			@Override
			public void callme(String resp) {
				// TODO Auto-generated method stub
				int x = 0;
			}
		}).execute(mMedJson);
		clearForNewMedicine();
		hideViews(rootView);
	}
	
	public class AddMedicineListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			addMedicineWrapper(1);
			//Fragment f = ((MainActivity)getActivity()).getNextFlowAddMed();
			//getFragmentManager().beginTransaction().replace(R.id.container, f, "").addToBackStack(null).commit();
		}
	}
	
	public class AddMoreMedicineListener implements OnClickListener{

		@Override
		public void onClick(View v) {			
			clearForNewMedicine();
			hideViews(rootView);
		}
	}
	
	private void showRecentMeds(){
		
		ListView lv = (ListView)rootView.findViewById(R.id.lstrecentmeds);
		lv.setVisibility(View.VISIBLE);
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				recmedsadapter.notifyDataSetChanged();
				recentmedslv.post(new Runnable() {
					
					@Override
					public void run() {
						recentmedslv.smoothScrollToPosition(0);;
						
					}
				});
			}
		});
	}
	
	private void hideRecentMeds(){
		
		ListView lv  = (ListView)rootView.findViewById(R.id.lstrecentmeds);
		lv.setVisibility(View.GONE);
		
	}
	
	private void clearForNewMedicine(){
		//clear up all views and hide.
		AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		actv.setText("");
		actv.setHint(R.string.medicineName);
		TextView tv = (TextView)rootView.findViewById(R.id.medComp);
		tv.setText("");
		tv.setHint(R.string.med_base_comp);
		//reset the food times
		if(foodtimes == null)
			foodtimes = new ChosenFoodTimes();
		
		foodtimes.resetTimes();
	}
	
	public class SkipMedicineListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			Fragment f = ((MainActivity)getActivity()).getNextFlowAddMed();
			getFragmentManager().beginTransaction().replace(R.id.container, f, "").addToBackStack(null).commit();
		}
	}
	
	private void hideViews(View parent){
		
		TableRow tr1 = (TableRow)parent.findViewById(R.id.trbfast);
		TableRow tr2 = (TableRow)parent.findViewById(R.id.trlunch);
		TableRow tr3 = (TableRow)parent.findViewById(R.id.trdinner);
		
		Button addmed = (Button)parent.findViewById(R.id.addMedBtn);
		Button clearmed = (Button)parent.findViewById(R.id.btnclearmed);
		((Button)parent.findViewById(R.id.btnRemind)).setVisibility(parent.INVISIBLE);
		((Button)parent.findViewById(R.id.btnDosage)).setVisibility(parent.INVISIBLE);
		tr1.setVisibility(parent.INVISIBLE);
		tr2.setVisibility(parent.INVISIBLE);
		tr3.setVisibility(parent.INVISIBLE);
		addmed.setVisibility(parent.INVISIBLE);
		clearmed.setVisibility(parent.INVISIBLE);
		showRecentMeds();
		/*
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		
		params.addRule(RelativeLayout.ALIGN_BOTTOM, ((TextView)parent.findViewById(R.id.medComp)).getId());
	
		RelativeLayout rl = (RelativeLayout)rootView.findViewById(R.id.addmedRelLyt);
		
		TextView tv = new TextView(getActivity());
		tv.setText("HELP");
		tv.setLayoutParams(params);
		
		rl.addView(tv, params);*/
	}
	
	private void showViews(View parent){
		TableRow tr1 = (TableRow)parent.findViewById(R.id.trbfast);
		TableRow tr2 = (TableRow)parent.findViewById(R.id.trlunch);
		TableRow tr3 = (TableRow)parent.findViewById(R.id.trdinner);
		Button addmed = (Button)parent.findViewById(R.id.addMedBtn);
		Button clearmed = (Button)parent.findViewById(R.id.btnclearmed);
		((Button)parent.findViewById(R.id.btnRemind)).setVisibility(parent.VISIBLE);
		((Button)parent.findViewById(R.id.btnDosage)).setVisibility(parent.VISIBLE);
		tr1.setVisibility(parent.VISIBLE);
		tr2.setVisibility(parent.VISIBLE);
		tr3.setVisibility(parent.VISIBLE);
		clearmed.setVisibility(parent.VISIBLE);
		
		AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		addmed.setVisibility(parent.VISIBLE);
		
		imm.hideSoftInputFromWindow(actv.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		hideRecentMeds();
	}
	
	private void prepareDialogs(){
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View reminder = inflater.inflate(R.layout.reminder_items, null);
		final LinearLayout dos = (LinearLayout) inflater.inflate(R.layout.dosage_items, null);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(reminder)
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   reminderdays.clear();
                       if(((ToggleButton)reminder.findViewById(R.id.tglMon)).isChecked()){
                    	   reminderdays.add("Mon");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglTue)).isChecked()){
                    	   reminderdays.add("Tue");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglWed)).isChecked()){
                    	   reminderdays.add("Wed");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglThu)).isChecked()){
                    	   reminderdays.add("Thu");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglFri)).isChecked()){
                    	   reminderdays.add("Fri");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglSat)).isChecked()){
                    	   reminderdays.add("Sat");
                       }
                       if(((ToggleButton)reminder.findViewById(R.id.tglSun)).isChecked()){
                    	   reminderdays.add("Sun");
                       }
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // User cancelled the dialog
                   }
               });
        // Create the AlertDialog object and return it
        reminderDialog = builder.create();
        
        
        ArrayAdapter<String> ba = new ArrayAdapter<String>(getActivity().getApplicationContext(), 
        		R.layout.spinner_cooey_dropdown_item, 
        		getResources().getStringArray(R.array.dosage));
        
        ((Spinner)dos.findViewById(R.id.medSpinDosage)).setAdapter(ba);
        AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
        dosageDialog = 
        		builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   dosages.add(0, ((TextView)dos.findViewById(R.id.medTxtDosage)).getText().toString());
                	   dosages.add(1, (String) ((Spinner)dos.findViewById(R.id.medSpinDosage)).getSelectedItem());
                	   
                	   imm.hideSoftInputFromWindow(dos.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   imm.hideSoftInputFromWindow(dos.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                   }
               }).setView(dos)
               //.setSingleChoiceItems(new String[]{"mg", "g", "capsule/s", "bottle/s"}, 0, null)               
               .create();

	}
	
	private void populateExpandableListViews(View parent){
		
		List<String> listDataHeader = new ArrayList<String>();
        listDataHeader.add("        Remind?");
        List<String> top250 = new ArrayList<String>();
        top250.add(" ");
        HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
        
        listDataChild.put(listDataHeader.get(0), top250);
        
		//expListView = (ExpandableListView)parent.findViewById(R.id.lvExp);
        //listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        /// setting list adapter
        //expListView.setAdapter(listAdapter);
        
        /*
        List<String> listDataHeaderD = new ArrayList<String>();
        listDataHeaderD.add("        Dosage");
        List<String> ch = new ArrayList<String>();
        ch.add("d");
        HashMap<String, List<String>> listDataChildD = new HashMap<String, List<String>>();
        
        listDataChildD.put(listDataHeaderD.get(0), ch);
        
		expListViewD = (ExpandableListView)parent.findViewById(R.id.lvExpDos);
        listAdapterD = new ExpandableListAdapter(getActivity(), listDataHeaderD, listDataChildD);
        expListViewD.setAdapter(listAdapterD);*/
        
	}
	
	private void populateRecentMeds(){
		
		CooeyMedDataSource ds = new CooeyMedDataSource(getActivity());
		ds.open();
		mLstRecentMeds = ds.getMedicinesForUser(1, " date DESC ");
		ds.close();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		rootView = inflater.inflate(R.layout.addmedicine, container, false);
		addMedicine = (Button)rootView.findViewById(R.id.addMedBtn);
		addMedicine.setOnClickListener(new AddMedicineListener());
		
		clearMedicine = (Button)rootView.findViewById(R.id.btnclearmed);
		clearMedicine.setOnClickListener(new AddMoreMedicineListener());
		
		if( ((MainActivity)getActivity()).isUserOnboarding() )
		{
			skipMedicine = (Button)rootView.findViewById(R.id.skipMedBtn);
			skipMedicine.setOnClickListener(new SkipMedicineListener());
		}else{
			skipMedicine = (Button)rootView.findViewById(R.id.skipMedBtn);
			skipMedicine.setText("Back");
			skipMedicine.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getFragmentManager().beginTransaction().replace(R.id.container, 
							((MainActivity)getActivity()).getDashboard(), "").addToBackStack(null).commit();
				}
			});
		}
		
		imm = (InputMethodManager)getActivity().getSystemService (Context.INPUT_METHOD_SERVICE);
		
		Button reminder = (Button)rootView.findViewById(R.id.btnRemind);
		reminder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				reminderDialog.show();
				
			}
		});
		setHasOptionsMenu(true);
		Button dosage = (Button)rootView.findViewById(R.id.btnDosage);
		dosage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dosageDialog.show();
				
			}
		});
		
		//populateExpandableListViews(rootView);
		prepareDialogs();
        
        
        populateRecentMeds();
        recentmedslv = (ListView)rootView.findViewById(R.id.lstrecentmeds);
        
        recmedsadapter = new MyRecentMedsAdapter<MedicineData>(getActivity().getApplicationContext(), 
        		R.layout.my_recentmeds, 
        		mLstRecentMeds);
        recentmedslv.setAdapter(recmedsadapter);
        
		medAdapter = new MedicineAdapter((Context)getActivity(),
		            android.R.layout.simple_dropdown_item_1line,
		            COUNTRIES);
		final TextView tv = (TextView)rootView.findViewById(R.id.medComp);
		final AutoCompleteTextView actv = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView1);
		actv.setAdapter(medAdapter);
		
		actv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				//fetch.cancel(true);
				//MedicineModel m = medAdapter.getItem(arg2);
				currentSelectedMedModel= medAdapter.getItem(arg2);
				tv.setText(currentSelectedMedModel.getMedComp());
				//TODO: populate dosages available as well.
				
				actv.setText( ((TextView)arg1.findViewById(android.R.id.text1)).getText());
				showViews(rootView);
			}
		});
		actv.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				Log.d(MainActivity.class.getName(), s.toString());
				//fetch.cancel(true);
				if(s.length() < 3){ medAdapter.clear(); return; }
				searchMedicine(s.toString().replace(" ", "%20"));
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		hideViews(rootView);
		clearForNewMedicine();
		return rootView;
	}

	
}
