package com.biz.health.cooey_app;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biz.health.model.*;

public class DashboardAdapter extends BaseAdapter{

	private LayoutInflater inflater = null;
	private int layoutResource;
	private Context ctx;
	private static final int CORNER_RADIUS = 24; // dips
	private static final int MARGIN = 12;
	private float density;
	private List<List<LinkedHashMap<String, Object>>> mData = new ArrayList<List<LinkedHashMap<String,Object>>>();
    private String[] mKeys;
    
    public DashboardAdapter(Context context, int resource, List<List<LinkedHashMap<String, Object>>> dashBoardListView){
    	ctx = context;
		layoutResource = resource;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		density = ctx.getResources().getDisplayMetrics().density;
    	mData  = dashBoardListView;
        //mKeys = mData.keySet().toArray(new String[dashBoardListView.size()]);
    }
    
	/*public DashboardAdapter(Context context, int resource, List<String> objects) {
		super(context, resource, objects);
		ctx = context;
		listOptions = objects;
		layoutResource = resource;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		density = ctx.getResources().getDisplayMetrics().density;
	}*/

    @Override
	public int getCount() {
		return mData.size();
    }
 
    @Override
    public Object getItem(int position) {
    	//return mData.get(mKeys[position]);
    	return mData.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
	
    private void prepareViewForMed(MedDashBoard md, View convertView){
    	
    	if(md == null) return;
    	
    	RelativeLayout h00 = (RelativeLayout)convertView.findViewById(R.id.header00);
    	h00.setBackgroundColor(ctx.getResources().getColor(R.color.green3));
    	RelativeLayout py00 = (RelativeLayout)convertView.findViewById(R.id.payld00);
    	py00.setBackgroundResource(R.drawable.roundbordergreen3);
    	
    	TextView txth00 = (TextView)convertView.findViewById(R.id.txtheader00);
		//TextView txtcnt00 = (TextView)convertView.findViewById(R.id.txtcount00);
		TextView txth200 = (TextView)convertView.findViewById(R.id.txtheader200);
		txth00.setText("MEDICINES");
		txth200.setText("Total");
		//txtcnt00.setText("0");
		
		//txtcnt00.setText(" " + String.valueOf(md.count)+" ");
		//((GradientDrawable)txtcnt00.getBackground()).setColor(ctx.getResources().getColor(R.color.White));
		
		TextView tvVal1Text = (TextView)convertView.findViewById(R.id.txtval1);
		TextView tvVal1 = (TextView)convertView.findViewById(R.id.txtval1val);
		TextView tvVal2Text = (TextView)convertView.findViewById(R.id.txtval2);
		TextView tvVal2 = (TextView)convertView.findViewById(R.id.txtval2val);
		TextView tvVal3Text = (TextView)convertView.findViewById(R.id.txtval3);
		TextView tvVal3 = (TextView)convertView.findViewById(R.id.txtval3val);
		
		ImageView img1 = (ImageView)convertView.findViewById(R.id.imgval1);
		img1.setImageDrawable(ctx.getResources().getDrawable(R.drawable.bottle48));
		
		ImageView img2 = (ImageView)convertView.findViewById(R.id.imgval2);
		img2.setVisibility(View.INVISIBLE);
		
		ImageView img3 = (ImageView)convertView.findViewById(R.id.imgval3);
		img3.setImageDrawable(ctx.getResources().getDrawable(R.drawable.clock));
		
		tvVal1Text.setText("Name");
		tvVal1.setText("");
		((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		
		tvVal2Text.setText("Dosage");
		tvVal2.setText("");
		((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
		
		tvVal3Text.setText("Last added");
		tvVal3.setText("");
		((GradientDrawable)tvVal3.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		
		if(md.md != null) {
			/*tvVal1Text.setText(md.md.getMedicineName());
			tvVal1.setVisibility(View.GONE);
			
			tvVal2.setText(md.md.getDosage());
			
			tvVal3.setText(md.md.getDate());*/
		}
		RelativeLayout pyld4 = (RelativeLayout) convertView.findViewById(R.id.paylditem004);
		pyld4.setVisibility(View.INVISIBLE);		
    }
    
    private void prepareViewForWT(WTDashBoard wd, View convertView){
    	
		if(wd == null) return;
		
		RelativeLayout h00 = (RelativeLayout)convertView.findViewById(R.id.header00);
    	h00.setBackgroundColor(ctx.getResources().getColor(R.color.teal3));
    	RelativeLayout py00 = (RelativeLayout)convertView.findViewById(R.id.payld00);
    	py00.setBackgroundResource(R.drawable.roundborderteal3);
    	
		TextView txth00 = (TextView)convertView.findViewById(R.id.txtheader00);
		//TextView txtcnt00 = (TextView)convertView.findViewById(R.id.txtcount00);
		TextView txth200 = (TextView)convertView.findViewById(R.id.txtheader200);
		
		txth200.setText("Weight");
		//txtcnt00.setText("0");
		
		//txtcnt00.setText(" " + String.valueOf(wd.count)+" ");
		//((GradientDrawable)txtcnt00.getBackground()).setColor(ctx.getResources().getColor(R.color.White));
		
		TextView tvVal1Text = (TextView)convertView.findViewById(R.id.txtval1);
		TextView tvVal1 = (TextView)convertView.findViewById(R.id.txtval1val);
		TextView tvVal2Text = (TextView)convertView.findViewById(R.id.txtval2);
		TextView tvVal2 = (TextView)convertView.findViewById(R.id.txtval2val);
		TextView tvVal3Text = (TextView)convertView.findViewById(R.id.txtval3);
		TextView tvVal3 = (TextView)convertView.findViewById(R.id.txtval3val);
		TextView tvVal4Text = (TextView)convertView.findViewById(R.id.txtval4);
		TextView tvVal4 = (TextView)convertView.findViewById(R.id.txtval4val);
		
		ImageView img1 = (ImageView)convertView.findViewById(R.id.imgval1);
		img1.setImageDrawable(ctx.getResources().getDrawable(R.drawable.wt48));
		
		ImageView img2 = (ImageView)convertView.findViewById(R.id.imgval2);
		img2.setImageDrawable(ctx.getResources().getDrawable(R.drawable.b48));
		
		ImageView img3 = (ImageView)convertView.findViewById(R.id.imgval3);
		img3.setImageDrawable(ctx.getResources().getDrawable(R.drawable.fat48));
		
		ImageView img4 = (ImageView)convertView.findViewById(R.id.imgval4);
		img4.setImageDrawable(ctx.getResources().getDrawable(R.drawable.clock));
		
		tvVal1Text.setText("Weight");
		tvVal1.setText("");
		((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		tvVal2Text.setText("BMI");
		tvVal2.setText("");
		((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
		tvVal3Text.setText("Fat");
		tvVal3.setText("");
		((GradientDrawable)tvVal3.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		tvVal4Text.setText("Last taken");
		tvVal4.setText("");
		((GradientDrawable)tvVal4.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
		
		if(wd.wt != null) {			
			tvVal1.setText(String.valueOf(Math.round(10  * wd.wt.getWeightKg())/10.0));
			((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));			
			
			tvVal2.setText(String.valueOf(Math.round(100  * wd.wt.getBmi())/100.0));
			((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));			
			
			tvVal3.setText(String.valueOf(Math.round(100  * wd.wt.getBodyFatRatio())/100.0));
			((GradientDrawable)tvVal3.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));			
			
			tvVal4.setText(wd.wt.getDate());
			((GradientDrawable)tvVal4.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));			
		}		
    }
    
    private void prepareViewForDEV(DevDashBoard db, View convertView){
    	
    	if(db == null) return;
    	
    	RelativeLayout h00 = (RelativeLayout)convertView.findViewById(R.id.header01);
    	h00.setBackgroundColor(ctx.getResources().getColor(R.color.green3));
    	RelativeLayout py01 = (RelativeLayout)convertView.findViewById(R.id.payld01);
    	py01.setBackgroundResource(R.drawable.roundbordergreen3);
    	
    	TextView txth01 = (TextView)convertView.findViewById(R.id.txtheader01);
		//TextView txtcnt01 = (TextView)convertView.findViewById(R.id.txtcount01);
		TextView txth201 = (TextView)convertView.findViewById(R.id.txtheader201);
		
		txth01.setText("DEVICES");
		txth201.setText("Total");
		//txtcnt01.setText(" " + String.valueOf(db.count)+" ");
		
		TextView tvVal1Text = (TextView)convertView.findViewById(R.id.pyld011txtval1);
		tvVal1Text.setText("Type");
		TextView tvVal1 = (TextView)convertView.findViewById(R.id.pyld011txtval1val);
		tvVal1.setText("");
		((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		ImageView img1 = (ImageView)convertView.findViewById(R.id.pyld011imgval1);
		img1.setImageDrawable(ctx.getResources().getDrawable(R.drawable.s48));
		
		
		TextView tvVal2Text = (TextView)convertView.findViewById(R.id.pyld012txtval2);
		TextView tvVal2 = (TextView)convertView.findViewById(R.id.pyld012txtval2val);
		
		tvVal2Text.setText("Added on");
		tvVal2.setText("");
		((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
		ImageView img2 = (ImageView)convertView.findViewById(R.id.pyld012imgval2);
		img2.setImageDrawable(ctx.getResources().getDrawable(R.drawable.clock));

		RelativeLayout pyld3 = (RelativeLayout) convertView.findViewById(R.id.paylditem013);
		pyld3.setVisibility(View.INVISIBLE);	
		RelativeLayout pyld4 = (RelativeLayout) convertView.findViewById(R.id.paylditem014);
		pyld4.setVisibility(View.INVISIBLE);	
    }
    
    private void prepareViewForBP(BPDashBoard bpd, View convertView){
    	
    	if(bpd == null) return;
    	RelativeLayout h00 = (RelativeLayout)convertView.findViewById(R.id.header01);
    	h00.setBackgroundColor(ctx.getResources().getColor(R.color.teal3));
    	RelativeLayout py01 = (RelativeLayout)convertView.findViewById(R.id.payld01);
    	py01.setBackgroundResource(R.drawable.roundborderteal3);
    	
    	TextView txth01 = (TextView)convertView.findViewById(R.id.txtheader01);
		//TextView txtcnt01 = (TextView)convertView.findViewById(R.id.txtcount01);
		TextView txth201 = (TextView)convertView.findViewById(R.id.txtheader201);
		
		//txtcnt01.setText(" " + String.valueOf(bpd.count)+" ");
		
		TextView tvVal1Text = (TextView)convertView.findViewById(R.id.pyld011txtval1);
		tvVal1Text.setText("Systolic");
		TextView tvVal1 = (TextView)convertView.findViewById(R.id.pyld011txtval1val);
		tvVal1.setText("");
		((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		ImageView img1 = (ImageView)convertView.findViewById(R.id.pyld011imgval1);
		img1.setImageDrawable(ctx.getResources().getDrawable(R.drawable.s48));
		
		TextView tvVal2Text = (TextView)convertView.findViewById(R.id.pyld012txtval2);
		TextView tvVal2 = (TextView)convertView.findViewById(R.id.pyld012txtval2val);
		
		tvVal2Text.setText("Diastolic");
		tvVal2.setText("");
		((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
		ImageView img2 = (ImageView)convertView.findViewById(R.id.pyld012imgval2);
		img2.setImageDrawable(ctx.getResources().getDrawable(R.drawable.d48));
		
		TextView tvVal3Text = (TextView)convertView.findViewById(R.id.pyld013txtval3);
		TextView tvVal3 = (TextView)convertView.findViewById(R.id.pyld013txtval3val);
		tvVal3Text.setText("Pulse rate");
		tvVal3.setText("");
		((GradientDrawable)tvVal3.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
		ImageView img3 = (ImageView)convertView.findViewById(R.id.pyld013imgval3);
		//img3.setImageDrawable(ctx.getResources().getDrawable(R.drawable.pulserate1));
	
		TextView tvVal4Text = (TextView)convertView.findViewById(R.id.pyld014txtval4);
		TextView tvVal4 = (TextView)convertView.findViewById(R.id.pyld014txtval4val);
		tvVal4Text.setText("Last taken ");
		tvVal4.setText("");
		((GradientDrawable)tvVal4.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
		ImageView img4 = (ImageView)convertView.findViewById(R.id.pyld014imgval4);
		img4.setImageDrawable(ctx.getResources().getDrawable(R.drawable.clock));
		if(bpd.bp != null) {
			tvVal1.setText(String.valueOf(bpd.bp.getSystolic()));
			tvVal2.setText(String.valueOf(bpd.bp.getDiastolic()));
			tvVal3.setText(String.valueOf(bpd.bp.getPulseRate()));
			tvVal4.setText(bpd.bp.getDate());
		}
		
    }
	
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
		{		
			convertView =inflater.inflate(layoutResource, parent, false);
		}
			RelativeLayout h00 = (RelativeLayout)convertView.findViewById(R.id.header00);
			RelativeLayout h01 = (RelativeLayout)convertView.findViewById(R.id.header01);
			
			List<LinkedHashMap<String,Object>> item = (List<LinkedHashMap<String, Object>>) getItem(position);
			
			for (LinkedHashMap<String, Object> i : item) {
				
				prepareViewForMed((MedDashBoard)i.get("MED"), convertView);
				prepareViewForBP((BPDashBoard)i.get("BP"), convertView);
				prepareViewForWT((WTDashBoard)i.get("WT"), convertView);
				prepareViewForDEV((DevDashBoard)i.get("DEV"), convertView);
				
			}
			
			/*
			RelativeLayout rlhdr = (RelativeLayout)convertView.findViewById(R.id.header);
			RelativeLayout rlpl = (RelativeLayout)convertView.findViewById(R.id.payld);
			RelativeLayout rl = (RelativeLayout)convertView.findViewById(R.id.dashbrditem);
			TextView tvcount = (TextView)convertView.findViewById(R.id.txtcount);
			
			TextView tvVal1Text = (TextView)convertView.findViewById(R.id.txtval1);
			TextView tvVal1 = (TextView)convertView.findViewById(R.id.txtval1val);
			TextView tvVal2Text = (TextView)convertView.findViewById(R.id.txtval2);
			TextView tvVal2 = (TextView)convertView.findViewById(R.id.txtval2val);
			TextView tvVal3Text = (TextView)convertView.findViewById(R.id.txtval3);
			TextView tvVal3 = (TextView)convertView.findViewById(R.id.txtval3val);
			TextView tvVal4Text = (TextView)convertView.findViewById(R.id.txtval4);
			TextView tvVal4 = (TextView)convertView.findViewById(R.id.txtval4val);
			
			if(mKeys[position] == "BP"){
				//getPaint().setColor(ctx.getResources().getColor(R.color.Red));
				BPDashBoard tmp = (BPDashBoard)getItem(position);
				tvcount.setText(String.valueOf(tmp.count));
				((GradientDrawable)tvcount.getBackground()).setColor(ctx.getResources().getColor(R.color.White));
				tvVal1Text.setText("Systolic");
				tvVal1.setText(String.valueOf(tmp.bp.getSystolic()));
				((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
				ImageView img1 = (ImageView)convertView.findViewById(R.id.imgval1);
				img1.setImageDrawable(ctx.getResources().getDrawable(R.drawable.s48));
				
				tvVal2Text.setText("Diastolic");
				tvVal2.setText(String.valueOf(tmp.bp.getDiastolic()));
				((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
				ImageView img2 = (ImageView)convertView.findViewById(R.id.imgval2);
				img2.setImageDrawable(ctx.getResources().getDrawable(R.drawable.d48));
				
				tvVal3Text.setText("Pulse rate");
				tvVal3.setText(String.valueOf(tmp.bp.getPulseRate()));
				((GradientDrawable)tvVal3.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
				ImageView img3 = (ImageView)convertView.findViewById(R.id.imgval3);
				img3.setImageDrawable(ctx.getResources().getDrawable(R.drawable.pulserate1));
				
				tvVal4Text.setText("Last taken on");
				tvVal4.setText(tmp.bp.getDate());
				((GradientDrawable)tvVal4.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
				ImageView img4 = (ImageView)convertView.findViewById(R.id.imgval4);
				img4.setImageDrawable(ctx.getResources().getDrawable(R.drawable.clock));
				
			}else if(mKeys[position] == "WT"){
				TextView tvhdr2 = (TextView)convertView.findViewById(R.id.txtheader2);
				tvhdr2.setText("WEIGHT");
				WTDashBoard tmp = (WTDashBoard)getItem(position);
				tvcount.setText(String.valueOf(tmp.count));
				((GradientDrawable)tvcount.getBackground()).setColor(ctx.getResources().getColor(R.color.White));
				
				tvVal1Text.setText("Weight");
				tvVal1.setText(String.valueOf(Math.round(10  * tmp.wt.getWeightKg())/10.0));
				((GradientDrawable)tvVal1.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
				ImageView img1 = (ImageView)convertView.findViewById(R.id.imgval1);
				img1.setImageDrawable(ctx.getResources().getDrawable(R.drawable.wt48));
				
				tvVal2Text.setText("BMI");
				tvVal2.setText(String.valueOf(Math.round(100  * tmp.wt.getBmi())/100.0));
				((GradientDrawable)tvVal2.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
				ImageView img2 = (ImageView)convertView.findViewById(R.id.imgval2);
				img2.setImageDrawable(ctx.getResources().getDrawable(R.drawable.b48));
				
				
				tvVal3Text.setText("Fat");
				tvVal3.setText(String.valueOf(Math.round(100  * tmp.wt.getBodyFatRatio())/100.0));
				((GradientDrawable)tvVal3.getBackground()).setColor(ctx.getResources().getColor(R.color.seaweed));
				ImageView img3 = (ImageView)convertView.findViewById(R.id.imgval3);
				img3.setImageDrawable(ctx.getResources().getDrawable(R.drawable.fat48));
				
				tvVal4Text.setText("Last taken on");
				tvVal4.setText(tmp.wt.getDate());
				((GradientDrawable)tvVal4.getBackground()).setColor(ctx.getResources().getColor(R.color.DarkGray));
				ImageView img4 = (ImageView)convertView.findViewById(R.id.imgval4);
				img4.setImageDrawable(ctx.getResources().getDrawable(R.drawable.clock));
			}else if(mKeys[position] == "DEV"){
				TextView tvhdr = (TextView)convertView.findViewById(R.id.txtheader);
				tvhdr.setText("DEVICES");
				TextView tvhdr2 = (TextView)convertView.findViewById(R.id.txtheader2);
				tvhdr2.setText("Total");
				DevDashBoard tmp = (DevDashBoard)getItem(position);
				tvcount.setText(String.valueOf(tmp.count));
				((GradientDrawable)tvcount.getBackground()).setColor(ctx.getResources().getColor(R.color.White));
				rlpl.setVisibility(View.GONE);
			}else if(mKeys[position] == "MED"){
				TextView tvhdr = (TextView)convertView.findViewById(R.id.txtheader);
				tvhdr.setText("MEDICINES");
				TextView tvhdr2 = (TextView)convertView.findViewById(R.id.txtheader2);
				tvhdr2.setText("Total");
				MedDashBoard tmp = (MedDashBoard)getItem(position);
				tvcount.setText(String.valueOf(tmp.count));
				((GradientDrawable)tvcount.getBackground()).setColor(ctx.getResources().getColor(R.color.White));
				rlpl.setVisibility(View.GONE);
			}
			else if(mKeys[position] == "NA"){
				TextView tvhdr = (TextView)convertView.findViewById(R.id.txtheader);
				tvhdr.setText("Your dashboard is still empty");
				TextView tvhdr2 = (TextView)convertView.findViewById(R.id.txtheader2);
				tvhdr2.setText("Add a device or a medicine to start");
				tvcount.setText("0");
				rlpl.setVisibility(View.GONE);
			}else{
				rl.setVisibility(View.GONE);
			}
			*/
		
		return convertView;
	}

}
